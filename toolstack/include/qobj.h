//
// Glass Display
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#ifndef QOBJ_CONNECT
#define QOBJ_CONNECT
#include <QObject>
namespace seam
{
void inline qobj_connect(const QObject *sender, const char *signal, const QObject *receiver, const char *method)
{
    if (!QObject::connect(sender, signal, receiver, method)) {
        throw std::runtime_error(std::string("Could not connect signal: ").append(signal).append(" to slot: ").append(method).append("!"));
    }
}

void inline qobj_connect(const QObject *sender, const char *signal, const QObject *receiver, const char *method, Qt::ConnectionType type)
{
    if (!QObject::connect(sender, signal, receiver, method, type)) {
        throw std::runtime_error(std::string("Could not connect signal: ").append(signal).append(" to slot: ").append(method).append("!"));
    }
}

} // namespace seam
#endif
