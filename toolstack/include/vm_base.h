//
// Glass Display
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#ifndef VM_BASE__H
#define VM_BASE__H

#include <../xenmgr/dbus_helpers.h>
#include <exception>
#include <glass_types.h>

#include <QObject>
#include <QRect>
#include <QUuid>

class vm_base_t : public QObject
{
    Q_OBJECT;

public:
    vm_base_t(std::shared_ptr<dbus_listener_t> dbus_listener)
        : m_dbus_listener(dbus_listener)
    {
        QDBusConnection::systemBus().connect("com.citrix.xenclient.xenmgr",
                                             "/",
                                             "com.citrix.xenclient.xenmgr",
                                             "vm_config_changed",
                                             this,
                                             SLOT(vm_config_changed(QString)));
    }
    ~vm_base_t()
    {
        QDBusConnection::systemBus().disconnect("com.citrix.xenclient.xenmgr",
                                                "/",
                                                "com.citrix.xenclient.xenmgr",
                                                "vm_config_changed",
                                                this,
                                                SLOT(vm_config_changed(QString)));
    }

    domid_t domid() { return m_domid; }
    domid_t stub_domid() { return m_stub_domid; }
    uuid_t uuid() { return m_uuid; }
    std::string name() { return m_name; }
    std::string os() { return m_os; }
    std::string primary_domain_color() { return m_primary_domain_color; }
    std::string secondary_domain_color() { return m_secondary_domain_color; }
    std::string image_path() { return m_image_path; }
    std::string long_form() { return m_long_form; }
    std::string short_form() { return m_short_form; }
    std::string text_color() { return m_text_color; }
    std::string gpu() { return m_gpu; }
    int border_width() { return m_border_width; }
    int border_height() { return m_border_height; }
    int slot() { return m_slot; }
    rect_t rect() { return m_rect; }
    bool is_sleeping() { return m_sleep_state; }

    void set_domid(domid_t domid) { m_domid = domid; }
    void set_stub_domid(domid_t stub_domid) { m_stub_domid = stub_domid; }
    void set_uuid(uuid_t uuid) { m_uuid = uuid; }
    void set_name(std::string name) { m_name = name; }
    void set_os(std::string os) { m_os = os; }
    void set_primary_domain_color(std::string primary_domain_color) { m_primary_domain_color = primary_domain_color; }
    void set_secondary_domain_color(std::string secondary_domain_color) { m_secondary_domain_color = secondary_domain_color; }
    void set_image_path(std::string image_path) { m_image_path = image_path; }
    void set_long_form(std::string long_form) { m_long_form = long_form; }
    void set_short_form(std::string short_form) { m_short_form = short_form; }
    void set_text_color(std::string text_color) { m_text_color = text_color; }
    void set_gpu(std::string gpu) { m_gpu = gpu; }
    void set_border_width(int border_width) { m_border_width = border_width; }
    void set_border_height(int border_height) { m_border_height = border_height; }
    void set_slot(int slot) { m_slot = slot; }
    void set_rect(rect_t rect) { m_rect = rect; }
    void set_sleep_state(bool is_sleeping) { m_sleep_state = is_sleeping; }

signals:
    void config_changed(QString uuid);

public slots:
    void vm_config_changed(QString uuid)
    {
        if (uuid_t(uuid) == m_uuid) {
            bool success = false;
            // All config values that need to be updated should go here in
            // separate try/catch blocks as needed, success should be set to
            // true only if the update succeeded
            try {
                set_slot(m_dbus_listener->get_slot(uuid));
                success = true;
            } catch (std::exception &e) {
                vg_debug() << e.what();
            }

            // If anything succeeded, we should emit
            if (success) {
                emit config_changed(uuid);
            }
        }
    }

private:
    std::shared_ptr<dbus_listener_t> m_dbus_listener;
    domid_t m_domid;
    domid_t m_stub_domid;
    uuid_t m_uuid;
    std::string m_name;
    std::string m_os;
    std::string m_primary_domain_color;
    std::string m_secondary_domain_color;
    std::string m_image_path;
    std::string m_long_form;
    std::string m_short_form;
    std::string m_text_color;
    std::string m_gpu;
    int m_border_width;
    int m_border_height;
    int m_slot;
    rect_t m_rect;
    bool m_sleep_state;
};

#endif // VM_BASE__H
