//
// Glass Display
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#ifndef XENMGR__H
#define XENMGR__H

#include <glass_types.h>

#include "dbus_helpers.h"
#include "dbus_listener.h"
#include <toolstack.h>

class xenmgr_t : public toolstack_t
{
    Q_OBJECT
public:
    xenmgr_t(std::shared_ptr<vm_render_factory_t> vm_render_factory,
             std::shared_ptr<vm_region_factory_t> vm_region_factory,
             std::shared_ptr<vm_input_factory_t> vm_input_factory);
    virtual ~xenmgr_t() = default;

    virtual std::shared_ptr<vm_t> find_guest(const uuid_t uuid);
    virtual std::shared_ptr<vm_t> find_guest(const domid_t domid);

    virtual bool get_display_configurable();
    virtual bool get_laptop();
    virtual bool get_display_power_indicator();

    virtual void reset();
signals:
    void guest_name_changed(std::shared_ptr<vm_t> guest);
    void wake_up_guest(uuid_t uuid);
    void sleep_guest(uuid_t uuid);
    void battery_charge_state_change(uint state);
    void battery_percentage_change(uint percentage);
    void refresh_battery(void);

public slots:
    // DBus glue

    void guest_started(const uuid_t &uuid);
    void guest_stopped(const uuid_t &uuid);
    void guest_rebooted(const uuid_t &uuid);
    void guest_slept(const uuid_t &uuid);
    void guest_name_changed(const uuid_t &uuid);
    void battery_charge_state_changed(uint state);
    void battery_percentage_changed(uint percentage);
    void battery_refresh(void);

private:
    domid_t get_domid(uuid_t uuid);
    domid_t get_stub_domid(uuid_t uuid);
    std::string get_vm_name(uuid_t uuid);
    std::string get_os(uuid_t uuid);
    std::string get_image_path(uuid_t uuid);
    std::string get_long_form(uuid_t uuid);
    std::string get_short_form(uuid_t uuid);
    std::string get_text_color(uuid_t uuid);
    std::string get_gpu(uuid_t uuid);
    int get_border_width(uuid_t uuid);
    int get_border_height(uuid_t uuid);
    int get_slot(uuid_t uuid);

    std::string get_primary_domain_color(uuid_t uuid);
    std::string get_secondary_domain_color(uuid_t uuid);

    void process_vm_list(const qlist_t<uuid_t> running_vm_list);

    std::shared_ptr<vm_base_t> make_vm_base(uuid_t uuid);

    void create_guest(uuid_t uuid);
    void refresh_guest(uuid_t uuid);
    void destroy_guest(uuid_t uuid);

    qhash_t<uuid_t, std::shared_ptr<vm_t>> m_vms;

    // Probably should make these unique ptrs that get moved
    // during construction.
    std::shared_ptr<vm_render_factory_t> m_vm_render_factory;
    std::shared_ptr<vm_region_factory_t> m_vm_region_factory;
    std::shared_ptr<vm_input_factory_t> m_vm_input_factory;

    std::shared_ptr<dbus_listener_t> m_dbus_listener;
};

#endif //XENMGR__H
