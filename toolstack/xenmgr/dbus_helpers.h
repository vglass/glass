//
// Glass Display
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#include "dbus_listener.h"

uuid_t
object_path_to_uuid(QString path);
QString
uuid_to_object_path(uuid_t uuid);
bool
is_valid_color(const QString &color);
bool
icon_is_null(const QString &image_path);
std::shared_ptr<QDBusConnection>
get_system_bus();
bool
qdbus_is_connected(const QDBusConnection *conn);
std::shared_ptr<xenmgr_dbus_t>
make_xenmgr_dbus(const QString &interface, const QString &path, const QDBusConnection *conn);
std::shared_ptr<xenmgr_host_dbus_t>
make_xenmgr_host_dbus(const QString &interface, const QString &path, const QDBusConnection *conn);
std::shared_ptr<xenmgr_vm_dbus_t>
make_xenmgr_vm_dbus(const QString &interface, const QString &path, const QDBusConnection *conn);
std::shared_ptr<ui_dbus_t>
make_ui_dbus(const QString &interface, const QString &path, const QDBusConnection *conn);
std::shared_ptr<xcpmd_dbus_t>
make_xcpmd_dbus(const QString &interface, const QString &path, const QDBusConnection *conn);
std::shared_ptr<dbus_listener_t>
make_dbus_listener();

bool
pending_reply_is_error(const QDBusPendingReply<qlist_t<QDBusObjectPath>> &reply);
qlist_t<QDBusObjectPath>
pending_reply_value(const QDBusPendingReply<qlist_t<QDBusObjectPath>> &reply);
