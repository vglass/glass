//
// Glass Display
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#include <dbus_helpers.h>
#include <dbus_listener.h>

#include <ui_interface.h>
#include <xcpmd_interface.h>
#include <xenmgr_host_interface.h>
#include <xenmgr_interface.h>
#include <xenmgr_vm_interface.h>

dbus_listener_t::dbus_listener_t(QObject *parent) : QObject(parent)
{
    m_dbus_connection = get_system_bus();
    m_xenmgr_dbus = make_xenmgr_dbus("com.citrix.xenclient.xenmgr", "/", m_dbus_connection.get());
    m_xenmgr_host_dbus = make_xenmgr_host_dbus("com.citrix.xenclient.xenmgr", "/host", m_dbus_connection.get());
    m_ui_dbus = make_ui_dbus("com.openxt", "/ui", m_dbus_connection.get());
    m_xcpmd_dbus = make_xcpmd_dbus("com.citrix.xenclient.xcpmd", "/", m_dbus_connection.get());

    if (!m_dbus_connection) {
        throw std::invalid_argument("No QDBusConnection");
    }

    if (!m_xenmgr_dbus) {
        throw std::invalid_argument("No xenmgr_dbus");
    }

    if (!m_xenmgr_host_dbus) {
        throw std::invalid_argument("No xenmgr_host_dbus");
    }

    if (!m_ui_dbus) {
        throw std::invalid_argument("No ui_dbus");
    }

    if (!m_xcpmd_dbus) {
        throw std::invalid_argument("No xcpmd_dbus");
    }

    if (!qdbus_is_connected(m_dbus_connection.get())) {
        throw std::runtime_error("Could not connect to dbus");
    } else {
        seam::qobj_connect(m_xenmgr_dbus.get(), SIGNAL(vm_state_changed(const QString &, const QDBusObjectPath &, const QString &, int)), this, SLOT(vm_state_changed(const QString &, const QDBusObjectPath &, const QString &, int)));
        seam::qobj_connect(m_xenmgr_dbus.get(), SIGNAL(vm_deleted(const QString &, const QDBusObjectPath &)), this, SLOT(vm_deleted(const QString &, const QDBusObjectPath &)));
        seam::qobj_connect(m_xenmgr_dbus.get(), SIGNAL(vm_name_changed(const QString &, const QDBusObjectPath &)), this, SLOT(vm_name_changed(const QString &, const QDBusObjectPath &)));

        seam::qobj_connect(m_xcpmd_dbus.get(), SIGNAL(ac_adapter_state_changed(uint)), this, SLOT(ac_adapter_state_changed(uint)));
        seam::qobj_connect(m_xcpmd_dbus.get(), SIGNAL(battery_status_changed(uint)), this, SLOT(battery_status_changed(uint)));
    }
}

void
dbus_listener_t::vm_state_changed(const QString &uuid, const QDBusObjectPath &obj_path, const QString &state, int acpi_state)
{
    uuid_t vm_uuid(uuid);

    if (m_vm_dbus_map.count(vm_uuid) == 0) {
        m_vm_dbus_map[vm_uuid] = make_xenmgr_vm_dbus("com.citrix.xenclient.xenmgr", obj_path.path(), m_dbus_connection.get());
    }

    if (m_vm_dbus_map[vm_uuid]->vglass_enabled()) {
        if (state == "running" && acpi_state == 0) {
            emit guest_started(vm_uuid);
        } else if (state == "running" && acpi_state == 3) {
            emit guest_slept(vm_uuid);
        } else if (state == "stopped" && acpi_state == 5) {
            emit guest_stopped(vm_uuid);
        } else if (state == "rebooted") {
            emit guest_rebooted(vm_uuid);
        }
    }
}

void
dbus_listener_t::vm_deleted(const QString &uuid, const QDBusObjectPath &obj_path)
{
    (void) obj_path;
    uuid_t vm_uuid(uuid);
    if (m_vm_dbus_map.count(vm_uuid) != 0) {
        m_vm_dbus_map.erase(vm_uuid);
    }

    emit guest_deleted(vm_uuid);
}

void
dbus_listener_t::vm_name_changed(const QString &uuid, const QDBusObjectPath &obj_path)
{
    (void) obj_path;
    uuid_t vm_uuid(uuid);
    if (m_vm_dbus_map.find(vm_uuid) != m_vm_dbus_map.end()) {
        emit guest_name_changed(vm_uuid);
    }
}

void
dbus_listener_t::ac_adapter_state_changed(uint state)
{
    emit battery_charge_state_changed(state);
}

void
dbus_listener_t::battery_status_changed(uint percentage)
{
    emit battery_percentage_changed(percentage);
}

void
dbus_listener_t::refresh_battery()
{
    emit battery_charge_state_changed(m_xcpmd_dbus->get_ac_adapter_state());
    emit battery_percentage_changed(m_xcpmd_dbus->aggregate_battery_percentage());
}

qlist_t<uuid_t>
dbus_listener_t::get_vm_list()
{
    qlist_t<uuid_t> vms = get_guest_vm_list();

    foreach (auto uuid, vms) {
        try {
            get_domid(uuid);
        } catch (std::runtime_error &e) {
            // No domid so it's not running yet
            vms.removeAll(uuid);
        }
    }

    return vms;
}

qlist_t<uuid_t>
dbus_listener_t::get_guest_vm_list()
{
    QDBusPendingReply<qlist_t<QDBusObjectPath>> reply = m_xenmgr_dbus->list_vms();

    reply.waitForFinished();

    if (pending_reply_is_error(reply)) {
        throw std::runtime_error("Error getting list of vms");
    }

    qlist_t<uuid_t> vms;
    qlist_t<QDBusObjectPath> vm_list = pending_reply_value(reply);

    for (auto vm : vm_list) {
        uuid_t uuid = object_path_to_uuid(vm.path());

        if (m_vm_dbus_map.count(uuid) == 0) {
            m_vm_dbus_map[uuid] = make_xenmgr_vm_dbus("com.citrix.xenclient.xenmgr", vm.path(), m_dbus_connection.get());
        }

        if (m_vm_dbus_map[uuid]->vglass_enabled()) {
            vms << uuid;
        }
    }

    return vms;
}

domid_t
dbus_listener_t::get_domid(uuid_t uuid)
{
    if (m_vm_dbus_map.count(uuid) == 0) {
        throw std::out_of_range("No matching uuid in map");
    }

    domid_t domid = m_vm_dbus_map[uuid]->domid();
    if (domid >= domid_t(DOMID_FIRST_RESERVED)) {
        throw std::runtime_error(std::to_string(m_vm_dbus_map[uuid]->domid()));
    }

    return domid;
}

domid_t
dbus_listener_t::get_stub_domid(uuid_t uuid)
{
    (void) uuid;
    if (m_vm_dbus_map.count(uuid) == 0) {
        throw std::out_of_range("No matching uuid in map");
    }

    domid_t domid = m_vm_dbus_map[uuid]->stub_domid();
    if (m_vm_dbus_map[uuid]->stubdom() && domid >= domid_t(DOMID_FIRST_RESERVED)) {
        throw std::runtime_error(std::to_string(m_vm_dbus_map[uuid]->stub_domid()));
    }
    return domid;
}

QString
dbus_listener_t::get_name(uuid_t uuid)
{
    if (m_vm_dbus_map.count(uuid) == 0) {
        throw std::out_of_range("No matching uuid in map");
    }

    return m_vm_dbus_map[uuid]->name();
}

QString
dbus_listener_t::get_os(uuid_t uuid)
{
    if (m_vm_dbus_map.count(uuid) == 0) {
        throw std::out_of_range("No matching uuid in map");
    }

    return m_vm_dbus_map[uuid]->os();
}

QString
dbus_listener_t::get_long_form(uuid_t uuid)
{
    if (m_vm_dbus_map.count(uuid) == 0) {
        throw std::out_of_range("No matching uuid in map");
    }

    return m_vm_dbus_map[uuid]->long_form_vg();
}

QString
dbus_listener_t::get_short_form(uuid_t uuid)
{
    if (m_vm_dbus_map.count(uuid) == 0) {
        throw std::out_of_range("No matching uuid in map");
    }

    return m_vm_dbus_map[uuid]->short_form_vg();
}

QString
dbus_listener_t::get_text_color(uuid_t uuid)
{
    if (m_vm_dbus_map.count(uuid) == 0) {
        throw std::out_of_range("No matching uuid in map");
    }

    QString color = m_vm_dbus_map[uuid]->text_color_vg();
    if (!is_valid_color(color)) {
        return "#000000";
    }

    return color;
}

int
dbus_listener_t::get_border_width(uuid_t uuid)
{
    if (m_vm_dbus_map.count(uuid) == 0) {
        throw std::out_of_range("No matching uuid in map");
    }

    int border_width = m_vm_dbus_map[uuid]->border_width_vg();
    if (border_width < 0) {
        throw std::runtime_error(std::to_string(border_width));
    }

    return border_width;
}

int
dbus_listener_t::get_border_height(uuid_t uuid)
{
    if (m_vm_dbus_map.count(uuid) == 0) {
        throw std::out_of_range("No matching uuid in map");
    }

    int border_height = m_vm_dbus_map[uuid]->border_height_vg();
    if (border_height < 0) {
        throw std::runtime_error(std::to_string(border_height));
    }

    return border_height;
}

bool
dbus_listener_t::get_stubdom(uuid_t uuid)
{
    if (m_vm_dbus_map.count(uuid) == 0) {
        throw std::out_of_range("No matching uuid in map");
    }

    return m_vm_dbus_map[uuid]->stubdom();
}

int
dbus_listener_t::get_slot(uuid_t uuid)
{
    if (m_vm_dbus_map.count(uuid) == 0) {
        throw std::out_of_range("No matching uuid in map");
    }

    return m_vm_dbus_map[uuid]->slot();
}

QString
dbus_listener_t::get_image_path(uuid_t uuid)
{
    if (m_vm_dbus_map.count(uuid) == 0) {
        throw std::out_of_range("No matching uuid in map");
    }

    QString image_path = m_vm_dbus_map[uuid]->image_path_vg().prepend(image_path_prepend);

    if (icon_is_null(image_path)) {
        image_path = default_image_path;
    }

    return image_path;
}

QString
dbus_listener_t::get_gpu(uuid_t uuid)
{
    if (m_vm_dbus_map.count(uuid) == 0) {
        throw std::out_of_range("No matching uuid in map");
    }

    return m_vm_dbus_map[uuid]->gpu();
}

QString
dbus_listener_t::get_primary_domain_color(uuid_t uuid)
{
    if (m_vm_dbus_map.count(uuid) == 0) {
        throw std::out_of_range("No matching uuid in map");
    }

    QString color = m_vm_dbus_map[uuid]->primary_domain_color();
    if (!is_valid_color(color)) {
        return "#FFFFFF";
    }

    return color;
}

QString
dbus_listener_t::get_secondary_domain_color(uuid_t uuid)
{
    if (m_vm_dbus_map.count(uuid) == 0) {
        throw std::out_of_range("No matching uuid in map");
    }

    QString color = m_vm_dbus_map[uuid]->secondary_domain_color();
    if (!is_valid_color(color)) {
        return get_primary_domain_color(uuid);
    }

    return color;
}

bool
dbus_listener_t::get_laptop()
{
    return m_xenmgr_host_dbus->laptop();
}

bool
dbus_listener_t::get_display_power_indicator()
{
    return m_ui_dbus->get_display_power_indicator();
}

bool
dbus_listener_t::get_display_configurable()
{
    return m_xenmgr_host_dbus->display_configurable();
}
