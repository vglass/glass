//
// Glass Display
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#include <dbus_helpers.h>

#include <QImage>
#include <ui_interface.h>
#include <xcpmd_interface.h>
#include <xenmgr_host_interface.h>
#include <xenmgr_interface.h>
#include <xenmgr_vm_interface.h>

//HELPERS
/* Convert object path to uuid
    "/vm/xxxxxxxx_xxxx_xxxx_xxxx_xxxxxxxxxxxx" -> "{xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx}"
*/
uuid_t
object_path_to_uuid(QString path)
{
    return path.replace('_', '-').section('/', -1);
}

/* Convert uuid to vm path
   "{xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx}" -> "/vm/xxxxxxxx_xxxx_xxxx_xxxx_xxxxxxxxxxxx"
*/
QString
uuid_to_object_path(uuid_t uuid)
{
    return QString(QString("/vm/").append(uuid.toString().remove('{').remove('}').replace('-', '_')));
}

/* Check that a QString Color is valid
    "#000" or "#000000" or "black"
    "#FFF" or "#ffffff" or "white"
*/
bool
is_valid_color(const QString &color)
{
    return QColor(color).isValid();
}

/* Check that a image_path is a valid icon*/
bool
icon_is_null(const QString &image_path)
{
    return QImage(image_path, "PNG").isNull();
}

// Seam for QDBusConnection::systemBus()
std::shared_ptr<QDBusConnection>
get_system_bus()
{
    return std::make_shared<QDBusConnection>(QDBusConnection::systemBus());
}

// Seam for the connection call
bool
qdbus_is_connected(const QDBusConnection *conn)
{
    return conn->isConnected();
}

// Seam for making a ptr for m_xenmgr_dbus
std::shared_ptr<xenmgr_dbus_t>
make_xenmgr_dbus(const QString &interface, const QString &path, const QDBusConnection *conn)
{
    return std::make_shared<xenmgr_dbus_t>(interface, path, *conn);
}

// Seam for making a ptr for m_xenmgr_host_dbus
std::shared_ptr<xenmgr_host_dbus_t>
make_xenmgr_host_dbus(const QString &interface, const QString &path, const QDBusConnection *conn)
{
    return std::make_shared<xenmgr_host_dbus_t>(interface, path, *conn);
}

std::shared_ptr<ui_dbus_t>
make_ui_dbus(const QString &interface, const QString &path, const QDBusConnection *conn)
{
    return std::make_shared<ui_dbus_t>(interface, path, *conn);
}

std::shared_ptr<xcpmd_dbus_t>
make_xcpmd_dbus(const QString &interface, const QString &path, const QDBusConnection *conn)
{
    return std::make_shared<xcpmd_dbus_t>(interface, path, *conn);
}

// Seam for making a ptr for m_vm_dbus
std::shared_ptr<xenmgr_vm_dbus_t>
make_xenmgr_vm_dbus(const QString &interface, const QString &path, const QDBusConnection *conn)
{
    return std::make_shared<xenmgr_vm_dbus_t>(interface, path, *conn);
}

std::shared_ptr<dbus_listener_t>
make_dbus_listener()
{
    return std::make_shared<dbus_listener_t>();
}

bool
pending_reply_is_error(const QDBusPendingReply<qlist_t<QDBusObjectPath>> &reply)
{
    return reply.isError();
}

qlist_t<QDBusObjectPath>
pending_reply_value(const QDBusPendingReply<qlist_t<QDBusObjectPath>> &reply)
{
    return reply.value();
}
