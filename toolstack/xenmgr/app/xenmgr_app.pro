QT += core dbus

include(../../../common_include.prx)

CONFIG += c++14

INCLUDEPATH += "../../include"
INCLUDEPATH += "../"

HEADERS += ../../include/toolstack.h
HEADERS += ../../include/vm.h

HEADERS += ../xenmgr_dbus.h
SOURCES += ../xenmgr_dbus.cpp

HEADERS += ../xenmgr_vm_dbus.h
SOURCES += ../xenmgr_vm_dbus.cpp

HEADERS += ../dbus_helpers.h
SOURCES += ../dbus_helpers.cpp

HEADERS += ../dbus_listener.h
SOURCES += ../dbus_listener.cpp

HEADERS += ../xenmgr.h
SOURCES += ../xenmgr.cpp

SOURCES += xenmgr_main.cpp
