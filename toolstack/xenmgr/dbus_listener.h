//
// Glass Display
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#ifndef XENMGR_DBUS__H
#define XENMGR_DBUS__H

#include <glass_types.h>

#include <QColor>
#include <QIcon>
#include <QtCore>
#include <QtDBus>

#include <qobj.h>

#include <xen/xen.h>

const QString image_path_prepend = "/usr/lib/xui/";
const QString default_image_path = "/usr/lib/xui/plugins/vmimages/green.png";

class xenmgr_dbus_t;
class xenmgr_vm_dbus_t;
class xenmgr_host_dbus_t;
class ui_dbus_t;
class xcpmd_dbus_t;

class dbus_listener_t : public QObject
{
    Q_OBJECT
public:
    explicit dbus_listener_t(QObject *parent = 0);

    // VM level
    virtual qlist_t<uuid_t> get_vm_list();
    virtual qlist_t<uuid_t> get_guest_vm_list();
    virtual domid_t get_domid(uuid_t);
    virtual domid_t get_stub_domid(uuid_t);
    virtual QString get_name(uuid_t uuid);
    virtual QString get_os(uuid_t uuid);
    virtual QString get_long_form(uuid_t uuid);
    virtual QString get_short_form(uuid_t uuid);
    virtual QString get_text_color(uuid_t uuid);
    virtual int get_border_width(uuid_t uuid);
    virtual int get_border_height(uuid_t uuid);
    virtual bool get_stubdom(uuid_t uuid);
    virtual int get_slot(uuid_t uuid);
    virtual QString get_image_path(uuid_t uuid);
    virtual QString get_gpu(uuid_t uuid);
    virtual QString get_primary_domain_color(uuid_t uuid);
    virtual QString get_secondary_domain_color(uuid_t uuid);

    // Host level
    virtual bool get_display_configurable();
    virtual bool get_laptop();

    // UI

    virtual bool get_display_power_indicator();

public slots:
    virtual void vm_state_changed(const QString &uuid, const QDBusObjectPath &obj_path, const QString &state, int acpi_state);
    virtual void vm_deleted(const QString &uuid, const QDBusObjectPath &obj_path);
    virtual void vm_name_changed(const QString &uuid, const QDBusObjectPath &obj_path);
    void ac_adapter_state_changed(uint state);
    void battery_status_changed(uint percentage);
    void refresh_battery(void);

signals:
    void guest_started(const uuid_t &uuid);
    void guest_stopped(const uuid_t &uuid);
    void guest_slept(const uuid_t &uuid);
    void guest_deleted(const uuid_t &uuid);
    void guest_rebooted(const uuid_t &uuid);
    void guest_name_changed(const uuid_t &uuid);
    void battery_charge_state_changed(uint state);
    void battery_percentage_changed(uint percentage);

private:
    std::shared_ptr<QDBusConnection> m_dbus_connection;
    std::shared_ptr<xenmgr_dbus_t> m_xenmgr_dbus;
    std::shared_ptr<xenmgr_host_dbus_t> m_xenmgr_host_dbus;
    std::shared_ptr<ui_dbus_t> m_ui_dbus;
    std::shared_ptr<xcpmd_dbus_t> m_xcpmd_dbus;
    std::map<uuid_t, std::shared_ptr<xenmgr_vm_dbus_t>> m_vm_dbus_map;
};
#endif
