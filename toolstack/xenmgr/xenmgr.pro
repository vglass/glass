QT += core

include(../../common_include.prx)
include(../../drm_include.prx)

CONFIG += c++14

TARGET = xenmgr_toolstack

TEMPLATE = lib
DESTDIR = $$VG_BASE_DIR/lib

DBUS_INTERFACES += xenmgr
xenmgr.files = xenmgr.xml
xenmgr.source_flags = -c xenmgr_dbus_t -N
xenmgr.header_flags = -c xenmgr_dbus_t -N
DBUS_INTERFACES += host
host.files = xenmgr_host.xml
host.source_flags = -c xenmgr_host_dbus_t -N
host.header_flags = -c xenmgr_host_dbus_t -N
DBUS_INTERFACES += vm
vm.files = xenmgr_vm.xml
vm.source_flags = -c xenmgr_vm_dbus_t -N
vm.header_flags = -c xenmgr_vm_dbus_t -N
DBUS_INTERFACES += ui
ui.files = ui.xml
ui.source_flags = -c ui_dbus_t -N
ui.header_flags = -c ui_dbus_t -N
DBUS_INTERFACES += xcpmd
xcpmd.files = xcpmd.xml
xcpmd.source_flags = -c xcpmd_dbus_t -N
xcpmd.header_flags = -c xcpmd_dbus_t -N
SOURCES += xenmgr.cpp 
SOURCES += dbus_listener.cpp
SOURCES += dbus_helpers.cpp

HEADERS += xenmgr.h
HEADERS += dbus_listener.h
HEADERS += dbus_helpers.h
HEADERS += ../include/toolstack.h
HEADERS += ../include/vm.h
HEADERS += ../include/vm_base.h

INCLUDEPATH += "../include"

target.path = /usr/lib
INSTALLS += target

xenmgr_headers.path = /usr/include/toolstack/xenmgr
xenmgr_headers.files = \
	dbus_helpers.h \
	dbus_listener.h
INSTALLS += xenmgr_headers
