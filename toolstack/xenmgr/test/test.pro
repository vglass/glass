QT += core dbus

include(../../../common_include.prx)

CONFIG += c++14

QMAKE_CXXFLAGS += -fprofile-arcs -ftest-coverage
QMAKE_LDFLAGS += -fprofile-arcs -ftest-coverage
LIBS += -lgcov

INCLUDEPATH += ../ ../../ ../../../common/test/

DBUS_INTERFACES += xenmgr
xenmgr.files = ../xenmgr.xml
xenmgr.source_flags = -c xenmgr_dbus_t -N
xenmgr.header_flags = -c xenmgr_dbus_t -N
DBUS_INTERFACES += host
host.files = ../xenmgr_host.xml
host.source_flags = -c xenmgr_host_dbus_t -N
host.header_flags = -c xenmgr_host_dbus_t -N
DBUS_INTERFACES += vm
vm.files = ../xenmgr_vm.xml
vm.source_flags = -c xenmgr_vm_dbus_t -N
vm.header_flags = -c xenmgr_vm_dbus_t -N

dbus.commands += sed -i \'s|inline|inline virtual|\' xenmgr_interface.h;
dbus.commands += sed -i \'s|static inline virtual const|static inline const|\' xenmgr_interface.h;

dbus.commands += sed -i \'s|inline|inline virtual|\' xenmgr_vm_interface.h;
dbus.commands += sed -i \'s|static inline virtual const|static inline const|\' xenmgr_vm_interface.h;

dbus.commands += sed -i \'s|inline|inline virtual|\' xenmgr_host_interface.h;
dbus.commands += sed -i \'s|static inline virtual const|static inline const|\' xenmgr_host_interface.h;

dbus.commands += touch interfaces.done

dbus.depends += xenmgr_interface.h
dbus.depends += xenmgr_vm_interface.h
dbus.depends += xenmgr_host_interface.h

dbus.target = interfaces.done

PRE_TARGETDEPS += interfaces.done

QMAKE_EXTRA_TARGETS += dbus

QMAKE_CLEAN += interfaces.done

HEADERS += ../dbus_listener.h
SOURCES += ../dbus_listener.cpp

HEADERS += ../dbus_helpers.h
SOURCES += ../dbus_helpers.cpp

HEADERS += ../../include/vm.h
HEADERS += ../../include/toolstack.h

DEFINES += TEST

HEADERS += ../xenmgr.h
SOURCES += ../xenmgr.cpp

SOURCES += test.cpp

QMAKE_CLEAN += *.gcda *.gcno *.gcov
