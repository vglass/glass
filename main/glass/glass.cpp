//
// Glass Display
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#include <QGuiApplication>
#include <fstream>
#include <iostream>
#include <signal.h>
#include <string>
#include <unistd.h>

#include <fs_vm_region_factory.h>
#include <pv_vm_input_factory.h>
#include <pv_vm_render_factory.h>

#define chosen_renderer(x) x##_renderer_t

#include <input/input_server/input_server.h>
#ifdef VNC_RENDERER
#include <renderer/vnc_renderer/vnc_renderer.h>
typedef chosen_renderer(vnc) r;
#else
#include <renderer/dumb_renderer/dumb_renderer.h>
typedef chosen_renderer(dumb) r;
#endif

#include <logging.h>
#include <toolstack/xenmgr/xenmgr.h>
#include <window_manager/fs_window_manager/fs_window_manager.h>

// const QString PIDFILE = QString("/var/run/vglass.pid");

class cmd_line_options
{
public:
    cmd_line_options() = default;
    ~cmd_line_options() = default;
    bool standalone_mode_enabled{0};
    uint32_t host_hotplug_test{0};
    std::string display_config{""};
    bool dump_display_config{false};
    std::string input_config{""};
    bool debugging_enabled{false};
    bool syslog_logging_enabled{false};
    bool enable_multitouch{false};
};

QGuiApplication *app_ref = NULL;
cmd_line_options cmd_options;
std::list<std::string> gpu_devices;

char **g_argv = NULL;
int reload = 0;

void
signal_handler(int signal)
{
    switch (signal) {
        case SIGHUP:
        case SIGUSR1:
        case SIGINT:
        case SIGTERM: {
            if (app_ref) {
                reload = -1;
                app_ref->quit();
            }
            break;
        }
        default:
            std::cout << signal;
            break;
    }
}

void
parse_command_line(QCommandLineParser &parser, QCoreApplication &app, cmd_line_options &opts)
{
    parser.setApplicationDescription("Glass Window Manager");
    parser.addHelpOption();
    parser.addVersionOption();

    // Left in to show how to do these with parameters
    // QCommandLineOption configOption(QStringList() << "c" << "config",
    //                                 "location of display configuration",
    //                                 "url", "db:///disman");
    // parser.addOption(configOption);

    QCommandLineOption standalone_option(QStringList() << "s"
                                                       << "standalone",
                                         "enable standalone mode");
    QCommandLineOption host_hotplug_test_option(QStringList() << "p"
                                                              << "host-hotplug-test",
                                                "Simulate a host hotplug every <n> seconds",
                                                "n");
    QCommandLineOption display_config_option(QStringList() << "d"
                                                           << "display-config",
                                             "Read display config from <file>",
                                             "file");
    QCommandLineOption dump_display_config_option(QStringList() << "e"
                                                                << "dump-display-config",
                                                  "Dump enumerated display config to stdout and exit");
    QCommandLineOption input_config_option(QStringList() << "i"
                                                         << "input-config",
                                           "Read input config from <file>",
                                           "file");
    QCommandLineOption devices_option(QStringList() << "devices",
                                      "Devices to start UIVM on",
                                      "device_list");
    QCommandLineOption debug_option(QStringList() << "debug",
                                    "enable debug/verbose logging");
    QCommandLineOption syslog_logging_option(QStringList() << "disable-syslog",
                                             "use console logging instead of syslog");
    QCommandLineOption enable_multitouch_option(QStringList() << "multitouch",
                                                "enable multitouch support");

    parser.addOption(standalone_option);
    parser.addOption(host_hotplug_test_option);
    parser.addOption(display_config_option);
    parser.addOption(dump_display_config_option);
    parser.addOption(input_config_option);
    parser.addOption(devices_option);
    parser.addOption(debug_option);
    parser.addOption(syslog_logging_option);
    parser.addOption(enable_multitouch_option);

    QStringList args = app.arguments();
    if (!parser.parse(args)) {
        vg_info() << "Invalid arguments:" << parser.unknownOptionNames();
    }

    opts.standalone_mode_enabled = parser.isSet(standalone_option);
    opts.host_hotplug_test = parser.value(host_hotplug_test_option).toInt();
    opts.display_config = parser.value(display_config_option).toStdString();
    opts.dump_display_config = parser.isSet(dump_display_config_option);
    opts.input_config = parser.value(input_config_option).toStdString();
    opts.debugging_enabled = parser.isSet(debug_option);
    opts.syslog_logging_enabled = !parser.isSet(syslog_logging_option);
    opts.enable_multitouch = parser.isSet(enable_multitouch_option);

    if (parser.isSet(devices_option)) {
        // Grab all occurances of --devices from the command line
        // Build one big list of all tokens
        // Validate that they are valid unsigned integers
        QStringList str_list = parser.values(devices_option);

        QRegExp rx("(\\,)");
        for (auto str : str_list) {
            for (auto str2 : str.split(rx)) {
                bool success = false;
                str2 = str2.trimmed();
                str2.toUInt(&success, 10);
                if (success) {
                    gpu_devices.push_back(str2.toStdString());
                }
            }
        }
    }
    // If we found nothing, default to 0
    if (gpu_devices.empty()) {
        gpu_devices.push_back("0");
    }
}

int
main_loop(int argc, const char **argv)
{
    int rc = 0;
    QGuiApplication app(argc, (char **) argv);
    std::unique_ptr<input_server_t> input = nullptr;
    std::unique_ptr<fs_window_manager_t> window_manager = nullptr;

    std::unique_ptr<renderer_t> renderer = nullptr;
    std::unique_ptr<xenmgr_t> toolstack = nullptr;
    app_ref = &app;
    json config;

    // Set stuff up for command line parsing
    QGuiApplication::setApplicationName("Glass");
    QGuiApplication::setApplicationVersion("1.0");
    QCommandLineParser parser;
    // Parse command line
    parse_command_line(parser, app, cmd_options);

    qInstallMessageHandler(Logging::logOutput);

    if (Logging::logger()) {
        Logging::logger()->syslogMode = cmd_options.syslog_logging_enabled;
        Logging::logger()->debugMode = cmd_options.debugging_enabled;
    }

    QTimer *reset = new QTimer(nullptr);

    config = public_get_config(gpu_devices);

    if (cmd_options.dump_display_config) {
        std::cout << config.dump(4) << std::endl;
        exit(0);
    }

    // Register our pid here, so the correct pid is in the pidfile, allowing our
    // scripts to properly work.
    // QFile pid_file(PIDFILE);
    // if(pid_file.exists()) {
    //     qCritical().nospace() << "The PID file (" << PIDFILE << ") already exists. Either vglass is already running, or it crashed unexpectedly.";
    //     std::exit(-1);
    // }

    // pid_t pid = getpid();
    // if (pid_file.open(QIODevice::ReadWrite)) {
    //     QTextStream stream(&pid_file);
    //     stream << QString::number((unsigned int)pid) << endl;
    // } else {
    //     vg_info() << "Failed to write pid file. Perhaps SELinux is preventing writes to /var/run";
    // }
    // pid_file.close();

    window_manager = std::make_unique<fs_window_manager_t>();
    window_manager->set_multitouch_enable(cmd_options.enable_multitouch);
    //vg_debug() << "glass::main multitouch enable set to: " << window_manager->get_multitouch_enable();

    if (!cmd_options.input_config.empty()) {
        input = std::make_unique<input_server_t>(*window_manager, cmd_options.input_config);
    } else {
        input = std::make_unique<input_server_t>(*window_manager);
    }

    if (!cmd_options.display_config.empty()) {
        renderer = std::make_unique<r>(*window_manager, cmd_options.display_config);
    } else {
        if (cmd_options.standalone_mode_enabled) {
            renderer = std::make_unique<r>(*window_manager, config);
        } else {
            renderer = std::make_unique<r>(*window_manager);
        }
    }

    toolstack = std::make_unique<xenmgr_t>(std::make_shared<pv_vm_render_factory_t>(*renderer),
                                           std::make_shared<fs_vm_region_factory_t>(*window_manager),
                                           std::make_shared<pv_vm_input_factory_t>(*window_manager, *input));

    QObject::connect(toolstack.get(), &xenmgr_t::remove_guest, renderer.get(), &renderer_t::remove_guest);
    QObject::connect(renderer.get(), &renderer_t::hotplug, toolstack.get(), &xenmgr_t::reset, Qt::QueuedConnection);
    QObject::connect(toolstack.get(), &xenmgr_t::remove_guest, input.get(), &input_t::remove_guest);
    QObject::connect(toolstack.get(), &xenmgr_t::remove_guest, window_manager.get(), &fs_window_manager_t::remove_guest);
    QObject::connect(toolstack.get(), &xenmgr_t::reboot_guest, input.get(), &input_t::reboot_guest);
    QObject::connect(toolstack.get(), &xenmgr_t::wake_up_guest, input.get(), &input_t::wake_up_guest);
    QObject::connect(toolstack.get(), &xenmgr_t::sleep_guest, input.get(), &input_t::sleep_guest);
    QObject::connect(toolstack.get(), &xenmgr_t::battery_charge_state_change, window_manager.get(), &fs_window_manager_t::ac_adapter_change);
    QObject::connect(toolstack.get(), &xenmgr_t::battery_percentage_change, window_manager.get(), &fs_window_manager_t::battery_percentage_change);
    QObject::connect(window_manager.get(), &fs_window_manager_t::battery_refresh, toolstack.get(), &xenmgr_t::battery_refresh);

    QObject::connect(input.get(), &input_server_t::show_vm_switcher, window_manager.get(), &fs_window_manager_t::show_vm_switcher);
    QObject::connect(input.get(), &input_server_t::hide_vm_switcher, window_manager.get(), &fs_window_manager_t::hide_vm_switcher);
    QObject::connect(input.get(), &input_server_t::advance_switcher_right, window_manager.get(), &fs_window_manager_t::advance_vm_right);
    QObject::connect(input.get(), &input_server_t::advance_switcher_left, window_manager.get(), &fs_window_manager_t::advance_vm_left);
    QObject::connect(input.get(), &input_server_t::center_mouse, window_manager.get(), &window_manager_t::center_mouse);
    QObject::connect(window_manager.get(), &fs_window_manager_t::set_highlight_vm, input.get(), &input_server_t::set_highlight_vm);

    QObject::connect(input.get(), &input_server_t::heartbeat, renderer.get(), &renderer_t::dpms_off);

    QObject::connect(input.get(), &input_server_t::save_screenshot, renderer.get(), &renderer_t::save_screenshot);
#ifdef DEBUG
    //QObject::connect(static_cast<r *>(renderer.get()), &r::input_event, input.get(), &input_server_t::event_slot);
#endif

    if (cmd_options.host_hotplug_test != 0) {
        QObject::connect(reset, &QTimer::timeout, renderer.get(), &renderer_t::reset);
        reset->start(cmd_options.host_hotplug_test * 1000);
    }

    toolstack->battery_refresh();
    window_manager->set_display_power(toolstack->get_laptop() && toolstack->get_display_power_indicator());

    rc = app.exec();

    // pid_file.remove();

    reset->deleteLater();

    return rc;
}

int
main(int argc, const char **argv)
{
    int rc = 0;

    // Root privileges are needed to get the code to work
    if (geteuid() != 0) {
        qCritical() << "This executable must be run with root privileges!!!";
        std::exit(1);
    }

    signal(SIGUSR1, signal_handler);
    signal(SIGHUP, signal_handler);
    signal(SIGTERM, signal_handler);
    signal(SIGINT, signal_handler);

    while (reload >= 0) {
        rc = main_loop(argc, argv);
    }

    return rc;
}
