# glass
Main backend executable 

Overview
----

Each subsystem (rendering, window management, input, toolstack) has a vm object type (vm_render_t, vm_region_t, vm_input_t, vm_base_t) which are contained within an overarching vm_t object. The toolstack module will take a factory for each of these objects, as different implementations may have different derived variants of these vm_*_t types. Each module should use the associated object for 99% of its operation. Any cross use of other modules' vm_\*_t data will have to be justified explicitly.

Dependencies
----

pv-display-helper:
```
git clone https://github.com/v-glass/pv-display-helper.git
cd pv-display-helper
make backend
make install_backend
cd ..
```
gperftools:
```
git clone https://github.com/gperftools/gperftools
cd gperftools
./autogen.sh
./configure --prefix=/usr
make
make install
cd ..
```

Measuring Performance
----
When in doubt whether an optimization is worth the time, please measure. You can use somewhat inaccurate methods to do measurements for stuff that is pretty coarse grain (is the process using 100% CPU in top?). You can also run it through a profiler, such as gperf-tools:

Run glass with the profiler:
```
CPUPROFILE=/storage/cpu.out LD_PRELOAD=/usr/lib/libprofiler.so glass < common/test/display_config.json &
```
When you are done with doing whatever action that you are interested in measuring (preferably repeatedly as the profiler uses sampling), send SIGUSR1 to the process:
```
kill -SIGUSR1 `pgrep glass`
```

Then to analyze the results:
```
pprof --text /usr/bin/glass /storage/cpu.out
```
