//
// Dumb renderer with additional VNC capabilities
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#ifndef VNC_RENDERER__H
#define VNC_RENDERER__H

#include <rfb/rfb.h>
#undef max

#include <glass_types.h>

#include <linux/uinput.h>

#include "vnc_translate.h"
#include <renderer.h>

using json = nlohmann::json;

class vnc_renderer_t;

// vnc server prototypes & definitions
class vnc_screen_t
{
public:
    vnc_screen_t(uint32_t x, uint32_t y, uint32_t w, uint32_t h, rfbScreenInfoPtr info, vnc_renderer_t *renderer) : m_x(x),
                                                                                                                    m_y(y),
                                                                                                                    m_w(w),
                                                                                                                    m_h(h),
                                                                                                                    m_info(info),
                                                                                                                    m_renderer((void *) renderer)
    {
    }
    ~vnc_screen_t() {}
    rfbScreenInfoPtr info() { return m_info; }
    uint32_t x() { return m_x; }
    uint32_t y() { return m_y; }
    uint32_t width() { return m_w; }
    uint32_t height() { return m_h; }
    vnc_renderer_t *renderer() { return (vnc_renderer_t *) m_renderer; }
    void send_key(int code, int val);
    void send_move(int x, int y);
    void send_scroll(int z);

private:
    uint32_t m_x{0};
    uint32_t m_y{0};
    uint32_t m_w{0};
    uint32_t m_h{0};

    rfbScreenInfoPtr m_info{nullptr};
    void *m_renderer;
};

class vnc_renderer_t : public renderer_t
{
    Q_OBJECT
public:
    explicit vnc_renderer_t(window_manager_t &wm);
    vnc_renderer_t(window_manager_t &wm, std::string display_config_path);
    vnc_renderer_t(window_manager_t &wm, json &display_config);
    virtual ~vnc_renderer_t();

    virtual point_t current_position() { return m_wm.input_plane()->hotspot(); }

signals:
    void input_event(xt_input_event event, std::shared_ptr<vm_input_t> target_vm = NULL);

protected:
    virtual void pre_render_display(desktop_plane_t *desktop, display_plane_t *display_plane);
    virtual void post_render_display(desktop_plane_t *desktop, display_plane_t *display_plane);

private:
    virtual void initialize_vnc_servers(void);

    list_t<vnc_screen_t *> m_screens;
};

/*
 * Local variables:
 * mode: C++
 * c-basic-offset: 4
 * indent-tabs-mode: nil
 * End:
 */
#endif // VNC_RENDERER__H
