//
// Glass Display
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#include "vnc_renderer.h"
#include "xt_input_global.h"

enum rfbNewClientAction
new_vnc_client(rfbClientPtr cl);
void
vnc_client_close(rfbClientPtr cl);
void
handle_vnc_key_event(rfbBool down, rfbKeySym key, rfbClientPtr cl);
void
handle_vnc_mouse_event(int buttonMask, int x, int y, rfbClientPtr cl);

void
vnc_screen_t::send_key(int code, int val)
{
    xt_input_event current_event;
    memset(&current_event, 0, sizeof(current_event));
    current_event.type = XT_TYPE_KEY;
    current_event.flags = val;
    current_event.keyCode = code;
    emit((vnc_renderer_t *) m_renderer)->input_event(current_event);
}

void
vnc_screen_t::send_move(int x, int y)
{
    xt_input_event current_event;
    memset(&current_event, 0, sizeof(current_event));
    current_event.type = XT_TYPE_ABSOLUTE;
    current_event.data.absolute.absX = x;
    current_event.data.absolute.absY = y;
    emit((vnc_renderer_t *) m_renderer)->input_event(current_event);
}

void
vnc_screen_t::send_scroll(int z)
{
    xt_input_event current_event;
    memset(&current_event, 0, sizeof(current_event));
    current_event.type = XT_TYPE_RELATIVE;
    current_event.data.relative.relZ = z;
    emit((vnc_renderer_t *) m_renderer)->input_event(current_event);
}

enum rfbNewClientAction
new_vnc_client(rfbClientPtr cl)
{
    vnc_screen_t *screen = ((vnc_screen_t *) cl->screen->screenData);
    vnc_renderer_t *renderer = screen->renderer();
    point_t current_cursor = renderer->current_position();
    screen->send_move(current_cursor.x(), current_cursor.y());
    cl->clientData = (void *) screen;
    cl->clientGoneHook = vnc_client_close;
    return RFB_CLIENT_ACCEPT;
}

void
vnc_client_close(rfbClientPtr cl)
{
    if (cl && cl->clientData) {
        cl->clientData = nullptr;
    }
}

void
handle_vnc_key_event(rfbBool down, rfbKeySym key, rfbClientPtr cl)
{
    vnc_screen_t *screen = ((vnc_screen_t *) cl->screen->screenData);

    uint32_t key_translated;

    key_translated = vnc_key_translate(key);

    screen->send_key(key_translated, (bool) down);
    rfbProcessEvents(screen->info(), 1000);
}

void
handle_vnc_mouse_event(int buttonMask, int x, int y, rfbClientPtr cl)
{
    vnc_screen_t *screen = ((vnc_screen_t *) cl->clientData);

    // buttonMask indicates which mouse buttons are pressed, if any
    // Each bit is a different button (0 = not pressed, 1 = pressed)
    screen->send_key(BTN_LEFT, (bool) (buttonMask & (1 << VNC_LEFT_CLICK_BIT)));
    screen->send_key(BTN_RIGHT, (bool) (buttonMask & (1 << VNC_RIGHT_CLICK_BIT)));

    if ((buttonMask & (1 << VNC_SCROLL_UP_BIT))) {
        screen->send_scroll(-1);
    }

    if ((buttonMask & (1 << VNC_SCROLL_DOWN_BIT))) {
        screen->send_scroll(1);
    }

    // since VNC clients are localized to a single screen, we need to
    // offset our vnc coordinates with the display's x & y offsets. these
    // offsets are equal to desktop offset + display offset at the time
    // of the VNC server's initialization.
    x += screen->x();
    y += screen->y();

    screen->send_move(x, y);

    rfbDefaultPtrAddEvent(buttonMask, x, y, cl);
    rfbProcessEvents(screen->info(), 1000);
}

vnc_renderer_t::vnc_renderer_t(window_manager_t &wm) : renderer_t(wm)
{
    initialize_vnc_servers();
}

vnc_renderer_t::vnc_renderer_t(window_manager_t &wm,
                               std::string display_config_path) : renderer_t(wm, display_config_path)
{
    initialize_vnc_servers();
}

vnc_renderer_t::vnc_renderer_t(window_manager_t &wm,
                               json &display_config) : renderer_t(wm, display_config)
{
    initialize_vnc_servers();
}

vnc_renderer_t::~vnc_renderer_t(void)
{
    // Clean up vnc server
    for (auto &screen : m_screens) {
        rfbScreenCleanup(screen->info());
    }
}

void
vnc_renderer_t::initialize_vnc_servers()
{
    uint16_t port = 5900;
    // Create the input node that the server will use
    // to handle input events

    for (auto &desktop : m_wm.input_plane()->desktops()) {
        for (auto &display : desktop->displays()) {
            // Initialize the VNC screen
            // Note we are hardcoding values for the bits per sample (8), the
            // samples per pixel (3), and the bytes per pixel (4), as these are
            // typical default values for VNC clients
            uint32_t x, y, w, h;

            x = display->origin().x() + desktop->origin().x();
            y = display->origin().y() + desktop->origin().y();
            w = display->rect().width();
            h = display->rect().height();

            rfbScreenInfoPtr rfb_screen = rfbGetScreen(NULL, NULL, w, h, 8, 3, 4);
            vnc_screen_t *screen = new vnc_screen_t(x, y, w, h, rfb_screen, this);
            rfb_screen->desktopName = strdup(display->name().c_str());
            rfb_screen->alwaysShared = true;
            rfb_screen->screenData = screen;

            // Set a callback for initializing new clients
            rfb_screen->newClientHook = new_vnc_client;
            rfb_screen->port = port++;

            // Set callbacks for processing keyboard & mouse events
            rfb_screen->kbdAddEvent = handle_vnc_key_event;
            rfb_screen->ptrAddEvent = handle_vnc_mouse_event;

            // Initialize the server
            rfbInitServer(rfb_screen);
            m_screens.push_back(screen);
        }
    }
}

void
vnc_renderer_t::pre_render_display(desktop_plane_t *desktop, display_plane_t *display_plane)
{
    (void) desktop;
    (void) display_plane;
}

void
vnc_renderer_t::post_render_display(desktop_plane_t *desktop, display_plane_t *display_plane)
{
    (void) desktop;

    // Make sure our vnc server buffer is up to date
    for (auto &screen : m_screens) {
        int32_t x = display_plane->origin().x() + desktop->origin().x();
        int32_t y = display_plane->origin().y() + desktop->origin().y();

        if (x == (int32_t) screen->x() && y == (int32_t) screen->y()) {
            screen->info()->frameBuffer = (char *) display_plane->framebuffer()->image().get()->bits();
            screen->info()->serverFormat.redShift = 16;
            screen->info()->serverFormat.blueShift = 0;
            // Mark clipped regions as modified so we can update
            // any listening vnc clients
            uint32_t x1, x2, y1, y2;
            for (auto rect = display_plane->dirty_bitmap().begin(); rect != display_plane->dirty_bitmap().end(); rect++) {
                x1 = rect->topLeft().x();
                y1 = rect->topLeft().y();
                x2 = rect->bottomRight().x();
                y2 = rect->bottomRight().y();

                rfbMarkRectAsModified(screen->info(), x1, y1, x2, y2);
            }
            rfbProcessEvents(screen->info(), 1000);
        }
    }
}

/*
 * Local variables:
 * mode: C++
 * c-basic-offset: 4
 * indent-tabs-mode: nil
 * End:
 */
