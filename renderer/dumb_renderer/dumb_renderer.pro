QT += core gui
QT -= opengl

include(../../common_include.prx)
include(../../drm_include.prx)

TARGET = dumb_renderer
DESTDIR = $$VG_BASE_DIR/lib
TEMPLATE = lib

SOURCES += dumb_renderer.cpp
SOURCES += $$VG_BASE_DIR/common/dbus.cpp
SOURCES += $$VG_BASE_DIR/renderer/common/renderer.cpp
SOURCES += $$VG_BASE_DIR/renderer/common/sort_config.cpp
SOURCES += $$VG_BASE_DIR/renderer/common/vglass_dbus_proxy.cpp
SOURCES += $$VG_BASE_DIR/renderer/common/sources/pv_vm_render_factory.cpp
SOURCES += $$VG_BASE_DIR/renderer/common/sinks/drm/drm_gpu.cpp
SOURCES += $$VG_BASE_DIR/renderer/common/sinks/drm/drm_crtc.cpp
SOURCES += $$VG_BASE_DIR/renderer/common/sinks/drm/drm_connector.cpp
SOURCES += $$VG_BASE_DIR/renderer/common/sinks/drm/drm_mode.cpp
SOURCES += $$VG_BASE_DIR/renderer/common/sinks/drm/dumb_fb.cpp
SOURCES += $$VG_BASE_DIR/renderer/common/sinks/drm/backlight.cpp
SOURCES += $$VG_BASE_DIR/renderer/common/sources/pv_guest/pv_display_resource.cpp
SOURCES += $$VG_BASE_DIR/renderer/common/sources/pv_guest/pv_desktop_resource.cpp
SOURCES += $$VG_BASE_DIR/renderer/common/sinks/pt_gpu/pt_gpu.cpp
SOURCES += $$VG_BASE_DIR/renderer/common/sinks/pt_gpu/pt_display.cpp

HEADERS += dumb_renderer.h
HEADERS += $$VG_BASE_DIR/common/include/dbus.h
HEADERS += $$VG_BASE_DIR/renderer/common/vglass_dbus_proxy.h
HEADERS += $$VG_BASE_DIR/renderer/common/sort_config.h
HEADERS += $$VG_BASE_DIR/renderer/common/sources/pv_vm_render_factory.h
HEADERS += $$VG_BASE_DIR/renderer/common/sinks/drm/drm_gpu.h
HEADERS += $$VG_BASE_DIR/renderer/common/sinks/drm/drm_crtc.h
HEADERS += $$VG_BASE_DIR/renderer/common/sinks/drm/drm_connector.h
HEADERS += $$VG_BASE_DIR/renderer/common/sinks/drm/drm_mode.h
HEADERS += $$VG_BASE_DIR/renderer/common/sinks/drm/dumb_fb.h
HEADERS += $$VG_BASE_DIR/renderer/common/sinks/drm/backlight.h
HEADERS += $$VG_BASE_DIR/renderer/common/sources/pv_guest/pv_display_resource.h
HEADERS += $$VG_BASE_DIR/renderer/common/sources/pv_guest/pv_desktop_resource.h
HEADERS += $$VG_BASE_DIR/renderer/common/sources/pv_guest/pv_vm_render.h
HEADERS += $$VG_BASE_DIR/renderer/common/sinks/pt_gpu/pt_gpu.h
HEADERS += $$VG_BASE_DIR/renderer/common/sinks/pt_gpu/pt_display.h
HEADERS += ../include/vm_render.h
HEADERS += ../include/renderer.h

INCLUDEPATH += "./"
INCLUDEPATH += "$$VG_BASE_DIR/renderer/common/sources"
INCLUDEPATH += "$$VG_BASE_DIR/renderer/common/sinks"

DBUS_ADAPTORS += vglass_dbus_proxy
vglass_dbus_proxy.files = $$VG_BASE_DIR/renderer/common/com.openxt.vglass.xml
vglass_dbus_proxy.source_flags = -l vglass_dbus_proxy
vglass_dbus_proxy.header_flags = -l vglass_dbus_proxy -i $$VG_BASE_DIR/renderer/common/vglass_dbus_proxy.h

LIBS += -lpvbackendhelper -livc

target.path = /usr/lib
INSTALLS += target
