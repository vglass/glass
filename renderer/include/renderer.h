//
// Glass Display
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#ifndef RENDERER__H
#define RENDERER__H

#include <glass_types.h>

#include <QObject>
#include <display_plane.h>
#include <gpu.h>
#include <vm.h>

#include <overlay.h>
#include <text_overlay.h>
#include <window_manager.h>

#include <drm/drm_gpu.h>
#include <pt_gpu/pt_gpu.h>

#include "vglass_dbus_proxy.h"

namespace banner
{
namespace position
{
constexpr const auto none = 1;
constexpr const auto top = 2;
constexpr const auto bottom = 3;
constexpr const auto both = 4;
}; // namespace position

constexpr const auto max_height = 50;
}; // namespace banner

using json = nlohmann::json;

json
public_get_config(std::list<std::string> devices);

int
dump_region(QRegion region);

class renderer_t : public QObject
{
    Q_OBJECT
public:
    renderer_t(window_manager_t &wm);
    renderer_t(window_manager_t &wm, std::string display_config_path);
    renderer_t(window_manager_t &wm, json &display_config);

    virtual ~renderer_t() = default;
    static uint32_t frame_time();
public slots:
    virtual void add_guest(std::shared_ptr<vm_render_t> guest);
    virtual void remove_guest(std::shared_ptr<vm_t> vm);

    virtual void identify_on(std::string name);
    virtual void identify_off(std::string name);

    virtual void dpms_on();
    virtual void dpms_off();

    virtual void refresh();
    virtual void reset();
    virtual void config_changed(json gpu_config);

    virtual void save_screenshot();

    virtual void render();

    virtual desktop_plane_t *desktop(uuid_t uuid);

signals:
    void hotplug();
    void increase_brightness();
    void decrease_brightness();
    void render_complete(bool success);

public slots:
    virtual void move_guest_cursor(uuid_t uuid, window_key_t key, point_t point);
    virtual void show_cursor(uuid_t uuid, uint32_t key, point_t point, std::shared_ptr<cursor_t> cursor);
    virtual void hide_cursor(uuid_t uuid, uint32_t key);
    virtual void render_slot();
    virtual void render_slot_uuid(const uuid_t &uuid);

private:
    bool render_vm_to_display(QPainter &painter,
                              uuid_t uuid,
                              desktop_plane_t *desktop,
                              display_plane_t *display,
                              region_t &display_region,
                              region_t &painted_region);

    virtual void render_desktop(desktop_plane_t *desktop, list_t<uuid_t> &focus_stack);
    virtual void dpms_display_on(display_plane_t &display);
    virtual void dpms_display_off(display_plane_t &display);

    template <typename Overlay>
    void render_overlays(QPainter &painter, desktop_plane_t *desktop, Overlay overlays, display_plane_t *display_plane, region_t &display_clip, region_t &painted_clip);

protected:
    virtual void pre_render_display(desktop_plane_t *desktop, display_plane_t *display_plane) = 0;
    virtual void post_render_display(desktop_plane_t *desktop, display_plane_t *display_plane) = 0;
    virtual void base_render_display(desktop_plane_t *desktop, display_plane_t *display_plane, list_t<uuid_t> &focus_stack);
    virtual void render_display(desktop_plane_t *desktop, display_plane_t *display_plane, list_t<uuid_t> &focus_stack);
    virtual void setup_gpus(json &display_config);
    virtual void setup_desktop(std::shared_ptr<desktop_plane_t> desktop_plane);
    bool current_config_matches(json &new_config);

    window_manager_t &m_wm;

    qlist_t<uuid_t> m_pinned_uuid;

    qhash_t<uuid_t, std::shared_ptr<vm_render_t>> m_guests;
    list_t<std::unique_ptr<gpu_t>> m_gpus;

    vglass_dbus_proxy m_vglass_dbus_proxy;

    QImage m_background_image;

    QTimer *m_timer;
    bool m_ready;
    bool m_cloned;
    // This should move to the window manager, just a quick hack for
    // identify_on identify_off
    list_t<std::shared_ptr<text_overlay_t>> m_text_overlays;
    json m_config; // The current disman configuration

    static uint32_t m_frame_time;

    bool m_screenblanking_state;
    bool m_config_changed = false;
};

#endif // RENDERER__H
