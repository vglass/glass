//
// Glass Display
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#ifndef DESKTOP_RESOURCE_H
#define DESKTOP_RESOURCE_H

#include "display.h"
#include "framebuffer.h"
//#include "transform.h"
#include "display_resource.h"

class desktop_resource_t
{
public:
    desktop_resource_t() {}
    virtual ~desktop_resource_t() = default;

    //    virtual void clear() = 0;
    //    virtual void remove(int32_t key) = 0;

    //    virtual void set_host_physical_displays(int32_t key, const std::shared_ptr<display_t> &display);
    //    virtual void set_host_virtual_displays(int32_t key, const std::shared_ptr<display_t> &display);
    //    virtual void set_guest_virtual_displays(int32_t key, const std::shared_ptr<display_t> &display);

    //    virtual bool is_valid(void) const = 0;
    //    virtual const std_shared<transform_t> &mapper(int32_t key);

private:
    bool m_valid;
};

#endif // DESKTOP_RESOURCE_H
