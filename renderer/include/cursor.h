//
// Glass Display
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#ifndef CURSOR__H
#define CURSOR__H

#include <QPainter>
#include <glass_types.h>
class cursor_t
{
public:
    cursor_t(point_t hotspot) : m_cursor(std::make_shared<QImage>(64, 64, QImage::Format_ARGB32)), m_hotspot(hotspot)
    {
        m_cursor->fill(Qt::transparent);
    }

    ~cursor_t()
    {
    }

    void set_cursor_image(std::shared_ptr<QImage> cursor)
    {
        QMutexLocker locker(&m_lock);
        if (cursor) {
            QPainter painter(m_cursor.get());
            painter.drawImage(point_t(0, 0), *cursor);
            painter.end();
        }
    }

    std::shared_ptr<QImage> cursor_image()
    {
        return m_cursor;
    }

    void set_hotspot(point_t hotspot)
    {
        m_hotspot = hotspot;
    }

    point_t hotspot()
    {
        return m_hotspot;
    }
    void hide_cursor()
    {
        m_enabled = false;
    }
    void show_cursor()
    {
        m_enabled = true;
    }
    bool enabled()
    {
        return m_enabled;
    }

private:
    std::shared_ptr<QImage> m_cursor;
    QMutex m_lock;
    point_t m_hotspot;
    bool m_enabled;
};

#endif //CURSOR__H
