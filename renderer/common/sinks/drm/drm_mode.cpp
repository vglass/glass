//
// Glass Display
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#include "drm_mode.h"

#include <errno.h>
#include <fcntl.h>
#include <getopt.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include <exception>
#include <iostream>

drm_mode_t::drm_mode_t(drmModeModeInfo *modeinfo) : m_modeinfo(modeinfo) { return; }

drm_mode_t::~drm_mode_t() {}

QSize
drm_mode_t::resolution(void) const
{
    return QSize(resolution_width(), resolution_height());
}

int32_t
drm_mode_t::resolution_width(void) const
{
    if (!m_modeinfo) {
        return 0;
    }

    return m_modeinfo->hdisplay;
}

int32_t
drm_mode_t::resolution_height(void) const
{
    if (!m_modeinfo)
        return 0;

    return m_modeinfo->vdisplay;
}

int32_t
drm_mode_t::refresh_rate(void) const
{
    if (!m_modeinfo)
        return 0;

    return m_modeinfo->vrefresh;
}

QString
drm_mode_t::name(bool filtered) const
{
    if (!m_modeinfo)
        return 0;

    // If filtered is set to true, we grab the width and height from the
    // mode, and then use that to construct the "name" from DRM for us.
    // This will in effect filter some of the changes that DRM makes to the
    // name itself. If filtered is off, we return the raw name, which
    // could be anything (with the word "preferred" often occurring)

    if (filtered == true) {
        return QString::number(resolution_width()) + "x" + QString::number(resolution_height());
    } else {
        return m_modeinfo->name;
    }
}

drmModeModeInfo *
drm_mode_t::modeinfo(void) const
{
    return m_modeinfo;
}
