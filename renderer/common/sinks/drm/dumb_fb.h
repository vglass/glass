//
// Glass Display
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#ifndef DUMBFB__H
#define DUMBFB__H

#include <QPainter>
#include <framebuffer.h>

extern "C" {
#include <xf86drm.h>
}

enum DEVICE_VENDOR {
    UNKNOWN = 0,
    INTEL_I915 = 100,
    AMD = 200,
    NVIDIA = 300,
    VMWARE = 400
};

class dumb_fb_t : public framebuffer_t
{
public:
    dumb_fb_t(int32_t fd, const QSize &size, QImage::Format format = QImage::Format_RGB32);

    virtual ~dumb_fb_t();

    virtual int32_t fd(void) const;

    virtual void clear();
    virtual void flush(const region_t &region);

private:
    void test_paint();

    int32_t m_drm_fd;
    size_t m_size;
    DEVICE_VENDOR m_vendor;
};

#endif // DUMBFB__H
