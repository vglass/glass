//
// Glass Display
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#include "drm_crtc.h"

drm_crtc_t::drm_crtc_t(int fd,
                       uint32_t crtc_id,
                       std::shared_ptr<drmModeRes> mode_resource,
                       uint32_t banner_height,
                       std::shared_ptr<plane_t> display_parent)
    : display_plane_t(rect_t(0, 0, 0, 0), point_t(0, 0), banner_height, display_parent),
      m_drm_fd(fd),
      m_crtc_id(crtc_id),
      m_mode_resource(mode_resource)
{
    /* Like xserver, clear out any cursors from previous users. */
    drmModeSetCursor(fd, crtc_id, 0, 0, 0);
}

drm_crtc_t::drm_crtc_t(std::shared_ptr<plane_t> display_parent) : display_plane_t(rect_t(0, 0, 0, 0), point_t(0, 0), 0, display_parent)
{
}

QString
qsize_to_resolution_string(QSize resolution)
{
    return QString::number(resolution.width()) + "x" + QString::number(resolution.height());
}

drm_crtc_t::~drm_crtc_t()
{
    drmModeSetCrtc(m_drm_fd, m_crtc_id, 0, 0, 0, NULL, 0, NULL);
    hide_cursor();
}

void
drm_crtc_t::set_position(const point_t &position)
{
    display_plane_t::set_origin(position);
}

void
drm_crtc_t::add_connector(std::shared_ptr<drm_connector_t> connector)
{
    if (connector) {
        m_compatible_connectors.append(connector);
    }
}

void
drm_crtc_t::dpms_off()
{
    for (auto connector : m_compatible_connectors) {
        // Don't pull this cursor call out of the loop, it's really
        // a fence to work around atomic drm drivers having insufficient
        // timeouts for page flips for the legacy API
        hide_cursor();
        connector->dpms_off();
    }
}

void
drm_crtc_t::dpms_on()
{
    for (auto connector : m_compatible_connectors) {
        connector->dpms_on();
        // Don't pull this cursor call out of the loop, it's really
        // a fence to work around atomic drm drivers having insufficient
        // timeouts for page flips for the legacy API
        show_cursor();
    }
}

void
drm_crtc_t::add_fb(std::shared_ptr<dumb_fb_t> fb)
{
    if (fb) {
        m_fb = fb;
    }
}

void
drm_crtc_t::add_cursor(std::shared_ptr<dumb_fb_t> cursor)
{
    if (cursor) {
        m_cursor = cursor;
        std::shared_ptr<cursor_t> tmp_cursor = std::make_shared<cursor_t>(point_t(0, 0));
        std::shared_ptr<QImage> arrow_cursor(std::make_shared<QImage>(64, 64, QImage::Format_ARGB32));
        if (!tmp_cursor || !arrow_cursor) {
            return;
        }

        arrow_cursor->load("/etc/vglass/cursor.png");
        tmp_cursor->set_cursor_image(arrow_cursor);
        show_cursor(tmp_cursor);
    }
}

bool
drm_crtc_t::set_resolution(std::shared_ptr<drm_connector_t> connector, const QSize &resolution)
{
    Expects(connector != nullptr);

    drmModeModeInfo *modeinfo = nullptr;

    if (connector->is_connected()) {
        uint32_t conn_id = connector->id();

        if (connector->mode(resolution)) {
            qDebug() << "Resolution is valid for this connector" << connector->name();
            modeinfo = connector->mode(resolution)->modeinfo();
        }

        if (!modeinfo) {
            qDebug() << "Failed to find an appropriate mode";
            return false;
        }

        connector->dpms_on();
        hide_cursor();
        if (drmModeSetCrtc(m_drm_fd, m_crtc_id, m_fb->unique_id(), 0, 0, &conn_id, 1, modeinfo)) {
            auto error = errno;
            qDebug() << "Failed to set CRTC" << error;
            return false;
        } else {
            // Set up the display plane
            display_plane_t::operator+=(rect_t(point_t(0, 0), resolution));
            m_name = connector->name().toStdString();
            m_clip += rect_t(point_t(0, 0), resolution);
            m_current_clip += rect_t(point_t(0, 0), resolution);
            qDebug() << "Resolution set successfully";
            show_cursor();
            return true;
        }
    } else {
        qDebug() << "Connector disabled!";
        return false;
    }
}

bool
drm_crtc_t::set_resolution(std::shared_ptr<drm_connector_t> connector, drmModeModeInfo *modeinfo)
{
    Expects(connector != nullptr);
    Expects(modeinfo != nullptr);

    if (connector->is_connected()) {
        uint32_t conn_id = connector->id();

        connector->dpms_on();
        hide_cursor();
        if (drmModeSetCrtc(m_drm_fd, connector->crtc_id(), m_fb->unique_id(), 0, 0, &conn_id, 1, modeinfo)) {
            auto error = errno;
            qDebug() << "Failed to set CRTC" << error;
            return false;
        } else {
            display_plane_t::operator+=(rect_t(point_t(0, 0), QSize(modeinfo->hdisplay, modeinfo->vdisplay)));
            m_name = connector->name().toStdString();
            m_clip += rect_t(point_t(0, 0), QSize(modeinfo->hdisplay, modeinfo->vdisplay));
            m_current_clip += rect_t(point_t(0, 0), QSize(modeinfo->hdisplay, modeinfo->vdisplay));
            qDebug() << "Resolution set successfully";
            show_cursor();
            return true;
        }
    } else {
        qDebug() << "Connector disabled!";
        return false;
    }

    return false;
}

void
drm_crtc_t::show_cursor(std::shared_ptr<cursor_t> cursor)
{
    if (!cursor || cursor->cursor_image()->isNull()) {
        return;
    }

    QPainter painter(m_cursor->image().get());

    m_cursor->image()->fill(Qt::transparent);
    painter.drawImage(point_t(0, 0), *cursor->cursor_image());
    painter.end();

    m_hot_spot = cursor->hotspot();

    for (const auto &connector : m_compatible_connectors) {
        drmModeSetCursor2(m_drm_fd, connector->crtc_id(), m_cursor->handle(), m_cursor->width(), m_cursor->height(), m_hot_spot.x(), m_hot_spot.y());
    }

    move_cursor(m_last_cursor_position);

    m_cursor_visible = true;
}

void
drm_crtc_t::show_cursor()
{
    if (!m_cursor || !m_cursor->image()) {
        return;
    }

    for (const auto &connector : m_compatible_connectors) {
        drmModeSetCursor2(m_drm_fd, connector->crtc_id(), m_cursor->handle(), m_cursor->width(), m_cursor->height(), m_hot_spot.x(), m_hot_spot.y());
    }

    move_cursor(m_last_cursor_position);

    m_cursor_visible = true;
}

void
drm_crtc_t::move_cursor(point_t point)
{
    if (m_last_cursor_position == point) {
        return;
    }

    m_last_cursor_position = point;

    for (const auto &connector : m_compatible_connectors) {
        drmModeMoveCursor(m_drm_fd, connector->crtc_id(), point.x(), point.y());
    }
}

void
drm_crtc_t::hide_cursor()
{
    for (const auto &connector : m_compatible_connectors) {
        drmModeSetCursor(m_drm_fd, connector->crtc_id(), 0, 0, 0);
    }

    m_cursor_visible = false;
}

uint32_t
drm_crtc_t::bitmask()
{
    for (int i = 0; i < m_mode_resource->count_crtcs; i++) {
        if (m_mode_resource->crtcs[i] == m_crtc_id)
            return (1 << i);
    }

    return 0;
}

void
drm_crtc_t::set_parent(std::shared_ptr<plane_t> parent)
{
    m_parent = parent;
}
