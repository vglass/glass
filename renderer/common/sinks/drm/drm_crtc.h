//
// Glass Display
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#ifndef DRM_CRTC__H
#define DRM_CRTC__H

#include <glass_types.h>

extern "C" {
#include <errno.h>
#include <fcntl.h>
#include <getopt.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <xf86drm.h>
#include <xf86drmMode.h>
}

#include <exception>
#include <iostream>

#include "drm_connector.h"
#include "dumb_fb.h"
#include <desktop_plane.h>
#include <display_plane.h>

class drm_crtc_t : public display_plane_t
{
public:
    drm_crtc_t(int fd, uint32_t crtc_id, std::shared_ptr<drmModeRes> mode_resource, uint32_t banner_height = 25, std::shared_ptr<plane_t> display_parent = nullptr);
    explicit drm_crtc_t(std::shared_ptr<plane_t> display_parent);
    ~drm_crtc_t();

    void set_position(const point_t &position);
    bool set_resolution(std::shared_ptr<drm_connector_t> connector, const QSize &resolution);
    bool set_resolution(std::shared_ptr<drm_connector_t> connector, drmModeModeInfo *modeinfo);

    virtual void show_cursor(std::shared_ptr<cursor_t> cursor);
    virtual void show_cursor();
    virtual void move_cursor(point_t point);
    virtual void hide_cursor();

    void add_connector(std::shared_ptr<drm_connector_t> connector);

    void add_fb(std::shared_ptr<dumb_fb_t> fb);
    void add_cursor(std::shared_ptr<dumb_fb_t> cursor);

    void dpms_off();
    void dpms_on();

    uint32_t bitmask();

    void set_parent(std::shared_ptr<plane_t> parent);

private:
    int m_drm_fd;

    uint32_t m_crtc_id;
    qlist_t<uint32_t> m_consumed_plane_crtcs;
    qlist_t<uint32_t> m_consumed_encoder_crtcs;

    std::shared_ptr<drmModeRes> m_mode_resource;

    qlist_t<std::shared_ptr<drm_connector_t>> m_compatible_connectors;

    point_t m_hot_spot{0, 0};
    point_t m_last_cursor_position{0, 0};
};

#endif // DRM_CRTC__H
