//
// Glass Display
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#ifndef DRM_GPU__H
#define DRM_GPU__H

extern "C" {
#include <drm.h>
#include <drm_mode.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <xf86drm.h>
#include <xf86drmMode.h>
}

#include "drm_connector.h"
#include "drm_crtc.h"
#include "dumb_fb.h"
#include <gpu.h>

class fd_close_t
{
public:
    fd_close_t() { m_fd = -1; };
    ~fd_close_t()
    {
        if (m_fd > -1) {
            drmDropMaster(m_fd);
            close(m_fd);
            m_fd = -1;
        }
    }
    fd_close_t &operator=(int fd)
    {
        m_fd = fd;
        return *this;
    };

private:
    int m_fd;
};

class drm_gpu_t : public gpu_t
{
public:
    // This is a constructor only for probing modes for a particular GPU
    drm_gpu_t(json &gpu_config, std::string &device_path, std::shared_ptr<plane_t> desktop_parent = nullptr);
    explicit drm_gpu_t(json &gpu_config, int banner_height = 0, std::shared_ptr<plane_t> desktop_parent = nullptr);
    ~drm_gpu_t();

    std::shared_ptr<desktop_plane_t> desktop() { return m_desktop; }
    qhash_t<uuid_t, std::shared_ptr<desktop_plane_t>> pinned_desktops();

    void disable_display(display_plane_t &display);
    void enable_display(display_plane_t &display);

    void show_cursor(display_plane_t &display, std::shared_ptr<cursor_t> cursor);
    void move_cursor(display_plane_t &display, point_t point);
    void hide_cursor(display_plane_t &display);

private:
    void create_planes();
    void create_connectors();
    void create_crtcs();
    void create_encoders();

    void associate_crtcs_to_connectors(const qlist_t<std::shared_ptr<drm_connector_t>> &connectors,
                                       qmap_t<std::shared_ptr<drm_connector_t>, uint32_t> &crtcid_lookup,
                                       qmap_t<uint32_t, std::shared_ptr<drm_connector_t>> &connector_lookup);
    void crtc_connector_join(std::shared_ptr<drm_connector_t> connector,
                             uint32_t crtc_id,
                             point_t origin,
                             QSize resolution,
                             std::shared_ptr<dumb_fb_t> pfb,
                             std::shared_ptr<dumb_fb_t> pcursor);

    QSize get_cloned_resolution(json &displays);

    void create_shared_desktop(json &displays);
    void create_cloned_desktop(json &displays);

    void setup_cloned_monitors(json &displays);
    void setup_shared_monitors(json &displays);
    void setup_pinned_monitors(json &displays);

    void add_cloned_display(std::shared_ptr<drm_crtc_t> crtc);
    void add_shared_display(std::shared_ptr<drm_crtc_t> crtc);

    void apply_to_crtcs(std::function<void(int, uint32_t)> fn);
    void apply_to_connectors(std::function<void(int, uint32_t)> fn);
    void apply_to_encoders(std::function<void(int, uint32_t)> fn);
    void apply_to_modes(uint32_t conn_id, std::function<void(uint32_t)> fn);

    // m_fd_close is first to ensure its destructor is called last.  That way
    // the m_drm_fd is still open for all the other members (like dumb_fb_t).
    fd_close_t m_fd_close;
    int m_drm_fd;
    uint32_t m_banner_height;

    std::shared_ptr<drmModeRes> m_mode_resource;

    qmap_t<uint32_t, std::shared_ptr<drm_crtc_t>> m_crtcs;
    qmap_t<uint32_t, std::shared_ptr<drm_connector_t>> m_connectors;

    qlist_t<uint32_t> m_crtcs_available;
    qlist_t<uint32_t> m_connectors_available;
    qlist_t<uint32_t> m_encoders_available;

    qlist_t<std::shared_ptr<drm_connector_t>> m_cloned_connectors;
    qlist_t<std::shared_ptr<drm_connector_t>> m_shared_connectors;
    qmap_t<uuid_t, qlist_t<std::shared_ptr<drm_connector_t>>> m_pinned_connectors;

    qlist_t<std::shared_ptr<display_plane_t>> m_cloned_display_planes;
    qlist_t<std::shared_ptr<display_plane_t>> m_shared_display_planes;
    qmap_t<uuid_t, qlist_t<std::shared_ptr<display_plane_t>>> m_pinned_display_planes;

    std::shared_ptr<desktop_plane_t> m_desktop{nullptr};
    qhash_t<uuid_t, std::shared_ptr<desktop_plane_t>> m_pinned_desktops;

    // IDs of the various components in the drmModeRes struct,
    // except the FBs, as on initialization, it is empty.
    gsl::span<uint32_t> m_crtc_span;
    gsl::span<uint32_t> m_connector_span;
    gsl::span<uint32_t> m_encoder_span;

    std::shared_ptr<plane_t> m_desktop_parent;
};

#endif // DRM_GPU__H
