//
// Glass Display
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#include "pt_gpu.h"

pt_gpu_t::pt_gpu_t(json &gpu_config, std::shared_ptr<plane_t> parent)
{
    uuid_t uuid;
    std::shared_ptr<desktop_plane_t> pt_desktop = nullptr;

    for (auto &display : gpu_config["displays"]) {
        point_t origin(display["x"], display["y"]);
        uuid = uuid_t(QString::fromStdString(display["uuid"]));

        if (!pt_desktop) {
            pt_desktop = std::make_shared<desktop_plane_t>(uuid,
                                                           rect_t(0, 0, 0, 0),
                                                           point_t(0, 0),
                                                           false,
                                                           desktop_plane_t::PINNED,
                                                           parent);

            if (!pt_desktop) {
                throw std::bad_alloc();
            }
        }

        int preferred_mode_index = display["preferred_mode_index"];
        QSize resolution = QSize(display["modes"][preferred_mode_index]["width"],
                                 display["modes"][preferred_mode_index]["height"]);
        std::shared_ptr<pt_display_t> d = std::make_shared<pt_display_t>(rect_t(point_t(0, 0),
                                                                                resolution),
                                                                         origin);
        if (!d) {
            continue;
        }

        pt_desktop->add_display(d);
    }

    pt_desktop->reorigin_displays();

    pt_desktop->translate_planes();
    m_pinned_desktops[uuid] = pt_desktop;
}

std::shared_ptr<desktop_plane_t>
pt_gpu_t::desktop()
{
    return nullptr;
}

qhash_t<uuid_t, std::shared_ptr<desktop_plane_t>>
pt_gpu_t::pinned_desktops()
{
    return m_pinned_desktops;
}

void
pt_gpu_t::disable_display(display_plane_t &display)
{
    (void) display;
}

void
pt_gpu_t::enable_display(display_plane_t &display)
{
    (void) display;
}
