//
// Glass Display
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#include "vglass_dbus_proxy.h"
#include "drm/drm_gpu.h"
#include "renderer.h"
#include "vglass_adaptor.h"
#include <cstdlib>

extern std::list<std::string> gpu_devices;

vglass_dbus_proxy::vglass_dbus_proxy() : m_dbus(std::make_unique<DBus>())
{
    QDBusConnection::systemBus().connect("com.openxt.disman",
                                         "/com/openxt/disman",
                                         "com.openxt.disman",
                                         "configChanged",
                                         this,
                                         SLOT(handleConfigChanged(QString)));
    QDBusConnection::systemBus().connect("com.citrix.xenclient.input",
                                         "/",
                                         "com.citrix.xenclient.input",
                                         "idle_timeout",
                                         this,
                                         SLOT(handleIdleTimeout(QString)));

    m_dbus->registerService<VglassAdaptor>(this, "com.openxt.vglass");
    m_dbus->registerObject(this, "/com/openxt/vglass");

    QDBusMessage msg = QDBusMessage::createMethodCall("com.openxt.disman", "/com/openxt/disman", "com.openxt.disman", "refresh");
    QDBusConnection::systemBus().send(msg);
}

vglass_dbus_proxy::~vglass_dbus_proxy()
{
    QDBusConnection::systemBus().disconnect("com.citrix.xenclient.input",
                                            "/",
                                            "com.citrix.xenclient.input",
                                            "idle_timeout",
                                            this,
                                            SLOT(handleIdleTimeout(QString)));
    QDBusConnection::systemBus().disconnect("com.openxt.disman",
                                            "/com/openxt/disman",
                                            "com.openxt.disman",
                                            "configChanged",
                                            this,
                                            SLOT(handleConfigChanged(QString)));

    m_dbus->unregisterObject("/com/openxt/vglass");
    m_dbus->unregisterService("com.openxt.vglass");
}

QString
vglass_dbus_proxy::getConfig(void)
{
    TRACE;

    json config = public_get_config(gpu_devices);

    return QString::fromStdString(config.dump());
}

void
vglass_dbus_proxy::handleConfigChanged(QString disconfig)
{
    TRACE;

    json config;

    try {
        config = json::parse(disconfig.toStdString());
    } catch (...) {
        vg_debug() << "Failed to parse config";
        vg_debug() << disconfig;
    }

    if (!config.empty()) {
        emit config_changed(config);
    }
}

void
vglass_dbus_proxy::handleIdleTimeout(const QString timerName)
{
    TRACE;

    if (timerName == "screen-blanking") {
        setDpms(true);
    }
}

void
vglass_dbus_proxy::setDpms(const bool on)
{
    TRACE;

    if (true == on) {
        emit dpms_on();
    } else {
        emit dpms_off();
    }
}

void
vglass_dbus_proxy::identify(const bool on, const QString name)
{
    TRACE;

    (void) name;
    if (on) {
        emit identify_on(name.toStdString());
    } else {
        emit identify_off(name.toStdString());
    }
}

void
vglass_dbus_proxy::show_text(const bool on, const QString name)
{
    TRACE;

    if (on) {
        emit show_text_on(name.toStdString());
    } else {
        emit show_text_off(name.toStdString());
    }
}

void
vglass_dbus_proxy::clear_text()
{
    TRACE;

    emit clear_all_text();
}

void
vglass_dbus_proxy::exit()
{
    std::exit(0);
}
