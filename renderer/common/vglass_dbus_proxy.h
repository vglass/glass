//
// Glass Display
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#ifndef VGLASS_DBUS_PROXY__H
#define VGLASS_DBUS_PROXY__H

#include <QtCore>

#include <glass_types.h>

#include <dbus.h>

class vglass_dbus_proxy : public QObject
{
    Q_OBJECT;

public:
    vglass_dbus_proxy();
    virtual ~vglass_dbus_proxy();

public slots:

    virtual QString getConfig(void);
    virtual void handleConfigChanged(QString disconfig);
    virtual void show_text(const bool on, const QString text);
    virtual void clear_text();
    // Used by UIVM
    virtual void identify(const bool on, const QString name);
    // Used by xcpmd
    virtual void setDpms(const bool on);

    virtual void handleIdleTimeout(const QString timerName);

    void exit();

signals:

    void config_changed(json config);
    void dpms_on();
    void dpms_off();
    void identify_on(const std::string name);
    void identify_off(const std::string name);

    void show_text_on(const std::string name);
    void show_text_off(const std::string name);
    void clear_all_text();

    void render_complete(bool success);

private:
    std::unique_ptr<DBus> m_dbus;
};

#endif // VGLASS_DBUS_PROXY__H
