#ifndef __SORT_CONFIG_H__
#define __SORT_CONFIG_H__

#include <glass_types.h>
#include <json.hpp>

// Comparison function to sort modes
bool
sort_modes(json &l, json &r);
// Comparison function to sort displays
bool
sort_displays(json &l, json &r);
// Sorts a gpu_config to normalize for comparison
void
sort_config(json &config);

#endif // __SORT_CONFIG_H__
