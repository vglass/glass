//
// Glass Display
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#ifndef INPUT_H
#define INPUT_H

#include <QObject>
#include <display.h>
#include <vm.h>
#include <vm_input.h>

class input_t : public QObject
{
    Q_OBJECT

public:
    input_t(void) {}
    virtual ~input_t(void) = default;

public slots:

    virtual void add_guest(const std::shared_ptr<vm_input_t> &vm) = 0;
    virtual void remove_guest(const std::shared_ptr<vm_t> &vm) = 0;
    virtual void reboot_guest(const uuid_t uuid) = 0;
    virtual void sleep_guest(const uuid_t uuid) = 0;
    virtual void set_shared_desktop_owner(const std::shared_ptr<vm_t> &vm) = 0;
    virtual void update_input_guest(const std::shared_ptr<vm_t> &vm) = 0;
    virtual void wake_up_guest(uuid_t uuid) = 0;

signals:
    void update_hotspot(display_plane_t *display, point_t point, bool grabbed, uuid_t uuid);
    void focused_vm_changed(const std::shared_ptr<vm_t> &vm);
    void show_switcher(const QList<std::shared_ptr<vm_t>> &vms, const std::shared_ptr<vm_t> highlighted_vm);
    void hide_switcher(void);
    void save_screenshot(void);

protected:
    std::shared_ptr<vm_t> temp_vm;
};

#endif // INPUT_H
