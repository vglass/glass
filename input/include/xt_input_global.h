//
// Glass Display
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#ifndef XT_INPUT_GLOBAL__H
#define XT_INPUT_GLOBAL__H

#include <QDebug>
#include <QMap>
#include <linux/input.h>
#include <memory>
#include <stdint.h>

// Input type flags
#define XT_TYPE_NULL 0
#define XT_TYPE_KEY 1
#define XT_TYPE_RELATIVE 2
#define XT_TYPE_ABSOLUTE 3

// Multitouch
#define XT_TYPE_TOUCH_DOWN 4
#define XT_TYPE_TOUCH_MOVE 5
#define XT_TYPE_TOUCH_UP 6
#define XT_TYPE_TOUCH_FRAME 7
#define XT_TYPE_TOUCH_CANCEL 8

// Tablet tools
#define XT_TYPE_TOOL_PROXIMITY 9
#define XT_TYPE_TOOL_TIP 10
#define XT_TYPE_TOOL_AXIS 11
#define XT_TYPE_TOOL_BUTTON 12

#define XT_TYPE_TOOL_FLAG_PROXIMITY 0x01
#define XT_TYPE_TOOL_FLAG_TIP 0x02
#define XT_TYPE_TOOL_FLAG_BUTTONPRESSED 0x04

#define XT_TYPE_TOOL_PEN 0
#define XT_TYPE_TOOL_ERASER 1

#define XT_TYPE_TOOL_PRESSUREMAX 1024

// Logical screen width and height for multitouch translations
#define XT_LOGICAL_SCREEN_WIDTH 32768
#define XT_LOGICAL_SCREEN_HEIGHT 32768

// Define the maximum and minmum possible button values,
// for iterating over a KeyMask structure containing mouse buttons.
#define BTN_MIN BTN_MISC
#define BTN_MAX BTN_GEAR_UP

// Input LED Codes
#define LED_CODE_SCROLLLOCK 0x01
#define LED_CODE_NUMLOCK 0x02
#define LED_CODE_CAPSLOCK 0x04

/*
  The xt_input_event is a kind of intermediary format.
  At its core, each field is expected to be structured
  like that of a standard linux input event, defined
  in <linux/input.h>. The type is defined within this
  file, so the key to adding a new source or sink is
  translating its events in to or out of the the linux
  event fields format.
*/

class vm_t;
typedef QList<std::shared_ptr<vm_t>> VMList;

// Exports the library symbols.
#if defined(INPUTSERVER_LIBRARY)
#define INPUT_PRIVATE_EXPORT Q_DECL_EXPORT
#else
#define INPUT_PRIVATE_EXPORT Q_DECL_IMPORT
#endif

// Try to keep this aligned, currently 32byte aligned
typedef struct qemu_xt_input_event {
    uint32_t type;  // 4
    uint32_t flags; // 4

    uint32_t absX; // 4
    uint32_t absY; // 4

    union {
        uint32_t absXMax; // 4
        float relX;       // 4
    };
    union {
        uint32_t absYMax; // 4
        float relY;       // 4
    };
    int32_t relZ;     // (Pretty much padding)
    uint32_t keyCode; // 4
} __attribute__((packed, aligned(32))) qemu_xt_input_event;

//event for key information
struct xt_type_key {
    uint32_t keyCode;
};

//event for relative positional information
struct xt_type_relative {
    uint32_t button;
    uint32_t buttonState;
    float relX;
    float relY;
    float relZ;
};

//event for absolute positional information
struct xt_type_absolute {
    uint32_t button;
    uint32_t buttonState;
    uint32_t absX;
    uint32_t absY;
    uint32_t relZ;
    uint32_t absXMax; //FIXME ensure the correct location of these
    uint32_t absYMax; //FIXME ensure the correct location of these
};

//event for touch positional information
struct xt_type_touch {
    uint32_t slot;
    uint32_t absX;
    uint32_t absY;
};

//event for tablet tool
struct xt_type_tablet_tool {
    uint8_t tool_type;
    uint8_t flags;
    int32_t absX;
    int32_t absY;
    int32_t pressure; /* 0 to 1024 */
    int32_t button;
};

//event for tablet tool button
struct xt_type_tablet_tool_button {
    uint32_t button;
    uint32_t buttonState;
};

//the new intermediate xt_input_event
typedef struct xt_input_event {
    uint32_t type;
    uint32_t flags;
    uint32_t keyCode;
    union {
        struct xt_type_relative relative;
        struct xt_type_absolute absolute;
        struct xt_type_touch touch;
        struct xt_type_tablet_tool tablet_tool;
        struct xt_type_tablet_tool_button tablet_tool_button;
    } data;
} __attribute__((packed, aligned(32))) xt_input_event_t;

extern const char *shortcut_strings[221];
extern const QMap<QString, int32_t> string_to_keycode;

class KeyComboListener;

#define KEY_MAX_DOUBLE_WORDS 12

//TODO: pvoid? void pointer?
struct KeyMask
{
    uint64_t mask[KEY_MAX_DOUBLE_WORDS];
    std::shared_ptr<void> kcl;
};

static inline void
CLEAR_ALL_KEYS(struct KeyMask &keymask)
{
    for (int i = 0; i < KEY_MAX_DOUBLE_WORDS; ++i) {
        keymask.mask[i] = 0;
    }
}

static inline uint64_t &
GET_KEY_DOUBLEWORD(struct KeyMask &keymask, const xt_input_event &event)
{
    size_t byte_number = event.keyCode / 64;
    return keymask.mask[byte_number];
}

static inline uint64_t &
GET_KEY_DOUBLEWORD(struct KeyMask &keymask, uint32_t code)
{
    size_t byte_number = code / 64;
    return keymask.mask[byte_number];
}

static inline uint64_t
GET_KEY_BITMASK(const xt_input_event &event)
{
    uint64_t keyCode = (uint64_t) event.keyCode % 64;
    return (uint64_t) 1 << keyCode;
}

static inline uint64_t
GET_KEY_BITMASK(uint32_t code)
{
    uint64_t keyCode = (uint64_t) code % 64;
    return (uint64_t) 1 << keyCode;
}

static inline void
CLEAR_KEY(struct KeyMask &keymask, const xt_input_event &event)
{
    if (event.flags)
        return;

    GET_KEY_DOUBLEWORD(keymask, event) &= ~GET_KEY_BITMASK(event);
}

static inline void
SET_KEY(struct KeyMask &keymask, const xt_input_event &event)
{
    if (!event.flags)
        return;

    GET_KEY_DOUBLEWORD(keymask, event) |= GET_KEY_BITMASK(event);
}

static inline void
SET_KEY(struct KeyMask &keyMask, uint32_t code)
{
    GET_KEY_DOUBLEWORD(keyMask, code) |= GET_KEY_BITMASK(code);
}

static inline void
UPDATE_KEY_MASK(struct KeyMask &keymask, const xt_input_event &event)
{
    if (event.flags)
        SET_KEY(keymask, event);
    else
        CLEAR_KEY(keymask, event);
}

static inline void
CLEAR_KEY_MASK(struct KeyMask &curr, struct KeyMask &keymask)
{
    for (int i = 0; i < KEY_MAX_DOUBLE_WORDS; ++i) {
        curr.mask[i] &= ~keymask.mask[i];
    }
}

static inline void
PRINT_KEY_MASK(struct KeyMask &keymask)
{
    (void) keymask;
    for (int i = 0; i < KEY_MAX_DOUBLE_WORDS; ++i) {
        // TODO: New debug
        //qDebug() << QString::number(keymask.mask[i], 16);
    }
}

static inline bool
CHECK_BIT_SET(struct KeyMask &keymask, size_t bit)
{
    uint64_t key_dword = keymask.mask[bit / 64];
    uint64_t key_mask = (uint64_t) 1 << (uint64_t)(bit % 64);
    uint64_t result = key_dword & key_mask;

    return (result != 0);
}

static inline void
SET_KEY_MASK(struct KeyMask &leftMask,
             const struct KeyMask &rightMask)
{
    for (int i = 0; i < KEY_MAX_DOUBLE_WORDS; ++i) {
        leftMask.mask[i] |= rightMask.mask[i];
    }
}

static inline bool
maskCleared(const struct KeyMask &keymask)
{
    for (int i = 0; i < KEY_MAX_DOUBLE_WORDS; ++i) {
        if (keymask.mask[i] != 0) {
            return false;
        }
    }

    // Return true if everything is cleared
    return true;
}

static inline bool
maskInMask(const struct KeyMask &comp, const struct KeyMask &curr)
{
    for (int i = 0; i < KEY_MAX_DOUBLE_WORDS; ++i) {
        // Check the given doubleword to see if it contains the keys of interest.
        uint64_t maskedKey = comp.mask[i] & curr.mask[i];

        // If it doesn't, this mask hasn't been matched.
        if (maskedKey != comp.mask[i]) {
            return false;
        }
    }

    // Return true iff all doublewords match.
    return true;
}

static inline bool
keyInMask(const xt_input_event &comp, struct KeyMask &curr)
{
    uint64_t key = GET_KEY_DOUBLEWORD(curr, comp);
    uint64_t bitmask = GET_KEY_BITMASK(comp);
    return ((key & bitmask) == bitmask);
}

//static inline to prevent compilation of multiple defs
static inline QDebug
operator<<(QDebug dbg, const xt_input_event &event)
{
    dbg << "XT InputEvent[\n";
    dbg << "\ttype=" << QString::number(event.type, 16) << "\n";
    dbg << "\tflags=" << QString::number(event.flags, 16) << "\n";
    dbg << "\tkey=" << ((event.keyCode < 128) ? QString(shortcut_strings[event.keyCode]) : QString::number(event.keyCode, 16)) << "\n";
    dbg << "\tkeycode=" << QString::number(event.keyCode, 16) << "\n";
    switch (event.type) {
        case XT_TYPE_TOUCH_DOWN:
        case XT_TYPE_TOUCH_MOVE:
        case XT_TYPE_TOUCH_UP:
        case XT_TYPE_TOUCH_FRAME:
            dbg << "\tslot=" << QString::number(event.data.touch.slot, 10) << "\n";
            dbg << "\tabsX=" << QString::number(event.data.touch.absX, 10) << "\n";
            dbg << "\tabsY=" << QString::number(event.data.touch.absY, 10) << "\n";
            break;
        case XT_TYPE_TOOL_PROXIMITY:
        case XT_TYPE_TOOL_TIP:
        case XT_TYPE_TOOL_AXIS:
        case XT_TYPE_TOOL_BUTTON:
            dbg << "\tpressure=" << QString::number(event.data.tablet_tool.pressure, 10) << "\n";
            dbg << "\tflags=" << QString::number(event.data.tablet_tool.flags, 10) << "\n";
            dbg << "\tabsX=" << QString::number(event.data.tablet_tool.absX, 10) << "\n";
            dbg << "\tabsY=" << QString::number(event.data.tablet_tool.absY, 10) << "\n";
            dbg << "\tbutton=" << QString::number(event.data.tablet_tool.button, 10) << "\n";
            break;
        case XT_TYPE_RELATIVE:
            dbg << "\tbutton=" << QString::number(event.data.relative.button) << "\n";
            dbg << "\tbuttonState=" << QString::number(event.data.relative.buttonState) << "\n";
            dbg << "\trelX=" << QString::number(event.data.relative.relX) << "\n";
            dbg << "\trelY=" << QString::number(event.data.relative.relY) << "\n";
            dbg << "\trelZ=" << QString::number(event.data.relative.relZ) << "\n";
            break;
        case XT_TYPE_ABSOLUTE:
            dbg << "\tbutton=" << QString::number(event.data.absolute.button) << "\n";
            dbg << "\tbuttonState=" << QString::number(event.data.absolute.buttonState) << "\n";
            dbg << "\tabsX=" << QString::number(event.data.absolute.absX, 10) << "\n";
            dbg << "\tabsY=" << QString::number(event.data.absolute.absY, 10) << "\n";
            dbg << "\trelZ=" << QString::number(event.data.absolute.relZ, 10) << "\n";
            dbg << "\tabsXMax=" << QString::number(event.data.absolute.absXMax, 10) << "\n";
            dbg << "\tabsYMax=" << QString::number(event.data.absolute.absYMax, 10) << "\n";
            break;
        default:
            dbg << "\tUNKNOWN XT TYPE. CANNOT FORM BODY"
                << "\n";
            break;
    }
    return dbg;
}

#endif
