//
// Glass Display
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#ifndef INPUT_ACTION__H
#define INPUT_ACTION__H

#include <QDebug>
#include <QObject>

class input_action_t : public QObject
{
    Q_OBJECT

public:
    input_action_t(void);
    virtual ~input_action_t(void);
    virtual void operator()() = 0;
};

#endif // INPUT_ACTION__H
