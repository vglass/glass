//
// Glass Display
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#include <hideswitchaction.h>

hide_switch_action_t::hide_switch_action_t()
{
}

hide_switch_action_t::~hide_switch_action_t(void)
{
}

void
hide_switch_action_t::operator()()
{
    emit hide_switcher(m_vm);
}

void
hide_switch_action_t::vm_changed(uuid_t highlight)
{
    m_vm = highlight;
}
