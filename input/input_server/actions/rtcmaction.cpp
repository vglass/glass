//
// Glass Display
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#include <cstdlib>
#include <rtcmaction.h>

#include <QtCore>
#include <QtDBus>

revert_to_cloned_mode_action_t::revert_to_cloned_mode_action_t(void) {}
revert_to_cloned_mode_action_t::~revert_to_cloned_mode_action_t(void) {}

void
revert_to_cloned_mode_action_t::operator()()
{
    QDBusMessage msg = QDBusMessage::createMethodCall("com.openxt.disman", "/com/openxt/disman", "com.openxt.disman", "revertToClonedMode");
    QDBusConnection::systemBus().send(msg);
}
