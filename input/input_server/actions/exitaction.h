//
// Glass Display
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#ifndef EXIT_ACTION__H
#define EXIT_ACTION__H

#include <inputaction.h>

class exit_action_t : public input_action_t
{
    Q_OBJECT

public:
    exit_action_t(void);
    ~exit_action_t(void);

    void operator()();
};

#endif //EXIT_ACTION__H
