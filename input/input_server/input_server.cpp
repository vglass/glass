//
// Glass Display
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#include <input_server.h>
#include <libinputsource.h>
//#include <libevdevsource.h>
#include "config.h"
#include "input_dbus_proxy.h"

// TODO: Move these to the inputconfigparser
#include <algorithm>
#include <fstream>
#include <longpressfilter.h>
#include <switchvmaction.h>
#include <vm.h>

#include <db_interface.h>
#include <xcpmd_interface.h>

input_server_t::input_server_t(window_manager_t &wm) : m_wm(wm), m_focused_vm(nullptr), m_default_guest(nullptr), m_unhandled_switch(0)
{
    // Root privileges is needed to get the code to work
    if (geteuid() != 0) {
        qCritical() << "This executable must be run with root privileges!!!";
        std::exit(1);
    }

    m_input_dbus_proxy = std::make_shared<input_dbus_proxy>(this);

    qRegisterMetaType<xt_input_event_t>("xt_input_event_t");
    qRegisterMetaType<VMList>("VMList");

    auto src = std::make_shared<libinput_source_t>(wm);
    connect(m_input_dbus_proxy.get(), &input_dbus_proxy::set_touchpad_tap_to_click, src.get(), &libinput_source_t::set_touchpad_tap_to_click);
    connect(m_input_dbus_proxy.get(), &input_dbus_proxy::set_touchpad_scrolling, src.get(), &libinput_source_t::set_touchpad_scrolling);
    connect(m_input_dbus_proxy.get(), &input_dbus_proxy::set_touchpad_speed, src.get(), &libinput_source_t::set_touchpad_speed);
    connect(m_input_dbus_proxy.get(), &input_dbus_proxy::set_mouse_speed_f, src.get(), &libinput_source_t::set_mouse_speed);
    connect(src.get(), &libinput_source_t::add_pointer_device, m_input_dbus_proxy.get(), &input_dbus_proxy::add_pointer_device);
#ifdef TAP_INPUT
    connect(m_input_dbus_proxy.get(), &input_dbus_proxy::set_tap_input_f, src.get(), &libinput_source_t::set_tap_input);
    connect(m_input_dbus_proxy.get(), &input_dbus_proxy::clear_tap_input_f, src.get(), &libinput_source_t::clear_tap_input);
#endif
    m_input_sources.push_back(src);
    //m_input_sources.push_back(std::make_shared<libevdev_source_t>());

    for (auto source : m_input_sources) {
        QObject::connect(source.get(), &input_source_t::forward_filtered_input_event, this, &input_server_t::event_slot);
    }

    json input_config = load_config();
    m_default_guest_uuid = QUuid(QString::fromStdString(input_config["default-vm"].get<std::string>()));
    set_seamless_mousing(input_config["seamless-mouse"]);
    parse_config(input_config);

    connect(&m_wm, &window_manager_t::focus_changed, this, &input_server_t::focus_changed);
    connect(this, &input_server_t::focused_vm_changed_region, &m_wm, &window_manager_t::set_render_focus);
    connect(m_input_dbus_proxy.get(), &input_dbus_proxy::idle_timer_update, this, &input_server_t::idle_timer_update);

    m_input_dbus_proxy->watched_value_changed("/ui/screenblanking-timeout");

    //connections for handling brightness key actions
    m_dbus_connection = get_system_bus();
    m_xcpmd_dbus = make_xcpmd_dbus("com.citrix.xenclient.xcpmd", "/", m_dbus_connection.get());

    m_db = std::make_unique<db_dbus_t>(QString("com.citrix.xenclient.db"),
                                       QString("/"),
                                       QDBusConnection::systemBus());

    if (!m_dbus_connection) {
        throw std::invalid_argument("No QDBusConnection");
    }
    if (!m_db) {
        throw std::invalid_argument("No db_dbus");
    }
    if (!m_xcpmd_dbus) {
        throw std::invalid_argument("No xcpmd_dbus");
    }

    //grab initial power_source for brightness code
    uint using_ac = m_xcpmd_dbus->get_ac_adapter_state();
    power_source = using_ac ? "ac_dim_backlight" : "batt_dim_backlight";

    seam::qobj_connect(m_xcpmd_dbus.get(), SIGNAL(ac_adapter_state_changed(uint)), this, SLOT(update_power_source(uint)));
}

input_server_t::input_server_t(window_manager_t &wm, std::string input_config_path) : m_wm(wm), m_focused_vm(nullptr), m_default_guest(nullptr), m_unhandled_switch(0)
{
    (void) input_config_path;

    // Root privileges is needed to get the code to work
    if (geteuid() != 0) {
        qCritical() << "This executable must be run with root privileges!!!";
        std::exit(1);
    }

    m_input_dbus_proxy = std::make_shared<input_dbus_proxy>(this);

    qRegisterMetaType<xt_input_event_t>("xt_input_event_t");
    qRegisterMetaType<VMList>("VMList");

    auto src = std::make_shared<libinput_source_t>(wm);
    connect(m_input_dbus_proxy.get(), &input_dbus_proxy::set_touchpad_tap_to_click, src.get(), &libinput_source_t::set_touchpad_tap_to_click);
    connect(m_input_dbus_proxy.get(), &input_dbus_proxy::set_touchpad_scrolling, src.get(), &libinput_source_t::set_touchpad_scrolling);
    connect(m_input_dbus_proxy.get(), &input_dbus_proxy::set_touchpad_speed, src.get(), &libinput_source_t::set_touchpad_speed);
    connect(m_input_dbus_proxy.get(), &input_dbus_proxy::set_mouse_speed_f, src.get(), &libinput_source_t::set_mouse_speed);
    connect(src.get(), &libinput_source_t::add_pointer_device, m_input_dbus_proxy.get(), &input_dbus_proxy::add_pointer_device);
#ifdef TAP_INPUT
    connect(m_input_dbus_proxy.get(), &input_dbus_proxy::set_tap_input_f, src.get(), &libinput_source_t::set_tap_input);
    connect(m_input_dbus_proxy.get(), &input_dbus_proxy::clear_tap_input_f, src.get(), &libinput_source_t::clear_tap_input);
#endif
    m_input_sources.push_back(src);
    //m_input_sources.push_back(std::make_shared<libevdev_source_t>());

    for (auto source : m_input_sources) {
        QObject::connect(source.get(), &input_source_t::forward_filtered_input_event, this, &input_server_t::event_slot);
    }

    json input_config = load_config();
    m_default_guest_uuid = QUuid(QString::fromStdString(input_config["default-vm"].get<std::string>()));
    set_seamless_mousing(input_config["seamless-mouse"]);
    parse_config(input_config);

    connect(&m_wm, &window_manager_t::focus_changed, this, &input_server_t::focus_changed);
    connect(this, &input_server_t::focused_vm_changed_region, &m_wm, &window_manager_t::set_render_focus);
    connect(m_input_dbus_proxy.get(), &input_dbus_proxy::idle_timer_update, this, &input_server_t::idle_timer_update);

    m_input_dbus_proxy->watched_value_changed("/ui/screenblanking-timeout");
}

input_server_t::~input_server_t(void)
{
}

json
input_server_t::load_config()
{
    Config config;

    if (config.exists(QUrl(INPUT_CONFIG_URL))) {
        return config.load(QUrl(INPUT_CONFIG_URL));
    } else {
        return config.load(QUrl(INPUT_CONFIG_DEFAULT_URL));
    }
}

bool
input_server_t::save_config(const json j)
{
    Config config;

    return config.save(QUrl(INPUT_CONFIG_URL), j);
}

window_manager_t &
input_server_t::get_wm()
{
    return m_wm;
}

bool
input_server_t::register_input_filter(const QString name, std::shared_ptr<input_server_filter_t> filter)
{
    (void) name;
    Q_CHECK_PTR(filter);

    connect(filter.get(), SIGNAL(filter_fire(const std::shared_ptr<input_action_t> &)), this, SLOT(input_action(const std::shared_ptr<input_action_t> &)));
    m_filter_list.push_back(filter);

    return true;
}

void
input_server_t::event_slot(xt_input_event event, std::shared_ptr<vm_input_t> target_vm)
{
    static QElapsedTimer time;

    if (!time.isValid())
        time.start();

    if (time.elapsed() > 1000) {
        time.restart();
        emit heartbeat();
    }

    // The goal is to make the pipeline from source to sink as fast as possible.
    // A large potential introduction of latency is going to be event filtering,
    // where we check to see if the user wants to do something that really doesn't
    // belong to the guest (switch VMs)

    // If we weren't provided with a target VM, use the currently focused guest.
    if (!target_vm) {
        if (!get_focused_vm()) {
            set_focused_vm();
        }
        target_vm = get_focused_vm();

        if (!target_vm) {
            qDebug() << "No vm to send input to!";
            //throw std::logic_error("No vm to send input to!");
            return;
        }
    }

    // Make sure we have a valid event
    if (filter_event(target_vm, event)) {
        vg_debug() << "input_server_t::event_slot::filter. xt event type: " << event.type << " not being sent to guest";
        //Send touch up events if a filter switched focus after a touch event.
        if ((event.type == XT_TYPE_TOUCH_MOVE || event.type == XT_TYPE_TOUCH_DOWN || event.type == XT_TYPE_TOUCH_UP) && target_vm != get_focused_vm()) {
            struct xt_input_event xt_event;
            memset(&xt_event, 0x00, sizeof(xt_input_event));
            xt_event.type = XT_TYPE_TOUCH_UP;
            for (uint32_t i = 0; i < 10; i++) {
                xt_event.data.touch.slot = i;
                event_to_guest(target_vm, xt_event);
            }
        }
        return;
    }

    // If the target VM is not the currently focused VM, make it the focused VM
    if (m_guest_map.contains(target_vm->base()->uuid())) {

        if (target_vm != get_focused_vm())
            set_focused_vm(target_vm);

        event_to_guest(target_vm, event);

        auto check_focus = get_focused_vm();

        if (check_focus && target_vm != check_focus && event.keyCode == BTN_LEFT && event.flags == 1) {
            // Simulate empty relative event to move mouse in guest where we need it to be
            event.flags = 0;
            event.keyCode = 0;

            event_to_guest(check_focus, event);

            // Simulate another mouse down for the vm that is now the focus.
            event.flags = 1;
            event.keyCode = BTN_LEFT;

            event_to_guest(check_focus, event);
        }
    }
}

void
input_server_t::parse_config(json &config)
{
    std::shared_ptr<input_config_parser_t> parser = std::make_shared<input_config_parser_t>(config, this);
    //Q_CHECK_PTR(parser);
    //Q_CHECK_PTR(config);
    /*
    inputWhitelist = parser->parseWhitelist();

    // If Seamless Mouse is enabled, register the relevant filter.
    if (config->toObject()->value("seamless-mouse")->toBool()) {
        std::shared_ptr<seamless_mouse_filter_t> seamless_filter = std::make_shared<seamless_mouse_filter_t>(&mInputMapper);
        connect(this, &::focused_vm_changed, seamless_filter.get(), &seamless_mouse_filter_t::focused_vm_changed);
        connect(seamless_filter.get(), &seamless_mouse_filter_t::out_of_band_event, this, &input_server_t::event_to_guest);

        register_input_filter(seamless_filter_t);
    }
*/
    //normal
    parser->create_filters_from_config();
}

const std::shared_ptr<vm_input_t>
input_server_t::get_focused_vm(void) const
{
    uuid_t uuid = m_wm.render_focus_vm();

    return m_guest_map[uuid];
}

void
input_server_t::update_input_guest(const std::shared_ptr<vm_t> &vm)
{
    (void) vm;
    return;
}

void
input_server_t::set_shared_desktop_owner(const std::shared_ptr<vm_t> &vm)
{
    (void) vm;
    return;
}

void
input_server_t::add_guest(const std::shared_ptr<vm_input_t> &vm)
{
    Q_CHECK_PTR(vm);
    TRACE;

    if (!vm) {
        return;
    }

    // Check to make sure that the vm doesn't already exist.
    for (auto &iter : m_guest_map) {
        if (vm && vm->base()->domid() == iter->base()->domid()) {
            return;
        }
    }

    connect(vm.get(), &vm_input_t::update_hotspot, this, &input_server_t::update_hotspot);

    // Update the guest map and guest list for bookkeeping
    m_guest_map[vm->base()->uuid()] = vm;

    uuid_t vm_id = vm->base()->uuid();
    if (vm_id == m_default_guest_uuid) {
        m_default_guest = vm;
        QTimer::singleShot(500, this, SLOT(set_focused_vm()));
        return;
    } else if (vm_id == m_reboot_guest_uuid &&
               m_focused_vm == m_default_guest) {
        set_focused_vm(vm);
    } else if (m_unhandled_switch == vm->base()->domid() &&
               m_focused_vm == m_default_guest) {
        set_focused_vm(vm);
    }

    m_unhandled_switch = 0;
    m_reboot_guest_uuid = uuid_t();

    return;
}

void
input_server_t::remove_guest(const std::shared_ptr<vm_t> &vm)
{
    Q_CHECK_PTR(vm);
    TRACE;

    if (m_guest_map.contains(vm->uuid()) == false) {
        return;
    } else {
        // Remove the guest from the map list and renderstack
        m_guest_map.remove(vm->uuid());
        if (m_default_guest && m_default_guest->base()->uuid() == vm->uuid()) {
            m_default_guest = nullptr;
        }
        if (m_focused_vm && m_focused_vm->base()->uuid() == vm->uuid()) {
            m_focused_vm = nullptr;
        }

        if (!m_focused_vm) {
            set_focused_vm();
        }
    }

    return;
}

void
input_server_t::reboot_guest(const uuid_t uuid)
{
    TRACE;

    if (m_focused_vm && m_focused_vm->base()->uuid() == uuid) {
        m_reboot_guest_uuid = uuid;
    }
}

void
input_server_t::sleep_guest(const uuid_t uuid)
{
    TRACE;

    if (m_focused_vm && m_focused_vm->base()->uuid() == uuid) {
        set_focused_vm();
    }
}

void
input_server_t::input_action(const std::shared_ptr<input_action_t> &input_action)
{
    if (input_action != nullptr) {
        (*input_action)();
    }
}

bool
input_server_t::is_pointer_event(xt_input_event event)
{
    return (event.type == XT_TYPE_RELATIVE || event.type == XT_TYPE_ABSOLUTE);
}

bool
input_server_t::filter_event(std::shared_ptr<vm_input_t> &guest, xt_input_event &event)
{
    bool rc = false;

    for (auto filter : m_filter_list) {
        if (filter && filter->filter_event(guest, &event)) {
            vg_debug() << "event being dropped per filter: " << filter->get_name();
            rc = true;
        }
    }
    return rc;
}

void
input_server_t::event_to_guest(const std::shared_ptr<vm_input_t> &guest, xt_input_event event)
{
    Q_ASSERT(m_guest_map.contains(guest->base()->uuid()));
    Q_CHECK_PTR(m_guest_map[guest->base()->uuid()]);

    m_guest_map[guest->base()->uuid()]->input_event_slot(event);
}

void
input_server_t::show_switcher()
{
    emit show_vm_switcher();
}

void
input_server_t::hide_switcher(uuid_t uuid)
{
    set_focused_vm(m_guest_map[uuid]);
    emit hide_vm_switcher();
}

void
input_server_t::set_highlight_vm(uuid_t uuid)
{
    emit highlight_vm_changed(uuid);
}

void
input_server_t::advance_vm_right(uuid_t highlighted_vm)
{
    emit advance_switcher_right(highlighted_vm);
}

void
input_server_t::advance_vm_left(uuid_t highlighted_vm)
{
    emit advance_switcher_left(highlighted_vm);
}

void
input_server_t::set_focused_vm(void)
{
    if (!m_default_guest) {
        qDebug() << "No guests have joined the input server, is the UIVM running?";
        return;
    }

    set_focused_vm(m_default_guest);
}

void
input_server_t::set_focused_vm(const std::shared_ptr<vm_input_t> &vm)
{
    if (!vm) {
        return;
    }

    emit focused_vm_changed_region(vm->base()->uuid());

    emit center_mouse(vm->base()->uuid());
}

bool
input_server_t::switch_focus(const domid_t domid, const bool)
{
    foreach (auto vm, m_guest_map) {
        if (vm && vm->base()->domid() == domid) {
            set_focused_vm(vm);
            return true;
        }
    }
    m_unhandled_switch = domid;
    return false;
}

void
input_server_t::switch_vm(const int32_t slot)
{
    int32_t vm_slot = slot;

    foreach (auto vm, m_guest_map) {
        if (vm && vm->base()->slot() == vm_slot) {
            set_focused_vm(vm);
            break;
        }
    }
}

void
input_server_t::focus_changed(const uuid_t uuid)
{
    if (m_guest_map.contains(uuid)) {
        auto vm = m_guest_map[uuid];

        if (vm) {
            if (m_focused_vm) {
                m_focused_vm->lost_focus();
            }

            for (auto source : m_input_sources) {
                if (m_focused_vm) {
                    disconnect(m_focused_vm.get(), &vm_input_t::set_input_led_code, source.get(), &input_source_t::set_led_code);
                }
                connect(vm.get(), &vm_input_t::set_input_led_code, source.get(), &input_source_t::set_led_code);
            }

            vm->got_focus();

            m_focused_vm = vm;
        }
    }

    m_unhandled_switch = 0;
    emit m_input_dbus_proxy->keyboard_focus_change(uuid.toString().mid(1, uuid.toString().length() - 2));
}

void
input_server_t::idle_timeout(const QString &idle_name)
{
    emit m_input_dbus_proxy->idle_timeout(idle_name);
}

bool
input_server_t::get_seamless_mousing()
{
    return m_wm.get_seamless_mousing();
}

bool
input_server_t::set_seamless_mousing(const bool enabled)
{
    json inputconfig = load_config();
    inputconfig["seamless-mouse"] = enabled;
    save_config(inputconfig);

    m_wm.set_seamless_mousing(enabled);

    return true;
}

void
input_server_t::wake_up_guest(uuid_t uuid)
{
    auto vm = m_guest_map[uuid];

    if (vm) {
        vm->wake_up_toggle();
    }

    m_wm.set_render_focus(uuid);
}

void
input_server_t::update_power_source(uint using_ac)
{
    power_source = using_ac ? "ac_dim_backlight" : "batt_dim_backlight";
}

void
input_server_t::increase_brightness()
{
    QString new_bright;
    int old_bright;

    old_bright = m_db->read(QString("/power-management/vars/") + power_source).value().toInt();

    if (old_bright == 100) {
        return; //brightness is at max, no need to execute further
    }

    new_bright = QString::number(std::max(std::min(old_bright + 10, 100), 1));
    m_xcpmd_dbus->add_var(power_source, new_bright);
}

void
input_server_t::decrease_brightness()
{
    QString new_bright;
    int old_bright;

    old_bright = m_db->read(QString("/power-management/vars/") + power_source).value().toInt();

    if (old_bright == 1) {
        return; //brightness is at min, no need to execute further
    }

    new_bright = QString::number(std::max(std::min(old_bright - 10, 100), 1));
    m_xcpmd_dbus->add_var(power_source, new_bright);
}
