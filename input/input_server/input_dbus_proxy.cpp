//
// Glass Display
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#include "input_dbus_proxy.h"
#include "input_adaptor.h"
#include "input_server.h"
#include <algorithm>
#include <config.h>
#include <db_interface.h>
#include <dbus.h>
#include <dbwatch.h>
#include <json.hpp>
extern "C" {
#include <sys/types.h>
#include <unistd.h>
#include <xenstore.h>
}

input_dbus_proxy::input_dbus_proxy(input_server_t *input_server) : m_dbus(std::make_unique<DBus>()),
                                                                   m_db(std::make_unique<db_dbus_t>(QString("com.citrix.xenclient.db"),
                                                                                                    QString("/"),
                                                                                                    QDBusConnection::systemBus())),
                                                                   m_dbw(std::make_unique<db_watch>()),
                                                                   m_is(input_server)
{
    m_dbus->registerService<InputAdaptor>(this, "com.citrix.xenclient.input");
    m_dbus->registerObject(this, "/");

    // Add the path to our watch
    m_dbw->addPath("/switcher/enabled");
    m_dbw->addPath("/ui/screenblanking-timeout");

    // Attempt to start the watch and connect the signal
    if (m_dbw->start()) {
        connect(m_dbw.get(), &db_watch::triggered, this, &input_dbus_proxy::watched_value_changed);
    } else {
        qWarning() << "db_watch failed to start";
    }
}

input_dbus_proxy::~input_dbus_proxy()
{
    m_dbw->stop();

    m_dbus->unregisterObject("/");
    m_dbus->unregisterService("com.citrix.xenclient.input");
}

void
input_dbus_proxy::add_pointer_device()
{
    if (!m_db) {
        return;
    }

    if (m_db->exists("/mouse/speed")) {
        set_mouse_speed(this->get_mouse_speed());
    } else {
        set_mouse_speed(5);
    }
    if (m_db->exists("/touchpad/speed")) {
        touchpad_set("speed", touchpad_get("speed"));
    } else {
        touchpad_set("speed", "5");
    }
    if (m_db->exists("/touchpad/scrolling-enable")) {
        touchpad_set("scrolling-enable", touchpad_get("scrolling-enable"));
    } else {
        touchpad_set("scrolling-enable", "true");
    }
    if (m_db->exists("/touchpad/tap-to-click-enable")) {
        touchpad_set("tap-to-click-enable", touchpad_get("tap-to-click-enable"));
    } else {
        touchpad_set("tap-to-click-enable", "true");
    }
}

//
// DBus Methods
//      this stuff is still supported
//

QStringList
input_dbus_proxy::get_kb_layouts()
{
    QStringList ret;
    FILE *f = NULL;
    char line[256];
    int i = 0;

    f = fopen(KEYMAP_LIST_FILE, "r");
    if (!f) {
        qWarning() << "Failed to open " << KEYMAP_LIST_FILE;
        return QStringList();
    }

    while (fgets(line, sizeof(line), f) && i < KEYMAP_LIST_MAX + 1) {
        char *p, *q;

        p = strchr(line, ':');
        if (!p) {
            qWarning() << "Invalid data in " << KEYMAP_LIST_FILE;
            break;
        }

        *p++ = '\0';

        q = strchr(p, ':');
        if (!q) {
            qWarning() << "Invalid data in " << KEYMAP_LIST_FILE;
            break;
        }

        *q++ = '\0';

        if (strcmp(p, "y") == 0) {
            ret << strdup(line);
        }
    }

    if (f) {
        fclose(f);
    }

    if (i > KEYMAP_LIST_MAX) {
        qWarning() << "Too many keyboard layouts in " << KEYMAP_LIST_FILE;
    }

    return ret;
}

QString
input_dbus_proxy::get_current_kb_layout()
{
    FILE *f = NULL;
    char layout[257];

    f = fopen(KEYMAP_CONF_FILE, "r");
    if (!f) {
        qWarning() << "Unable to open keymap configuration file " << KEYMAP_CONF_FILE;
        return QString();
    }

    // Read the keyboard layout...
    int count = fscanf(f, " KEYBOARD='%256[^']'", layout);
    fclose(f);

    if (!count) {
        qWarning() << "Could not parse keyboard configuration!\n";
        return QString();
    }

    return QString::fromLocal8Bit(layout);
}

void
input_dbus_proxy::set_current_kb_layout(const QString &layout)
{
    QProcess process(this);

    process.start("/usr/bin/loadkeys", QStringList() << layout);
    if (!process.waitForFinished(2000)) {
        qWarning() << "Loadkeys failed to execute, code=" << process.exitCode();
        return;
    }

    /* write keymap to conf file */
    FILE *f = fopen(KEYMAP_CONF_FILE, "w");
    if (f) {
        fprintf(f, "KEYBOARD='%s'\n", layout.toStdString().c_str());
        fflush(f);
        fsync(fileno(f));
        fclose(f);
    }

    /* write keymap to xenstore */
    struct xs_handle *handle = xs_open(0);
    xs_write(handle, XBT_NULL, "/xenclient/keyboard/layout", layout.toStdString().c_str(), layout.length());
    xs_close(handle);
}

int
input_dbus_proxy::get_focus_domid()
{
    if (!m_is || !m_is->get_focused_vm()) {
        return -1;
    }
    return m_is->get_focused_vm()->base()->domid();
}

bool
input_dbus_proxy::switch_focus(int domid, bool force)
{
    return m_is->switch_focus(domid, force);
}

int
input_dbus_proxy::get_mouse_speed()
{
    QString value = m_db->read("/mouse/speed");

    if (!value.isEmpty()) {
        bool success;
        int32_t mouse_speed = value.toInt(&success, 10);
        if (success) {
            // Force to valid range
            mouse_speed = std::min(10, std::max(1, mouse_speed));

            return mouse_speed;
        }
    } else {
        return -1;
    }

    return -1;
}

void
input_dbus_proxy::set_mouse_speed(int mouse_speed)
{
    // Force to valid range
    mouse_speed = std::min(10, std::max(1, mouse_speed));

    m_db->write("/mouse/speed", QString::number(mouse_speed));

    emit set_mouse_speed_f((float) mouse_speed * 2.f / 10 - 1);
}

QString
input_dbus_proxy::touchpad_get(const QString &prop)
{
    QString value;

    if (prop == "speed") {
        value = m_db->read("/touchpad/speed");

        bool success;
        int32_t touchpad_speed = value.toInt(&success, 10);
        if (success) {
            // Force to valid range
            touchpad_speed = std::min(10, std::max(1, touchpad_speed));

            return QString::number(touchpad_speed);
        }
    } else if (prop == "scrolling-enable") {
        value = m_db->read("/touchpad/scrolling-enable");

        if (value == "true" || value == "false") {
            return value;
        }
    } else if (prop == "tap-to-click-enable") {
        value = m_db->read("/touchpad/tap-to-click-enable");

        if (value == "true" || value == "false") {
            return value;
        }
    }

    return QString();
}

void
input_dbus_proxy::touchpad_set(const QString &prop, const QString &value)
{
    if (prop == "speed") {
        bool success;

        int32_t touchpad_speed = value.toInt(&success, 10);

        if (success) {
            // Force to valid range
            touchpad_speed = std::min(10, std::max(1, touchpad_speed));

            m_db->write("/touchpad/speed", QString::number(touchpad_speed));

            emit set_touchpad_speed((float) touchpad_speed * 2.f / 10 - 1);
        } else {
            return;
        }
    } else if (prop == "scrolling-enable") {
        if (value == "true" || value == "false") {
            m_db->write("/touchpad/scrolling-enable", value);

            emit set_touchpad_scrolling(value == "true");
        }
    } else if (prop == "tap-to-click-enable") {
        if (value == "true" || value == "false") {
            m_db->write("/touchpad/tap-to-click-enable", value);

            emit set_touchpad_tap_to_click(value == "true");
        }
    }
}

void
input_dbus_proxy::update_idle_timer(const QString &timer_name, int timeout)
{
    emit idle_timer_update(timer_name, timeout);
}

bool
input_dbus_proxy::get_seamless_mousing()
{
    return m_is->get_seamless_mousing();
}

bool
input_dbus_proxy::set_seamless_mousing(bool enabled)
{
    return m_is->set_seamless_mousing(enabled);
}

bool
input_dbus_proxy::reload_config_from_disk()
{
    Config config;

    json j = config.load(QUrl(INPUT_CONFIG_DEFAULT_URL));
    // if (!j.empty()) {
    // If additional checking becomes necessary do it here, leave j empty if
    // you wish it to fail.
    // }

    if (!j.empty()) {
        return m_is->save_config(j);
    }

    qWarning() << "There is something wrong with the config file.";
    return false;
}

#ifdef TAP_INPUT
bool
input_dbus_proxy::get_tap_input()
{
    QString value = m_db->read("/tap_input/enabled");
    bool ret = false;

    if (!value.isEmpty()) {
        if (value.toLower() == "true") {
            ret = true;
        }
    }

    return ret;
}

void
input_dbus_proxy::set_tap_input(bool enabled)
{
    if (enabled) {
        m_db->write("/tap_input/enabled", "true");
    } else {
        m_db->write("/tap_input/enabled", "false");
    }
    emit set_tap_input_f(enabled);
}

void
input_dbus_proxy::clear_tap_input()
{
    emit clear_tap_input_f();
}
#else
bool
input_dbus_proxy::get_tap_input()
{
    return false;
}

void
input_dbus_proxy::set_tap_input(bool enabled)
{
    (void) enabled;
    return;
}

void
input_dbus_proxy::clear_tap_input()
{
    return;
}
#endif //TAP_INPUT

//
// Internal Methods
//

void
input_dbus_proxy::watched_value_changed(const QString &path)
{
    QString value;

    if (path.isEmpty()) {
        return;
    }

    if (m_db->exists(path)) {
        value = m_db->read(path);

        if (path == "/switcher/enabled") {
            if (value == "true") {
                set_seamless_mousing(true);
            } else if (value == "false") {
                set_seamless_mousing(false);
            }
        } else if (path == "/ui/screenblanking-timeout") {
            bool success;
            int timer = value.toInt(&success, 10);
            if (success) {
                update_idle_timer("screen-blanking", timer * 60);
            }
        }
    } else {
        if (path == "/switcher/enabled") {
            set_seamless_mousing(true);
        } else if (path == "/ui/screenblanking-timeout") {
            update_idle_timer("screen-blanking", 0);
        }
    }
}

//
//  DBus JUNK
//      not supported anymore, but too integrated with xenmgr to easily remove
//

bool
input_dbus_proxy::auth_begin()
{
    return false;
}

void
input_dbus_proxy::auth_clear_status()
{
    return;
}

void
input_dbus_proxy::auth_collect_password()
{
    return;
}

void
input_dbus_proxy::auth_create_hash(const QString &, const QString &)
{
    return;
}

QString
input_dbus_proxy::auth_get_context(const QString &, int)
{
    return QString();
}

QString
input_dbus_proxy::auth_get_status(bool, int &)
{
    return QString();
}

void
input_dbus_proxy::auth_remote_login(const QString &, const QString &)
{
    return;
}

void
input_dbus_proxy::auth_remote_status(bool, int, const QString &, const QString &, const QString &, uint)
{
    return;
}

bool
input_dbus_proxy::auth_rm_platform_user(QString &)
{
    return false;
}

void
input_dbus_proxy::auth_set_context(const QString &, const QString &)
{
    return;
}

void
input_dbus_proxy::auth_set_context_flags(const QString &, const QString &, int)
{
    return;
}

QString
input_dbus_proxy::auth_title()
{
    return QString();
}

void
input_dbus_proxy::divert_keyboard_focus(const QString &)
{
    return;
}

void
input_dbus_proxy::divert_mouse_focus(const QString &, uint, uint, uint, uint, uint, uint, uint, uint)
{
    return;
}

void
input_dbus_proxy::focus_mode(int)
{
    return;
}

bool
input_dbus_proxy::get_auth_on_boot()
{
    return false;
}

int
input_dbus_proxy::get_idle_time()
{
    return 0;
}

int
input_dbus_proxy::get_last_input_time()
{
    return 0;
}

uint
input_dbus_proxy::get_lid_state()
{
    return -1;
}

QString
input_dbus_proxy::get_platform_user(int &)
{
    return QString();
}

QString
input_dbus_proxy::get_remote_user_hash(const QString &)
{
    return QString();
}

QString
input_dbus_proxy::get_user_keydir(const QString &)
{
    return QString();
}

void
input_dbus_proxy::lock(bool)
{
    return;
}

int
input_dbus_proxy::lock_timeout_get()
{
    return -1;
}

void
input_dbus_proxy::lock_timeout_set(int)
{
    return;
}

void
input_dbus_proxy::set_auth_on_boot(bool)
{
    return;
}

void
input_dbus_proxy::set_slot(int, int)
{
    return;
}

void
input_dbus_proxy::stop_keyboard_divert()
{
    return;
}

void
input_dbus_proxy::stop_mouse_divert()
{
    return;
}

//
// Act on the focused VM. Callers should put the VM they want to 'touch'
// into focus first.
//
void
input_dbus_proxy::touch()
{
    auto vm = m_is->get_focused_vm();
    if (vm) {
        vm->wake_up_toggle();
    }
    return;
}

void
input_dbus_proxy::update_seamless_mouse_settings(const QString &)
{
    return;
}
