//
// Glass Display
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#ifndef CONSOLE_SINK__H
#define CONSOLE_SINK__H

#include <guestinputsink.h>

class console_sink_t : public guest_input_sink_t
{
    Q_OBJECT

public:
    explicit console_sink_t(int32_t domid);
    ~console_sink_t();

    void set_inactive();
    void set_active();

public slots:
    void enqueue_input_event(xt_input_event event);

private:
    int32_t m_domid;
    bool m_active;
};

#endif // CONSOLE_SINK__H
