//
// Glass Display
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#include <qemu2sink.h>

qemu2_sink_t::qemu2_sink_t(domid_t domid) : m_ivc_server(domid, QEMU2_INPUT_PORT),
                                            m_input_channel(nullptr),
                                            m_led_code(0)
{
    QObject::connect(&m_ivc_server, SIGNAL(client_ready(void *)), this, SLOT(new_client(void *)), Qt::QueuedConnection);
}

qemu2_sink_t::~qemu2_sink_t()
{
}

void
qemu2_sink_t::handle_disconnect()
{
    QMutexLocker locker(&m_lock);
    if (m_input_channel && m_input_channel->connected()) {
        m_input_channel->disconnect();
        m_input_channel = nullptr;
    }

    vg_debug() << DTRACE << m_input_channel.get();
}

void
qemu2_sink_t::new_client(void *c)
{
    TRACE;
    QMutexLocker locker(&m_lock);
    struct libivc_client *client = reinterpret_cast<struct libivc_client *>(c);

    if (!client) {
        return;
    }

    m_input_channel = std::make_unique<ivc_connection_t>(
        client,
        [&](struct libivc_client *cli) {
            size_t size_read = 0;
            libivc_read(cli,
                        (char *) &m_led_code,
                        sizeof(m_led_code),
                        &size_read);
            emit keyboard_led_changed(m_led_code);
        },
        [&](struct libivc_client *cli) {
            (void) cli;
            handle_disconnect();
        });

    if (!m_input_channel) {
        vg_warning() << DTRACE << "Failed to wrap new ivc connection";
    }
}

void
qemu2_sink_t::enqueue_input_event(xt_input_event event)
{
    QMutexLocker locker(&m_lock);
    if (!m_input_channel || !m_input_channel->connected()) {
        // Drop event
        return;
    }

    qemu_xt_input_event qemu_event;
    if (transform_xt_input_event_for_qemu(&event, &qemu_event)) {
        if (SUCCESS != libivc_send(m_input_channel->client(), (char *) &qemu_event, sizeof(qemu_xt_input_event))) {
            vg_debug() << "Failed to queue event on input IVC channel";
        } else {
            //for debugging non-operation
            vg_debug() << "qemu2_sinkt_t::afo::write_event: type: " << qemu_event.type << " flags: " << qemu_event.flags
                       << " absX,Y: " << qemu_event.absX << "," << qemu_event.absY << " absX,YMax: " << qemu_event.absXMax
                       << "," << qemu_event.absYMax << " relX,Y,Z: " << qemu_event.relX << "," << qemu_event.relY
                       << "," << qemu_event.relZ << " keyCode: " << qemu_event.keyCode;
        }
    }
}

/*
 * Thinking about what to write here. I spent some time trying to get the new
 * event working in qemu ( vglass.h/c ) and was not successful. The stubdom
 * kept crashing on creation. So this is an intermediate fix to stabilize the baseline
 * and should definitely be removed at a later date.
 *
 * Another overlooked issue is that the xt_input_event is defined in another file in
 * the qemu project, vglass.c. Not vglass.h.. in our patch to the qemu project.
 */
bool
qemu2_sink_t::transform_xt_input_event_for_qemu(xt_input_event_t *new_event, qemu_xt_input_event *qemu_event)
{
    bool retVal = true;

    try {
        //transform
        qemu_event->type = new_event->type;
        qemu_event->flags = new_event->flags;

        qemu_event->absX = new_event->data.absolute.absX;
        qemu_event->absY = new_event->data.absolute.absY;

        //absXMax versus rel
        if (new_event->type == XT_TYPE_ABSOLUTE) {
            qemu_event->absXMax = new_event->data.absolute.absXMax;
            qemu_event->absYMax = new_event->data.absolute.absYMax;
        } else if (new_event->type == XT_TYPE_RELATIVE) {
            qemu_event->relX = new_event->data.relative.relX;
            qemu_event->relY = new_event->data.relative.relY;
        }

        //this is used in qemu for wheel up/downs.
        int relZ = relZ_float_to_int(new_event->data.relative.relZ);
        if (relZ > 1)
            qemu_event->relZ = 1;
        else if (relZ < -1)
            qemu_event->relZ = -1;
        else
            qemu_event->relZ = relZ;
        qemu_event->keyCode = new_event->keyCode;
    } catch (...) {
        retVal = false;
    }

    return retVal;
}
