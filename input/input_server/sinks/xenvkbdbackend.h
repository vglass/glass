//
// Glass Display
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#ifndef XEN_VKBD_BACKEND__H
#define XEN_VKBD_BACKEND__H

#include <QMap>
#include <QObject>
#include <QSocketNotifier>
#include <memory>
#include <render_target_plane.h>
#include <stdint.h>
#include <xen/event_channel.h>
#include <xen/io/kbdif.h>
#include <xen/io/protocols.h>
#include <xen/io/ring.h>
#include <xenbackend.h>

class xen_vkbd_backend_t;

class vkbd_device_t : public QObject
{
    Q_OBJECT

public:
    vkbd_device_t(void) : m_backend(NULL), m_backend_vkbd(NULL), m_backend_vinput(NULL), m_device_id(0), m_evt_chn_fd(-1), m_evt_chn_event(NULL), m_owner(NULL), m_page(NULL), m_use_absolute(false), m_use_multi_touch(false) {}
    ~vkbd_device_t() {}

    xen_backend_t m_backend;        // In-use pointer to one of the below
    xen_backend_t m_backend_vkbd;   // Registration pointer
    xen_backend_t m_backend_vinput; // Registration pointer
    int32_t m_device_id;
    int32_t m_evt_chn_fd;
    std::shared_ptr<QSocketNotifier> m_evt_chn_event;
    xen_vkbd_backend_t *m_owner;
    void *m_page;

    bool m_use_absolute;
    bool m_use_multi_touch;
};

class xen_vkbd_backend_t : public QObject
{
    Q_OBJECT

public:
    xen_vkbd_backend_t(int32_t domid, bool is_linux);
    ~xen_vkbd_backend_t(void);

    bool init(void);
    static xen_device_t backend_alloc(xen_backend_t backend, int32_t devid, void *priv);
    static xen_device_t backend_alloc_vkbd(xen_backend_t backend, int32_t devid, void *priv);
    static int32_t backend_init(xen_device_t dev);
    static int32_t backend_connect(xen_device_t dev);
    static void backend_disconnect(xen_device_t dev);
    static void backend_free(xen_device_t dev);

    void write_to_ring(union xenkbd_in_event *event);
    bool is_connected() const;

    int32_t m_backend_domid;
    int32_t m_domid;
    int m_retry;

    bool absolute_enabled() { return m_absolute_enabled; }

signals:

    void connection_status(bool is_connected);

private slots:

    void event_channel_ready(int32_t fd);
    void finish_backend_connect();

private:
    static xen_device_t backend_alloc_common(xen_backend_t backend, int32_t devid, void *priv, bool vinput);
    std::shared_ptr<vkbd_device_t> m_device;
    QMap<int32_t, vkbd_device_t *> m_fd_to_device_map;
    bool m_is_connected;
    int32_t m_device_width_pixels;
    int32_t m_device_height_pixels;

    bool m_absolute_enabled{true};
    bool m_is_linux{false};
};

#endif //XEN_VKBD_BACKEND__H
