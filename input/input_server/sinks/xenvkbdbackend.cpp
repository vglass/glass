//
// Glass Display
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#include <QTimer>
#include <vkbdsingleton.h>
#include <vkbdsink.h>
#include <xenvkbdbackend.h>
extern "C" {
#include <sys/types.h>
#include <unistd.h>
#include <xen/io/xenbus.h>
#include <xenstore.h>
}

#define RETRY_COUNT 20

static std::shared_ptr<vkbd_singleton_t> be = NULL;

xen_vkbd_backend_t::xen_vkbd_backend_t(int32_t domid, bool is_linux) : m_backend_domid(0), m_domid(domid), m_device(std::make_shared<vkbd_device_t>()), m_is_connected(false), m_device_width_pixels(XT_LOGICAL_SCREEN_WIDTH), m_device_height_pixels(XT_LOGICAL_SCREEN_HEIGHT), m_is_linux(is_linux)
{
    if (be == NULL)
        be = std::make_shared<vkbd_singleton_t>(m_backend_domid);
}

xen_vkbd_backend_t::~xen_vkbd_backend_t(void)
{
    if (m_device->m_backend_vkbd)
        backend_release(m_device->m_backend_vkbd);
    if (m_device->m_backend_vinput)
        backend_release(m_device->m_backend_vinput);
}

xen_device_t
xen_vkbd_backend_t::backend_alloc_common(xen_backend_t backend, int32_t devid, void *priv, bool vinput)
{
    xen_vkbd_backend_t *local = reinterpret_cast<xen_vkbd_backend_t *>(priv);

    Q_CHECK_PTR(local);

    if (local->m_device->m_backend) {
        qDebug() << __func__ << "m_backend already set for" << local->m_domid;
        return NULL;
    }

    // Release the complementary device which won't be used.
    if (vinput) {
        qDebug() << "Using vinput for" << local->m_domid;
        if (local->m_device->m_backend_vkbd) {
            backend_release(local->m_device->m_backend_vkbd);
            local->m_device->m_backend_vkbd = NULL;
        }
    } else {
        qDebug() << "Using vkbd for" << local->m_domid;
        if (local->m_device->m_backend_vinput) {
            backend_release(local->m_device->m_backend_vinput);
            local->m_device->m_backend_vinput = NULL;
        }
    }

    // Store the backend-relevant data in our internal device structure.
    local->m_device->m_backend = backend;
    local->m_device->m_device_id = devid;
    local->m_device->m_page = NULL;

    vkbd_device_t *vdev = local->m_device.get();
    Q_CHECK_PTR(vdev);

    // Keep a reference to this object in the singleton object to prevent
    // the object's refcount to dropping to zero while libxenbackend still
    // has a reference to it.
    be->register_backend(vdev->m_owner);

    if (backend_print(vdev->m_backend, vdev->m_device_id, "width", "32768") < 0) {
        vg_debug() << "Failed to set width...";
    }

    if (backend_print(vdev->m_backend, vdev->m_device_id, "height", "32768") < 0) {
        vg_debug() << "Failed to set height...";
    }

    if (backend_print(vdev->m_backend, vdev->m_device_id, "feature-abs-pointer", "1") < 0) {
        vdev->m_owner->m_absolute_enabled = false;
        vg_debug() << "Failed to set feature-abs-pointer...";
    }

    if (backend_print(vdev->m_backend, vdev->m_device_id, "feature-raw-pointer", "1") < 0) {
        vg_debug() << "Failed to set feature-raw-pointer...";
    }

    //multitouch writes values to the front end..
    QString szEnableMultiTouch("1"); //this is bound to a command line switch,
    QString szNumContacts("10");
    //just settings 10 because that is how many
    //fingers I have

    //get dimensions from wm
    QString szScreenWidthInPixels = QString("%1").arg(vdev->m_owner->m_device_width_pixels);
    QString szScreenHeightInPixels = QString("%1").arg(vdev->m_owner->m_device_height_pixels);

    if (backend_print(vdev->m_backend, vdev->m_device_id, "feature-multi-touch", szEnableMultiTouch.toStdString().c_str()) < 0) {
        vg_debug() << "Failed to set feature-multi-touch...";
    }

    if (backend_print(vdev->m_backend, vdev->m_device_id, "multi-touch-num-contacts", szNumContacts.toStdString().c_str()) < 0) {
        vg_debug() << "Failed to set multi-touch-num-contacts...";
    }

    if (backend_print(vdev->m_backend, vdev->m_device_id, "multi-touch-width", szScreenWidthInPixels.toStdString().c_str()) < 0) {
        vg_debug() << "Failed to set multi-touch-width...";
    }

    if (backend_print(vdev->m_backend, vdev->m_device_id, "multi-touch-height", szScreenHeightInPixels.toStdString().c_str()) < 0) {
        vg_debug() << "Failed to set multi-touch-height...";
    }

    return (xen_device_t) local->m_device.get();
}

xen_device_t
xen_vkbd_backend_t::backend_alloc(xen_backend_t backend, int32_t devid, void *priv)
{
    return backend_alloc_common(backend, devid, priv, true);
}

xen_device_t
xen_vkbd_backend_t::backend_alloc_vkbd(xen_backend_t backend, int32_t devid, void *priv)
{
    return backend_alloc_common(backend, devid, priv, false);
}

int32_t
xen_vkbd_backend_t::backend_init(xen_device_t dev)
{
    (void) dev;
    return 0;
}

void
xen_vkbd_backend_t::finish_backend_connect()
{
    int val = 0;
    vkbd_device_t *vdev = m_device.get();
    Q_CHECK_PTR(vdev);
    Q_CHECK_PTR(vdev->m_owner);

    if (vdev->m_owner->m_retry == RETRY_COUNT) {
        vg_debug() << "Failed to set correct logical extents for kbdfront.";
    }

    // If the m_evt_chn_fd isn't -1 to start, backend_bind_evtchn will fail. The second check in the function is
    vdev->m_evt_chn_fd = backend_bind_evtchn(vdev->m_backend, vdev->m_device_id);

    if (vdev->m_evt_chn_fd < 0) {
        qDebug() << "Failed to get event channel file descriptor";
        return;
    }

    // Check to see if the evt_chn was initialized to ensure a watch that fires multiple times doesn't screw things up.
    if (vdev->m_evt_chn_event == NULL) {
        //vdev->m_evt_chn_event = std::make_shared<QSocketNotifier>(vdev->m_evt_chn_fd, QSocketNotifier::Read);
        //Q_CHECK_PTR(vdev->m_evt_chn_event);
    }

    if (frontend_scan(vdev->m_backend, vdev->m_device_id, "request-abs-pointer", "%d", &val) == 1) {
        if (val) {
            vdev->m_use_absolute = true;
            qDebug() << QString(vdev->m_owner->m_domid) << " requested absolute pointer...";
        } else {
            vdev->m_use_absolute = false;
            qDebug() << QString(vdev->m_owner->m_domid) << " requested relative pointer...";
        }
    } else {
        qDebug() << "Failed to read request-abs-pointer...";
    }

    //reading multitouch values from front end.. the guest kernel will have written these
    if (frontend_scan(vdev->m_backend, vdev->m_device_id, "request-multi-touch", "%d", &val) == 1) {
        if (val) {
            vdev->m_use_multi_touch = true;
            qDebug() << QString(vdev->m_owner->m_domid) << " supports multi touch events...";
        } else {
            vdev->m_use_multi_touch = false;
            qDebug() << QString(vdev->m_owner->m_domid) << " does not support multi touch events...";
        }
    } else {
        qDebug() << "Failed to read request-multi-touch...";
    }

    // Presumed to hang.
    //connect(vdev->m_evt_chn_event.get(), &QSocketNotifier::activated,
    //        vdev->m_owner, &xen_vkbd_backend_t::event_channel_ready, Qt::QueuedConnection);

    vdev->m_page = backend_map_shared_page(vdev->m_backend, vdev->m_device_id);
    Q_CHECK_PTR(vdev->m_page);

    vdev->m_owner->m_fd_to_device_map[vdev->m_evt_chn_fd] = vdev;

    vdev->m_owner->m_is_connected = true;
    emit vdev->m_owner->connection_status(true);
    return;
}

int32_t
xen_vkbd_backend_t::backend_connect(xen_device_t dev)
{
    vkbd_device_t *vdev = reinterpret_cast<vkbd_device_t *>(dev);
    int fe_state = 0;
    Q_CHECK_PTR(vdev);
    Q_CHECK_PTR(vdev->m_owner);

    // There is a scenario where kdbfront won't be ready and the state machine might
    // skip a step and initialize early, so we check for the correct state before
    // proceeding. Make sure to set retry count to 0 before initiating the check.
    vdev->m_owner->m_retry = 0;
    frontend_scan(vdev->m_backend, vdev->m_device_id, "state", "%d", &fe_state);
    while (fe_state != XenbusStateConnected && vdev->m_owner->m_retry < RETRY_COUNT && vdev->m_owner->m_is_linux) {
        QThread::msleep(250);
        frontend_scan(vdev->m_backend, vdev->m_device_id, "state", "%d", &fe_state);
        vdev->m_owner->m_retry++;
    }

    vdev->m_owner->finish_backend_connect();

    return 0;
}

void
xen_vkbd_backend_t::backend_disconnect(xen_device_t dev)
{
    vkbd_device_t *vdev = reinterpret_cast<vkbd_device_t *>(dev);
    Q_CHECK_PTR(vdev);

    // We can't assert on these conditions, because these callbacks get generated multiple times.
    // They are called for xenstore watch events which a single event can cause multiple calls
    if (vdev->m_evt_chn_fd != -1 && vdev->m_evt_chn_event != NULL) {
        vdev->m_evt_chn_event->setEnabled(false);
        backend_unbind_evtchn(vdev->m_backend, vdev->m_device_id);

        // Reset the event channel so a reconnect could happen
        vdev->m_evt_chn_fd = -1;
        vdev->m_evt_chn_event = NULL;
    }

    // If a shared page is mapped, unmap it.
    if (vdev->m_page != NULL) {
        backend_unmap_shared_page(vdev->m_backend, vdev->m_device_id, vdev->m_page);
        vdev->m_page = NULL;
    }

    vdev->m_owner->m_is_connected = false;
    emit vdev->m_owner->connection_status(false);
}

void
xen_vkbd_backend_t::backend_free(xen_device_t dev)
{
    vkbd_device_t *vdev = reinterpret_cast<vkbd_device_t *>(dev);

    xen_vkbd_backend_t::backend_disconnect(dev);

    if (vdev->m_page == NULL && vdev->m_evt_chn_fd == -1 && vdev->m_evt_chn_event == NULL) {
        // Now that the libxenbackend portion has been torn down,
        // we can drop the refcount of this object, and it can
        // ultimately be freed
        be->unregister_backend(vdev->m_owner);
    }
}

static struct
    xen_backend_ops xen_vkbd_backend_ops =
        {
            xen_vkbd_backend_t::backend_alloc,
            xen_vkbd_backend_t::backend_init,
            xen_vkbd_backend_t::backend_connect,
            xen_vkbd_backend_t::backend_disconnect,
            NULL,
            NULL,
            NULL,
            xen_vkbd_backend_t::backend_free};

static struct
    xen_backend_ops xen_vkbd_backend_vkbd_ops =
        {
            xen_vkbd_backend_t::backend_alloc_vkbd,
            xen_vkbd_backend_t::backend_init,
            xen_vkbd_backend_t::backend_connect,
            xen_vkbd_backend_t::backend_disconnect,
            NULL,
            NULL,
            NULL,
            xen_vkbd_backend_t::backend_free};

bool
xen_vkbd_backend_t::init(void)
{
    m_device->m_owner = this;
    qDebug() << "backend_register(\"vinput\") for" << m_domid;
    // backend_register will call backend_alloc() for any detected devices.
    m_device->m_backend_vinput = backend_register("vinput", m_domid, &xen_vkbd_backend_ops, (backend_private_t) this);
    if (m_device->m_backend_vinput == NULL) {
        qDebug() << "Failed to register vinput backend for dom[" << QString(m_domid) << "]";
        return false;
    }

    // m_backend != NULL means backend_alloc already ran, so we don't need to
    // register the alternate backend.
    if (m_device->m_backend == NULL) {
        qDebug() << "backend_register(\"vkbd\") for" << m_domid;
        m_device->m_backend_vkbd = backend_register("vkbd", m_domid, &xen_vkbd_backend_vkbd_ops, (backend_private_t) this);

        if (m_device->m_backend_vkbd == NULL) {
            qDebug() << "Failed to register vkbd backend for dom[" << QString(m_domid) << "]";
            return false;
        }
    } else {
        qDebug() << "Skipping backend_register(\"vkbd\") for" << m_domid;
    }

    return true;
}

void
xen_vkbd_backend_t::write_to_ring(union xenkbd_in_event *event)
{
    struct xenkbd_page *page = (struct xenkbd_page *) m_device->m_page;

    if (!event)
        return;
    if (!page)
        return;

    uint32_t producer = page->in_prod;

    xen_mb();
    XENKBD_IN_RING_REF(page, producer) = *event;
    xen_wmb();

    page->in_prod = producer + 1;

    backend_evtchn_notify(m_device->m_backend, m_device->m_device_id);
}

bool
xen_vkbd_backend_t::is_connected() const
{
    return m_is_connected;
}

void
xen_vkbd_backend_t::event_channel_ready(int32_t fd)
{
    Q_ASSERT(fd > 0);

    backend_evtchn_handler(backend_evtchn_priv(m_fd_to_device_map[fd]->m_backend, m_fd_to_device_map[fd]->m_device_id));
}
