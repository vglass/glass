//
// Glass Display
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#ifndef VKBD_SINGLETON__H
#define VKBD_SINGLETON__H

#include <QSocketNotifier>
#include <xenvkbdbackend.h>

class vkbd_singleton_t : public QObject
{
    Q_OBJECT

public:
    explicit vkbd_singleton_t(int32_t backend_id);
    ~vkbd_singleton_t(void);

    void register_backend(xen_vkbd_backend_t *backend);
    void unregister_backend(xen_vkbd_backend_t *backend);

private slots:

    void backend_event(int32_t fd);

private:
    int32_t m_backend_domid;
    int32_t m_device_count;

    std::shared_ptr<QSocketNotifier> m_xenstore_event;
    QList<xen_vkbd_backend_t *> m_backend_list;
};

#endif // VKBD_SINGLETON__H
