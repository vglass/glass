//
// Glass Display
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#include <consolesink.h>

console_sink_t::console_sink_t(int32_t domid) : m_domid(domid), m_active(false)
{
}

console_sink_t::~console_sink_t(void)
{
}

void
console_sink_t::set_inactive(void)
{
    emit error(QString("ConsoleSink inactive."), 0);
    m_active = false;
}

void
console_sink_t::set_active(void)
{
    emit error(QString("ConsoleSink active."), 0);
    m_active = true;
}

void
console_sink_t::enqueue_input_event(xt_input_event event)
{
    if (m_active) {
        qDebug() << "Dom[" << m_domid << "]:";
        qInfo() << &event;
    }
}
