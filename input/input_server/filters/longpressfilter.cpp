//
// Glass Display
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#include <longpressfilter.h>

long_press_filter_t::long_press_filter_t(uint32_t key_code, std::shared_ptr<input_action_t> action, uint16_t ms_threshold) : m_key_code(key_code), m_action(action), m_threshold(ms_threshold)
{
    // Clear the flags variable for our initial event, so it won't be interpreted as a keypress.
    m_last_event.flags = 0;
    this->set_name("long_press_filter");
}

long_press_filter_t::~long_press_filter_t(void)
{
}

bool
long_press_filter_t::filter_event(std::shared_ptr<vm_t> &vm, xt_input_event *event)
{
    Q_CHECK_PTR(event);
    Q_CHECK_PTR(m_action);

    bool filtered = false;

    // If this isn't a key event, don't track it.
    if (event->type != XT_TYPE_KEY) {
        return false;
    }

    // If this key event completes our long press...
    if (this->completes_long_press(event)) {
        //... forward the key event to the guest, so it gets its requisite release.
        emit out_of_band_event(vm, *event);

        //... trigger our action...
        // This isn't how actions should be used, they should be sent to the input server private to be called.
        (*m_action)();

        //... and prevent this action from filtering to the potentially-new guest.
        //drop the event
        filtered = true;
    }

    // Track the key press time, and the time at which it occurs.
    m_last_event = *event;
    m_last_event_time = QDateTime::currentMSecsSinceEpoch();

    return filtered;
}

bool
long_press_filter_t::completes_long_press(xt_input_event *event)
{
    Q_CHECK_PTR(event);

    // If this isn't the right key, reject it.
    if (m_last_event.keyCode != m_key_code) {
        return false;
    }

    // If this isn't a keyUp, it can't trigger an event.
    if (event->flags != 0) {
        return false;
    }

    // If the last event wasn't a keypress, this wasn't our event.
    if (m_last_event.flags == 0) {
        return false;
    }

    // If the user has interacted with other keys, this isn't a simple
    // press-and-hold; reject it.
    if (m_last_event.keyCode != event->keyCode) {
        return false;
    }

    // Compute the milliseconds passed since our last event...
    auto time_since_last_event = QDateTime::currentMSecsSinceEpoch() - m_last_event_time;

    // ... and return true iff it exceeds our threshold.
    return (time_since_last_event > m_threshold);
}
