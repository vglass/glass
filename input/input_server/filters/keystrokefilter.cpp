//
// Glass Display
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#include <keystrokefilter.h>

key_stroke_filter_t::key_stroke_filter_t(
    std::shared_ptr<input_action_t> entered_action,
    std::shared_ptr<input_action_t> secondary_keyed_action,
    std::shared_ptr<input_action_t> secondary_released_action,
    std::shared_ptr<input_action_t> primary_keyed_action,
    std::shared_ptr<input_action_t> primary_released_action) : m_entered_action(entered_action),
                                                               m_secondary_keyed_action(secondary_keyed_action),
                                                               m_secondary_released_action(secondary_released_action),
                                                               m_primary_keyed_action(primary_keyed_action),
                                                               m_primary_released_action(primary_released_action)
{
    CLEAR_ALL_KEYS(m_current_keymask);
    CLEAR_ALL_KEYS(m_last_keymask);

    this->set_name("key_stroke_filter");
}

key_stroke_filter_t::~key_stroke_filter_t(void)
{
}

bool
key_stroke_filter_t::primary_released(const struct KeyMaskSet &key_mask_set)
{
    return key_mask_set.entered && maskInMask(key_mask_set.primaryMask, m_last_keymask) && !maskInMask(key_mask_set.primaryMask, m_current_keymask);
}

bool
key_stroke_filter_t::primary_pressed(const struct KeyMaskSet &key_mask_set)
{
    return key_mask_set.entered && !maskInMask(key_mask_set.primaryMask, m_last_keymask) && maskInMask(key_mask_set.primaryMask, m_current_keymask);
}

bool
key_stroke_filter_t::secondary_released(const struct KeyMaskSet &key_mask_set)
{
    return key_mask_set.entered && maskInMask(key_mask_set.secondaryMask, m_last_keymask) && !maskInMask(key_mask_set.secondaryMask, m_current_keymask);
}

bool
key_stroke_filter_t::secondary_pressed(const struct KeyMaskSet &key_mask_set)
{
    return key_mask_set.entered && !maskInMask(key_mask_set.secondaryMask, m_last_keymask) && maskInMask(key_mask_set.secondaryMask, m_current_keymask);
}

void
key_stroke_filter_t::evaluate_entry(struct KeyMaskSet &key_mask_set)
{
    KeyMask mask = m_current_keymask;
    CLEAR_KEY_MASK(mask, key_mask_set.primaryMask);
    CLEAR_KEY_MASK(mask, key_mask_set.secondaryMask);
    key_mask_set.entered = maskCleared(mask) && maskInMask(key_mask_set.primaryMask, m_current_keymask) && maskInMask(key_mask_set.secondaryMask, m_current_keymask);
}

bool
key_stroke_filter_t::filter_event(std::shared_ptr<vm_input_t> &vm, xt_input_event *event)
{
    bool rc = false;

    // If this isn't a key event, we don't have any filtering to do.
    if (event->type != XT_TYPE_KEY) {
        return false;
    }

    if (!keyInMask(*event, m_global_white_key_mask)) {
        return false;
    }

    //The stack is expecting 139 and libinput is reporting 127
    //for the right menu key.
    if (event->keyCode == 127) {
        event->keyCode = 139;
        return false;
    }

    // Update the current global key state.
    UPDATE_KEY_MASK(m_current_keymask, (*event));

    for (int i = 0; i < m_triggers.size(); i++) {
        KeyMaskSet &key_mask_set = m_triggers[i];

        if (keyInMask(*event, key_mask_set.whiteKeyMask)) {

            if (m_primary_keyed_action != NULL && primary_pressed(key_mask_set)) {
                emit filter_fire(m_primary_keyed_action);
            }

            if (!key_mask_set.entered) {
                evaluate_entry(key_mask_set);

                if (key_mask_set.entered) {
                    emit filter_fire(m_entered_action);
                    emit filter_fire(m_secondary_keyed_action);
                    vm->lost_focus();
                    //drop the event
                    rc = true;
                }
            } else {
                if (m_secondary_released_action != NULL && secondary_released(key_mask_set)) {
                    emit filter_fire(m_secondary_released_action);
                } else if (m_secondary_keyed_action != NULL && secondary_pressed(key_mask_set)) {
                    emit filter_fire(m_secondary_keyed_action);
                } else if (m_primary_released_action != NULL && primary_released(key_mask_set)) {
                    emit filter_fire(m_primary_released_action);
                    key_mask_set.entered = false;
                }

                //drop the event
                rc = true;
            }
        }
    }

    UPDATE_KEY_MASK(m_last_keymask, (*event));
    return rc;
}

void
key_stroke_filter_t::add_white_list_keys(struct KeyMask &white_key_mask, const struct KeyMask &primary, const struct KeyMask &secondary)
{
    CLEAR_ALL_KEYS(white_key_mask);
    SET_KEY_MASK(white_key_mask, primary);
    SET_KEY_MASK(white_key_mask, secondary);
}

bool
key_stroke_filter_t::set_triggers(const QString triggers_str)
{
    (void) triggers_str;
    return true;
}

bool
key_stroke_filter_t::clear_triggers()
{
    m_triggers.clear();
    CLEAR_ALL_KEYS(m_global_white_key_mask);
    return true;
}

bool
key_stroke_filter_t::add_trigger(const json trigger)
{
    if ((trigger.find("disabled") != trigger.end()) && (trigger["disabled"].get<bool>() == true)) {
        return false;
    }

    struct KeyMaskSet key_mask_set;
    QPair<KeyMask, KeyMask> mask = input_config_parser_t::get_base_secondary_pair(trigger);

    key_mask_set.primaryMask = mask.first;
    key_mask_set.secondaryMask = mask.second;
    add_white_list_keys(key_mask_set.whiteKeyMask, mask.first, mask.second);
    SET_KEY_MASK(m_global_white_key_mask, mask.first);
    SET_KEY_MASK(m_global_white_key_mask, mask.second);

    key_mask_set.entered = false;

    m_triggers.append(key_mask_set);

    return true;
}
