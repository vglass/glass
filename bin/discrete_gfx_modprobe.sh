#! /bin/bash
#
# Discrete graphics initialization script; correctly loads the modules
# for a discrete graphics card-- ensuring that only the Boot VGA device is
# available for use in dom0. Other GPUs are reserved for passthrough.
#
# Copyright (c) 2015 Assured Information Security, inc.
#   Author: Kyle J. Temkin
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#

#
# Keep in mind that this script can be /sourced/ as well as executed;
# if you add code, don't exit unless you mean it.
#

#
# Wait for a given device to show up as bound to pciback.
#
function wait_for_pciback() {
  # Get the device's (domain):BDF...
  local BDF=$1
  local timeout=15

  while [ $timeout -gt 0 ]; do
    # Wait one second so the bind can occur...
    sleep 1
    timeout=$((timeout - 1))

    # If we've found that the device is bound to pciback, return.
    if [ -d "/sys/bus/pci/drivers/pciback/$BDF" ]; then
      return 0

    # Otherwise, warn the user and try again.
    else
      echo "Discrete graphics not yet bound to pciback."
      echo "Waiting up to $timeout more seconds."
    fi
  done

  return 1
}

#
# Attempt to bind a given device to pciback, preventing it from being used in dom0.
# Accepts either a (Domain)-BDF, or a sysfs PCI device path.
#
function bind_to_pciback() {
  # Get the device's (domain):BDF...
  local BDF="$1"

  # ... and use that to bind the device to PCIback.
  echo "${BDF}" > "/sys/bus/pci/drivers/pciback/new_slot"
  echo "${BDF}" > "/sys/bus/pci/drivers/pciback/bind"
}


# Get a list of GPUs from XenMgr, or fail out.
GPUS=$(xec-vm -o /host list-gpu-devices) || exit 1

# First, ensure that Xen's pciback driver has already been loaded.
modprobe xen-pciback >& /dev/null

#
# Next, we'll need to limit the devices that can bind to this graphics module,
# so only the Boot VGA is accessible from dom0. The other devices will be reserved
# for passthrough.
#
for device_path in /sys/bus/pci/devices/*; do
  BDF=$(basename "${device_path}")

  # If this isn't a passthrough GPU, skip it.
  if ! echo "${GPUS}" | grep -q "${BDF}"; then
    continue
  fi
  # If this is the boot VGA device, skip it.
  if [ -r "${device_path}/boot_vga" ] && \
    [ "$(cat ${device_path}/boot_vga)" = "1" ]; then
    continue
  fi

  # ... and it's not already bound to pciback...
  if ! [ -d "/sys/bus/pci/drivers/pciback/$BDF" ]; then
    echo "[VGlass] Binding graphics card $BDF to pciback..."

    # ... bind it to pciback, effectively reserving it for passthrough.
    bind_to_pciback "${BDF}"

    # Wait for the device to be bound to pciback.
    # If it never appears, bail out!
    wait_for_pciback "${BDF}"
  fi
done

# Finally, load the supported drivers.
modprobe --ignore-install nouveau
modprobe --ignore-install radeon
