//
// Glass Display
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#ifndef RENDER_PLANE__H
#define RENDER_PLANE__H

#include <render_target_plane.h>
// See definitions in common/include/plane.h

using render_key_t = uint32_t;

class render_plane_t : public plane_t
{
public:
    render_plane_t(rect_t rect = rect_t(0, 0, 0, 0), QPoint plane_origin = QPoint(0, 0), render_key_t key = 0);
    virtual ~render_plane_t() = default;

    virtual void set_key(uint32_t key);

    virtual uint32_t key()
    {
        return m_key;
    }

private:
    uint32_t m_key;
};

#endif // RENDER_PLANE__H
