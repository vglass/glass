//
// Glass Display
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#ifndef RENDER_SOURCE_PLANE__H
#define RENDER_SOURCE_PLANE__H

#include <cursor.h>
#include <framebuffer.h>
#include <plane.h>
// See definitions in common/include/plane.h

using window_key_t = uint32_t;

class render_source_plane_t : public plane_t
{
public:
    render_source_plane_t(rect_t rect = rect_t(0, 0, 0, 0),
                          QPoint plane_origin = QPoint(0, 0),
                          std::shared_ptr<plane_t> parent = nullptr);
    virtual ~render_source_plane_t() { vg_debug() << "render source plane destroyed: " << this; }
    virtual std::shared_ptr<cursor_t> cursor();
    virtual std::shared_ptr<QImage> framebuffer();
    virtual region_t dirty_region();
    virtual void set_key(window_key_t key);
    virtual bool valid() { return m_valid; }
    virtual void set_valid(bool valid) { m_valid = valid; }
    virtual void invalidate() { m_valid = false; }
    virtual uint32_t key() { return m_key; }
    virtual bool origin_from_guest() { return m_origin_from_guest; }
    virtual void set_origin_from_guest(bool set) { m_origin_from_guest = set; }

    virtual rect_t parent_rect() { return rect_t(point_t(0, 0), rect().size()); }
    virtual rect_t guest_rect() { return rect_t(origin(), rect().size()); }

protected:
    std::shared_ptr<QImage> m_framebuffer;
    std::shared_ptr<cursor_t> m_cursor;
    region_t m_dirty_region;
    window_key_t m_key;

    bool m_valid = false;
    bool m_origin_from_guest = false;
};

#endif // RENDER_SOURCE_PLANE__H
