//
// Glass Display
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#ifndef WINDOW_MANAGER__H
#define WINDOW_MANAGER__H

#include <glass_types.h>

#include <QObject>

#include <battery_overlay.h>
#include <display_plane.h>
#include <input_plane.h>
#include <plane.h>
#include <render_plane.h>
#include <render_target_plane.h>
#include <switcher_overlay.h>
#include <text_overlay.h>
#include <vm.h>

const uint32_t TRANSLATE_FAIL = 0;
const uint32_t TRANSLATE_SUCCESS = 1;
const uint32_t TRANSLATE_SOLARIS_HACK = 2;

typedef struct guest_mouse_event {
    guest_mouse_event(point_t event_point, QSize guest_size)
    {
        point = event_point;
        size = guest_size;
        translate_status = TRANSLATE_FAIL;
    }
    point_t point;
    QSize size;
    uint32_t translate_status;
} guest_mouse_event;

class window_manager_t : public QObject
{
    Q_OBJECT
public:
    window_manager_t();

    virtual ~window_manager_t() = default;

    virtual void add_guest(std::shared_ptr<vm_region_t> vm);
    virtual void update_guest(uuid_t uuid);
    virtual void remove_guest(std::shared_ptr<vm_t> vm);
    virtual region_t dirty_region(uuid_t uuid);
    virtual region_t visible_region(uuid_t uuid);
    virtual region_t &global_visible_region();
    virtual void reset();
    virtual std::shared_ptr<input_plane_t> input_plane();
    virtual desktop_uuid_t desktop_uuid(uuid_t uuid);
    virtual rect_t target(uuid_t uuid, point_t point) = 0;
    // Clamp rects must all be mapped back to input_plane to work properly
    virtual rect_t clamp_rect(uuid_t uuid, point_t point, bool mouse_down) = 0;
    virtual guest_mouse_event map_event_to_guest(uuid_t uuid, point_t point, bool mouse_down, bool has_absolute_sink, bool is_touch) = 0;
    // This is very window manager specific. The window manager's job here is to
    // create the position of the render target(s) for a guest, so it can be used
    // to inform the guest's driver of its options.
    virtual void create_render_targets(uuid_t uuid) = 0;

    // The working set is what the renderer operates on, and is a list,
    // with the front of the list being a tuple that has the in focus vm,
    // and the rest of the list sorted in most recently used order. For
    // the simple case of just full screen window management (one VM visible
    // at a time), the QRegion is just the dirty region.
    virtual list_t<uuid_t> render_focus_stack();
    virtual std::shared_ptr<vm_region_t> vm_at_location(point_t point);

    virtual uuid_t render_focus_vm();
    virtual void reset_global_visibility();
    virtual vm_region_t *vm_region(uuid_t uuid);
    virtual void set_display_power(bool display);
    virtual bool set_seamless_mousing(const bool enabled);
    virtual bool get_seamless_mousing();
    virtual void move_cursor_from_guest(uuid_t uuid, window_key_t key, point_t point);
    virtual bool get_multitouch_enable();
    virtual void set_multitouch_enable(bool f);
    virtual list_t<std::shared_ptr<switcher_overlay_t>> switcher_overlays();
    virtual list_t<std::shared_ptr<text_overlay_t>> text_overlays();
    virtual list_t<std::shared_ptr<banner_overlay_t>> banner_overlays();
    virtual void check_and_set_banner_overlays(desktop_plane_t *desktop, std::shared_ptr<vm_base_t> vm);
public slots:
    virtual void add_plane(std::shared_ptr<desktop_plane_t> plane);
    virtual void set_render_focus(uuid_t uuid);
    virtual void update_render_focus();
    virtual void show_vm_switcher(void);
    virtual void hide_vm_switcher(void);
    virtual void advance_vm_right(uuid_t highlighted_vm);
    virtual void advance_vm_left(uuid_t highlighted_vm);
    virtual void ac_adapter_change(bool ac_in_use);
    virtual void battery_percentage_change(double battery_percentage);
    virtual void text_overlays_on();
    virtual void text_overlays_off();
    virtual void calculate_guest_size(uuid_t uuid);
    virtual void update_render_source(uuid_t uuid, uint32_t key, rect_t rect);
    virtual void center_mouse(uuid_t uuid);

signals:
    void update_hotspot(display_plane_t *display, point_t hotspot, bool mouse_down, uuid_t focus_uuid);

    void set_highlight_vm(uuid_t uuid);
    void hide_switcher_overlay(void);
    void show_switcher_overlay(void);
    void focus_changed(const uuid_t &uuid);
    void battery_refresh(void);
    void render_signal(void);

private:
    void set_render_focus_helper(uuid_t uuid);

protected:
    list_t<uuid_t> m_focus_stack;

    qlist_t<uuid_t> m_pinned_uuid;
    qhash_t<uuid_t, std::shared_ptr<vm_region_t>> m_vms;

    region_t m_global_dirty_region;
    region_t m_global_visible_region;
    bool m_global_update;

    std::shared_ptr<input_plane_t> m_input_plane;

    list_t<std::shared_ptr<switcher_overlay_t>> m_switcher_overlays;
    list_t<std::shared_ptr<text_overlay_t>> m_text_overlays;
    list_t<std::shared_ptr<banner_overlay_t>> m_banner_overlays;

    bool m_seamless_mouse;
    bool m_display_power;

    bool m_enable_multitouch;
};

#endif //WINDOW_MANAGER__H
