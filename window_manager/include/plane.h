//
// Glass Display
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#ifndef PLANE__H
#define PLANE__H

#include <glass_types.h>

// The "render source plane" is the coordinate space native to a single guest
// display. It has a 1:1 mapping to a "render target plane" which is a subset
// of the "render plane". Scaling occurs from "render source plane" to
// "render target plane" if necessary. The "render plane" is the subset of
// pixels eligible for displaying guest data from within the "display plane".
// The "display plane" is the set of all renderable pixels that are contained
// on one display. The "desktop plane" is the set of all display planes with
// the same owner.
// Finally the "input plane" is the set of all desktop planes, including those
// involved in GPU passthrough.
//
// NOTE: Matrix multiplication is not commutative (AB != BA) so order of
// combination is important.

uint
qHash(rect_t rect);

class plane_t
{
public:
    plane_t(rect_t rect = QRect(0, 0, 0, 0), point_t plane_origin = QPoint(0, 0), std::shared_ptr<plane_t> parent = nullptr);
    ~plane_t() { m_parent = nullptr; }

    virtual rect_t parent_rect();

    virtual rect_t rect();
    virtual void set_rect(rect_t rect);

    virtual region_t region() { return m_plane; }

    virtual point_t origin();
    virtual void set_origin(const point_t &point);

    virtual void operator+=(const rect_t &rect);
    virtual void operator-=(const rect_t &rect);

    virtual void reset_plane();

    virtual bool updated() { return m_updated; }
    virtual void set_updated(bool updated) { m_updated = updated; }

    virtual point_t map_to(plane_t *dest_plane, point_t point);
    virtual point_t map_from(plane_t *src_plane, point_t point);

    virtual rect_t map_to(plane_t *dest_plane, rect_t rect);
    virtual rect_t map_from(plane_t *src_plane, rect_t rect);

    virtual region_t map_to(plane_t *dest_plane, region_t region);
    virtual region_t map_from(plane_t *src_plane, region_t region);

    virtual transform_t from(plane_t &plane);
    virtual transform_t to(plane_t &plane);
    virtual transform_t translate(plane_t &plane);

protected:
    region_t m_plane;
    point_t m_plane_origin;
    bool m_updated;
    std::shared_ptr<plane_t> m_parent;
};

#endif // PLANE__H
