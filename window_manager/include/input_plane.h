//
// Glass Display
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#ifndef INPUT_PLANE__H
#define INPUT_PLANE__H

#include <desktop_plane.h>

class input_plane_t : public plane_t
{
public:
    input_plane_t(rect_t rect = rect_t(0, 0, 0, 0), point_t plane_origin = point_t(0, 0), std::shared_ptr<plane_t> parent = nullptr);
    virtual ~input_plane_t() = default;

    rect_t rect();

    virtual void add_desktop(std::shared_ptr<desktop_plane_t> plane);
    virtual qhash_t<uuid_t, std::shared_ptr<desktop_plane_t>> &desktops();

    virtual void reset();

    std::shared_ptr<desktop_plane_t> desktop(point_t point);

    point_t &hotspot() { return m_hotspot; }

    desktop_plane_t *current_desktop() { return m_current_desktop; }
    void set_current_desktop(desktop_plane_t *d) { m_current_desktop = d; }

private:
    qhash_t<uuid_t, std::shared_ptr<desktop_plane_t>> m_desktop_planes;

    point_t m_hotspot;

    // Used for input clamping
    desktop_plane_t *m_current_desktop = nullptr;
};

#endif // INPUT_PLANE__H
