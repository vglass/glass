//
// Glass Display
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#include <desktop_plane.h>
#include <input_plane.h>

desktop_plane_t::desktop_plane_t(uuid_t uuid, rect_t rect, point_t plane_origin, bool renderable, int mode, std::shared_ptr<plane_t> parent) : plane_t(rect, plane_origin, parent), m_renderable(renderable), m_uuid(uuid), m_mode(mode) {}

desktop_plane_t::~desktop_plane_t()
{
}

void
desktop_plane_t::reset_render_planes(uuid_t uuid)
{
    for (auto target : m_render_targets[uuid]) {
        target->reset();
        target = nullptr;
    }

    m_render_targets[uuid].clear();
}

void
desktop_plane_t::reset()
{
    m_current_display = nullptr;

    for (auto display : m_displays) {
        display = nullptr;
    }
    m_displays.clear();

    for (auto list : m_render_targets) {
        for (auto target : list.second) {
            target->reset();
            target = nullptr;
        }
    }
    m_render_targets.clear();

    for (auto source : m_qemu_source) {
        source.second = nullptr;
    }
    m_qemu_source.clear();
}

void
desktop_plane_t::add_display(std::shared_ptr<display_plane_t> display)
{
    if (!display) {
        return;
    }

    for (auto &d : m_displays) {
        if (!d) {
            continue;
        }

        if (d.get() == display.get()) {
            return;
        }
    }

    rect_t extent = display->parent_rect();

    m_visible_region += extent;

    vg_info() << display.get() << extent;

    // Take care of the math-y portion first
    m_plane += extent;

    set_origin(m_plane.boundingRect().topLeft());

    // Now some resource organization stuff
    m_displays.push_back(display);
}

void
desktop_plane_t::add_render_target(uuid_t uuid, std::shared_ptr<render_target_plane_t> render_target)
{
    render_targets(uuid).push_back(std::move(render_target));

    m_visible_region += render_targets(uuid).back()->parent_rect();
}

display_plane_t *
desktop_plane_t::display(point_t point)
{
    for (auto &display : m_displays) {
        if (!display) {
            continue;
        }

        if (display->parent_rect().contains(point)) {
            return display.get();
        }
    }

    return nullptr;
}

display_plane_t *
desktop_plane_t::display(uint32_t key)
{
    for (auto &d : m_displays) {
        if (!d) {
            continue;
        }

        if (d->unique_id() == key) {
            return d.get();
        }
    }

    return nullptr;
}

void
desktop_plane_t::dump_render_targets(uuid_t uuid)
{
    vg_debug() << this << uuid << ": #rtps -> " << render_targets(uuid).size();
    for (auto &rtp : render_targets(uuid)) {
        if (!rtp) {
            vg_debug() << "nullptr stored in render targets for " << uuid;
            continue;
        }

        vg_debug() << uuid << ": " << rtp->origin() << rtp->rect();
    }
}

render_target_plane_t *
desktop_plane_t::render_target(uuid_t uuid, point_t point)
{
    for (auto &rtp : render_targets(uuid)) {
        if (!rtp) {
            continue;
        }

        if (rtp->parent_rect().contains(point)) {
            return rtp.get();
        }
    }

    return nullptr;
}

render_target_plane_t *
desktop_plane_t::render_target(uuid_t uuid, display_plane_t *display)
{
    if (!display) {
        return nullptr;
    }

    if (render_targets(uuid).size() == 1) {
        for (auto &rtp : render_targets(uuid)) {
            if (!rtp) {
                continue;
            }

            if (rtp->parent_rect().intersects(display->parent_rect())) {
                return rtp.get();
            }
        }
    }

    for (auto &rtp : render_targets(uuid)) {
        if (!rtp) {
            continue;
        }

        if (rtp->rect().size() == display->render_plane().rect().size() && rtp->key() == display->unique_id()) {
            return rtp.get();
        }
    }

    return nullptr;
}

render_target_plane_t *
desktop_plane_t::render_target(uuid_t uuid, window_key_t key)
{
    for (auto &rtp : render_targets(uuid)) {
        if (!rtp) {
            continue;
        }

        if (rtp->key() == key) {
            return rtp.get();
        }
    }
    return nullptr;
}

render_source_plane_t *
desktop_plane_t::render_source(uuid_t uuid, window_key_t key)
{
    auto rtp = render_target(uuid, key);
    if (!rtp)
        return nullptr;
    return rtp->render_source();
}

list_t<std::shared_ptr<display_plane_t>> &
desktop_plane_t::displays()
{
    return m_displays;
}

hash_t<uuid_t, list_t<std::shared_ptr<render_target_plane_t>>> &
desktop_plane_t::render_targets()
{
    return m_render_targets;
}

list_t<std::shared_ptr<render_target_plane_t>> &
desktop_plane_t::render_targets(uuid_t uuid)
{
    return m_render_targets[uuid];
}

void
desktop_plane_t::remove_render_target(uuid_t uuid, render_target_plane_t *render_target)
{
    m_render_targets[uuid].remove_if([&](const std::shared_ptr<render_target_plane_t> &rtp) { return render_target == rtp.get(); });
}

bool
desktop_plane_t::renderable()
{
    return m_renderable;
}

region_t &
desktop_plane_t::visible_region()
{
    return m_visible_region;
}

void
desktop_plane_t::remove_guest(uuid_t uuid)
{
    m_qemu_source[uuid] = nullptr;
}

void
desktop_plane_t::reorigin_displays()
{
    vg_info() << "Reorigin displays for plane" << m_uuid;
    for (auto display : m_displays) {
        if (!display) {
            continue;
        }
        vg_info() << "before: " << origin() << display->origin();
        display->set_origin(display->origin() - origin());
        vg_info() << "after: " << origin() << display->origin();
    }
}

void
desktop_plane_t::translate_planes()
{
    m_plane = m_plane.translated(-origin().x(), -origin().y());
    m_visible_region = m_visible_region.translated(-origin().x(), -origin().y());
}

void
desktop_plane_t::set_qemu_source(uuid_t uuid, std::shared_ptr<render_source_plane_t> qemu)
{
    m_qemu_source[uuid] = qemu;
}

void
desktop_plane_t::attach_qemu_source(uuid_t uuid)
{
    qDebug() << "reattaching qemu surfaces";
    if (m_qemu_source[uuid]) {
        for (auto &rtp : render_targets(uuid)) {
            if (!rtp) {
                continue;
            }

            rtp->attach_render_source(m_qemu_source[uuid], true);
            rtp->force_qemu_render_source(true);
        }
    } else
        vg_info() << __FUNCTION__ << ": no qemu surface to reattach!";
}
