//
// Glass Display
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#include <window_manager.h>

window_manager_t::window_manager_t() : m_seamless_mouse(true)
{
    m_input_plane = std::make_shared<input_plane_t>();
}

void
window_manager_t::add_guest(std::shared_ptr<vm_region_t> vm)
{
    Expects(vm);
    uuid_t uuid = vm->uuid();

    if (m_vms.contains(uuid)) {
        m_focus_stack.remove_if([&](uuid_t &u) { return u == uuid; });
    }

    m_vms[uuid] = vm;

    create_render_targets(uuid);
    m_focus_stack.push_front(uuid);

    if (m_focus_stack.size() == 1) {
        set_render_focus_helper(uuid);
    }

    connect(m_vms[uuid].get(), &vm_region_t::calculate_guest_size, this, &window_manager_t::calculate_guest_size);

    auto desktop = m_input_plane->desktops()[desktop_uuid(uuid)];

    if (desktop && !desktop->renderable()) {
        calculate_guest_size(uuid);
    }
}

void
window_manager_t::update_guest(uuid_t uuid)
{
    if (!m_vms.contains(uuid)) {
        return;
    }

    create_render_targets(uuid);
    if (uuid == m_focus_stack.back()) {
        update_render_focus();
    }
}

void
window_manager_t::remove_guest(std::shared_ptr<vm_t> vm)
{
    Expects(vm);
    uuid_t uuid = vm->uuid();

    m_focus_stack.remove_if([&](uuid_t &u) { return u == uuid; });
    m_vms.remove(uuid);
}

region_t
window_manager_t::dirty_region(uuid_t uuid)
{
    auto vm_region = m_vms[uuid];
    Expects(vm_region);

    return vm_region->dirty_region();
}

region_t
window_manager_t::visible_region(uuid_t uuid)
{
    auto vm_region = m_vms[uuid];
    Expects(vm_region);

    return vm_region->visible_region();
}

region_t &
window_manager_t::global_visible_region()
{
    return m_global_visible_region;
}

void
window_manager_t::reset()
{
    m_pinned_uuid.clear();

    for (auto overlay : m_switcher_overlays) {
        overlay = nullptr;
    }
    for (auto overlay : m_text_overlays) {
        overlay = nullptr;
    }
    for (auto overlay : m_banner_overlays) {
        overlay = nullptr;
    }

    m_switcher_overlays.clear();
    m_text_overlays.clear();
    m_banner_overlays.clear();

    m_input_plane->reset();
}

std::shared_ptr<input_plane_t>
window_manager_t::input_plane()
{
    return m_input_plane;
}

desktop_uuid_t
window_manager_t::desktop_uuid(uuid_t uuid)
{
    if (m_pinned_uuid.contains(uuid)) {
        return desktop_uuid_t(uuid);
    } else {
        return desktop_uuid_t();
    }
}

// The working set is what the renderer operates on, and is a list,
// with the front of the list being a tuple that has the in focus vm,
// and the rest of the list sorted in most recently used order. For
// the simple case of just full screen window management (one VM visible
// at a time), the QRegion is just the dirty region.
list_t<uuid_t>
window_manager_t::render_focus_stack()
{
    return m_focus_stack;
}

std::shared_ptr<vm_region_t>
window_manager_t::vm_at_location(point_t point)
{
    //Get the desktop point
    auto desktop = m_input_plane->desktop(point);
    if (!desktop) {
        return nullptr;
    }

    plane_t *desktop_plane = desktop.get();
    point_t desktop_point = m_input_plane->map_to(desktop_plane, point);

    for (std::list<uuid_t>::reverse_iterator rit = m_focus_stack.rbegin(); rit != m_focus_stack.rend(); ++rit) {
        auto uuid = *rit;
        auto vm = m_vms[uuid].get();

        if (!vm) {
            continue;
        }

        bool correct_desktop = (desktop_uuid(uuid) == desktop->uuid());
        bool in_region = (vm->visible_region().contains(desktop_point));

        if (correct_desktop && in_region) {
            return m_vms[uuid];
        }
    }

    return nullptr;
}

uuid_t
window_manager_t::render_focus_vm()
{
    if (m_focus_stack.size() > 0) {
        return m_focus_stack.back();
    }

    return uuid_t();
}

void
window_manager_t::reset_global_visibility()
{
    for (auto &vm : m_vms) {
        if (!vm) {
            continue;
        }

        vm->clear_dirty_region();
    }
}

vm_region_t *
window_manager_t::vm_region(uuid_t uuid)
{
    return m_vms[uuid].get();
}

void
window_manager_t::set_display_power(bool display)
{
    m_display_power = true;

    for (auto &overlay : m_banner_overlays) {
        if (!overlay) {
            continue;
        }

        if (overlay->battery_overlay()) {
            overlay->battery_overlay()->set_visible(display);
        }
        overlay->set_updated(true);
    }
}

bool
window_manager_t::set_seamless_mousing(const bool enabled)
{
    m_seamless_mouse = enabled;

    return m_seamless_mouse; // ? Don't need this?
}

bool
window_manager_t::get_seamless_mousing()
{
    return m_seamless_mouse;
}

void
window_manager_t::move_cursor_from_guest(uuid_t uuid, window_key_t key, point_t point)
{
    auto desktop = m_input_plane->desktops()[desktop_uuid(uuid)];

    if (!desktop || !desktop->renderable() || uuid != m_focus_stack.back()) {
        return;
    }

    render_target_plane_t *rtp = desktop->render_target(uuid, key);

    if (!rtp) {
        return;
    }

    auto rsp = rtp->render_source();
    if (!rsp) {
        return;
    }

    point_t rtp_point = rtp->map_from(rsp, point);
    point_t desktop_point = desktop->map_from(rtp, rtp_point);

    display_plane_t *display = desktop->display(key);

    if (!display) {
        display = desktop->display(desktop_point);
    }

    if (!display) {
        return;
    }

    if (!display->cursor_visible()) {
        for (auto &d : desktop->displays()) {
            if (!d) {
                continue;
            }

            if (d.get() != display) {
                d->hide_cursor();
            }
        }
        display->show_cursor();
    }

    point_t display_point = desktop->map_to(display, desktop_point);

    display->move_cursor(display_point);
}

bool
window_manager_t::get_multitouch_enable()
{
    return m_enable_multitouch;
}

void
window_manager_t::set_multitouch_enable(bool f)
{
    m_enable_multitouch = f;
}

list_t<std::shared_ptr<switcher_overlay_t>>
window_manager_t::switcher_overlays()
{
    return m_switcher_overlays;
}

list_t<std::shared_ptr<text_overlay_t>>
window_manager_t::text_overlays()
{
    return m_text_overlays;
}
list_t<std::shared_ptr<banner_overlay_t>>
window_manager_t::banner_overlays()
{
    return m_banner_overlays;
}

void
window_manager_t::check_and_set_banner_overlays(desktop_plane_t *desktop, std::shared_ptr<vm_base_t> vm)
{
    if (!desktop || !vm.get()) {
        return;
    }
    for (auto &display : desktop->displays()) {
        if (!display) {
            continue;
        }

        for (auto &banner_overlay : m_banner_overlays) {
            if (!banner_overlay) {
                continue;
            }

            if (banner_overlay->display_id() == display->unique_id() && !banner_overlay->configured()) {
                banner_overlay->set_updated(true);
                banner_overlay->set_vm(vm);
                banner_overlay->process_updates(display->framebuffer());
            }
        }
    }
}

void
window_manager_t::add_plane(std::shared_ptr<desktop_plane_t> plane)
{
    if (!plane) {
        return;
    }

    if (uuid_t() != plane->uuid()) {
        m_pinned_uuid.push_back(plane->uuid());
    }

    m_input_plane->add_desktop(plane);

    if (plane->renderable()) {
        for (auto &display : plane->displays()) {
            if (!display) {
                continue;
            }

            std::shared_ptr<switcher_overlay_t> switcher_overlay(new switcher_overlay_t(display->unique_id(), region_t(), m_vms, m_focus_stack));
            std::shared_ptr<text_overlay_t> text_overlay(new text_overlay_t(display->unique_id(), region_t(), display->name()));
            std::shared_ptr<banner_overlay_t> banner_overlay(new banner_overlay_t(display->unique_id(), point_t(0, 0), region_t()));
            std::shared_ptr<battery_overlay_t> battery_overlay(new battery_overlay_t(display->unique_id(), region_t()));

            if (banner_overlay) {
                if (battery_overlay) {
                    banner_overlay->set_battery_overlay(battery_overlay);
                    battery_overlay->set_visible(m_display_power);
                }

                banner_overlay->set_visible(true);
                banner_overlay->set_updated(true);
                m_banner_overlays.push_back(banner_overlay);
            }

            if (switcher_overlay) {
                connect(this, &window_manager_t::set_highlight_vm, switcher_overlay.get(), &switcher_overlay_t::set_highlighted_vm);
                connect(this, &window_manager_t::hide_switcher_overlay, switcher_overlay.get(), &switcher_overlay_t::hide_overlay);
                connect(this, &window_manager_t::show_switcher_overlay, switcher_overlay.get(), &switcher_overlay_t::show_overlay);

                m_switcher_overlays.push_back(switcher_overlay);
            }

            if (text_overlay) {
                m_text_overlays.push_back(text_overlay);
            }
            m_global_dirty_region += display->rect();
            m_global_visible_region += display->rect();
        }
    } else if (m_vms[plane->uuid()]) {
        calculate_guest_size(plane->uuid());
    }
}

void
window_manager_t::set_render_focus(uuid_t uuid)
{
    uuid_t old_focus = m_focus_stack.back();

    if (uuid == old_focus) {
        return;
    }

    auto old_vm = m_vms[old_focus];
    if (old_vm) {
        old_vm->set_visible_updated(true);
        desktop_plane_t *desktop = m_input_plane->desktops()[desktop_uuid(old_focus)].get();
        if (desktop) {
            for (auto &rtp : desktop->render_targets(old_focus)) {
                if (!rtp) {
                    continue;
                }

                rtp->set_updated(true);
            }
        }
    }

    set_render_focus_helper(uuid);

    auto desktop = m_input_plane->desktop(m_input_plane->hotspot());

    if (!desktop) {
        return;
    }

    auto desktop_point = m_input_plane->map_to(desktop.get(), m_input_plane->hotspot());
    auto display = desktop->display(desktop_point);
    auto rtp = desktop->render_target(uuid, desktop_point);

    if (!display) {
        return;
    }

    if (!rtp) {
        return;
    }

    if (rtp->render_source() && rtp->render_source()->cursor()) {
        display->show_cursor(rtp->render_source()->cursor());
    } else {
        display->hide_cursor();
    }
}

void
window_manager_t::update_render_focus()
{
    uuid_t uuid = m_focus_stack.back();
    std::shared_ptr<vm_region_t> vm = m_vms[uuid];
    desktop_plane_t *desktop = m_input_plane->desktops()[desktop_uuid(uuid)].get();

    if (!vm) {
        return;
    }
    if (!desktop) {
        return;
    }
    vm->set_visible_updated(true);
    for (auto display : desktop->displays()) {
        if (!display) {
            continue;
        }

        for (auto &banner_overlay : m_banner_overlays) {
            if (!banner_overlay) {
                continue;
            }

            if (banner_overlay->display_id() == display->unique_id()) {
                banner_overlay->set_updated(true);
                banner_overlay->set_vm(vm->base());
            }
        }
    }
    emit battery_refresh();
}

void
window_manager_t::show_vm_switcher(void)
{
    emit set_highlight_vm(m_focus_stack.back());
    emit show_switcher_overlay();
    m_global_update = true;
    emit render_signal();
}

void
window_manager_t::hide_vm_switcher(void)
{
    emit hide_switcher_overlay();
    m_global_update = true;
    emit render_signal();
}

//
// These are backwards from what you might expect, because the m_focus_stack
// has the latest VM at the back.
//
void
window_manager_t::advance_vm_right(uuid_t highlighted_vm)
{
    auto &vm_list = m_focus_stack;
    QList<uuid_t> vm_stack(vm_list.begin(), vm_list.end());

    if (vm_stack.length() == 0) {
        return;
    }

    if (highlighted_vm == uuid_t()) {
        highlighted_vm = vm_stack.last();
    }

    QListIterator<uuid_t> iter(vm_stack);
    iter.toBack();

    if (iter.hasPrevious()) {
        while (iter.hasPrevious()) {
            if (iter.previous() == highlighted_vm) {
                if (iter.hasPrevious()) {
                    emit set_highlight_vm(iter.previous());
                } else {
                    emit set_highlight_vm(vm_stack.last());
                }
                break;
            }
        }
    } else {
        emit set_highlight_vm(vm_stack.last());
    }
}

void
window_manager_t::advance_vm_left(uuid_t highlighted_vm)
{
    auto &vm_list = m_focus_stack;
    QList<uuid_t> vm_stack(vm_list.begin(), vm_list.end());

    if (vm_stack.length() == 0) {
        return;
    }

    if (highlighted_vm == uuid_t()) {
        highlighted_vm = vm_stack.last();
    }

    QListIterator<uuid_t> iter(vm_stack);

    if (iter.hasNext()) {
        while (iter.hasNext()) {
            if (iter.next() == highlighted_vm) {
                if (iter.hasNext()) {
                    emit set_highlight_vm(iter.next());
                } else {
                    emit set_highlight_vm(vm_stack.first());
                }
                break;
            }
        }
    } else {
        emit set_highlight_vm(vm_stack.first());
    }
}

void
window_manager_t::ac_adapter_change(bool ac_in_use)
{
    for (auto &overlay : m_banner_overlays) {
        if (!overlay) {
            continue;
        }

        if (overlay->battery_overlay()) {
            overlay->battery_overlay()->set_power(ac_in_use);
        }
        overlay->set_updated(true);
    }

    emit render_signal();
}

void
window_manager_t::battery_percentage_change(double battery_percentage)
{
    for (auto &overlay : m_banner_overlays) {
        if (!overlay) {
            continue;
        }

        if (overlay->battery_overlay()) {
            overlay->battery_overlay()->set_value(battery_percentage);
        }
        overlay->set_updated(true);
    }

    emit render_signal();
}

void
window_manager_t::text_overlays_on()
{
    for (auto &overlay : m_text_overlays) {
        if (!overlay) {
            continue;
        }

        overlay->set_updated(true);
        overlay->set_visible(true);
    }
}

void
window_manager_t::text_overlays_off()
{
    for (auto &overlay : m_text_overlays) {
        if (!overlay) {
            continue;
        }

        overlay->set_visible(false);
    }
    m_global_update = true;
}

void
window_manager_t::calculate_guest_size(uuid_t uuid)
{
    auto vm = m_vms[uuid];
    if (!vm) {
        // throw?
        return;
    }

    auto desktop = m_input_plane->desktops()[desktop_uuid(uuid)];

    if (!desktop) {
        return;
    }

    if (!desktop->renderable()) {
        vm->guest_desktop() = desktop->region();
        return;
    }

    region_t guest_desktop;
    list_t<render_target_plane_t *> valid_rtps;

    for (auto &rtp : desktop->render_targets(uuid)) {
        if (!rtp) {
            continue;
        }

        if (rtp->render_source() && rtp->render_source()->valid()) {
            valid_rtps.push_back(rtp.get());
        }
    }

    if (valid_rtps.size() == 1) {
        guest_desktop += valid_rtps.front()->render_source()->parent_rect();
    } else {
        for (auto v_rtp : valid_rtps) {
            if (!v_rtp) {
                continue;
            }

            guest_desktop += v_rtp->render_source()->guest_rect();
        }
    }

    vm->guest_desktop() = guest_desktop;
}

void
window_manager_t::update_render_source(uuid_t uuid, uint32_t key, rect_t rect)
{
    auto vm = m_vms[uuid];
    auto desktop = m_input_plane->desktops()[desktop_uuid(uuid)];

    if (!vm || !desktop) {
        return;
    }

    for (auto &rtp : desktop->render_targets(uuid)) {
        if (!rtp) {
            continue;
        }

        if (rtp->key() == key) {
            auto rsp = rtp->render_source();

            if (rsp) {
                rsp->set_origin(rect.topLeft());
                rsp->set_origin_from_guest(true);
                rsp->set_rect(rect_t(point_t(0, 0), rect.size()));
            }

            break;
        }
    }
    calculate_guest_size(uuid);
}

void
window_manager_t::center_mouse(uuid_t uuid)
{
    auto desktop = m_input_plane->desktops()[desktop_uuid(uuid)];
    auto vm = m_vms[uuid];

    if (!desktop || !vm) {
        return;
    }

    if (uuid == m_focus_stack.back() && !desktop->parent_rect().contains(m_input_plane->hotspot())) {
        auto display = desktop->displays().front();
        if (display) {
            m_input_plane->hotspot() = m_input_plane->map_from(desktop.get(), display->parent_rect().center());
        }
    }
}

void
window_manager_t::set_render_focus_helper(uuid_t uuid)
{
    auto new_vm = m_vms[uuid];
    if (new_vm) {
        desktop_plane_t *desktop = m_input_plane->desktops()[desktop_uuid(uuid)].get();
        if (desktop) {
            new_vm->set_visible_updated(true);
            for (auto &display : desktop->displays()) {
                if (!display) {
                    continue;
                }

                for (auto &banner_overlay : m_banner_overlays) {
                    if (!banner_overlay) {
                        continue;
                    }

                    if (banner_overlay->display_id() == display->unique_id()) {
                        banner_overlay->set_updated(true);
                        banner_overlay->set_vm(m_vms[uuid]->base());
                    }
                }
            }
            for (auto &rtp : desktop->render_targets(uuid)) {
                if (!rtp) {
                    continue;
                }

                rtp->set_updated(true);
            }
        }
    }

    m_focus_stack.remove_if([&](uuid_t &u) { return u == uuid; });
    m_focus_stack.push_back(uuid);

    m_input_plane->set_current_desktop(m_input_plane->desktops()[desktop_uuid(uuid)].get());

    emit focus_changed(uuid);
}
