//
// Glass Display
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#include <input_handler.h>

input_handler_t::input_handler_t(region_t region,
                                 std::function<void(point_t, input_handler_t *)> button_press_fn,
                                 std::function<void(point_t, input_handler_t *)> button_held_fn,
                                 std::function<void(point_t, input_handler_t *)> button_release_fn,
                                 std::function<void(point_t, input_handler_t *)> button_clear_fn) : m_mbutton_state(mbutton_clear),
                                                                                                    m_region(region),
                                                                                                    m_button_press_function(button_press_fn),
                                                                                                    m_button_held_function(button_held_fn),
                                                                                                    m_button_release_function(button_release_fn),
                                                                                                    m_button_clear_function(button_clear_fn),
                                                                                                    m_grabbed(false)
{
}

void
input_handler_t::handle_input(point_t point, bool mouse_down)
{
    if (!m_region.contains(point) && !m_grabbed) {
        // Not our input!
        return;
    }

    process_mbutton_state_transitions(mouse_down);

    switch (m_mbutton_state) {
        case mbutton_press: {
            if (m_button_press_function) {
                m_button_press_function(point, this);
            }
            break;
        }
        case mbutton_held: {
            if (m_button_held_function) {
                m_button_held_function(point, this);
            }
            break;
        }
        case mbutton_release: {
            if (m_button_release_function) {
                m_button_release_function(point, this);
            }
            break;
        }
        case mbutton_clear: {
            if (m_button_clear_function) {
                m_button_clear_function(point, this);
            }
            break;
        }
        default:
            return;
    }

    return;
}

void
input_handler_t::set_region(region_t region)
{
    m_region = region;
}

region_t
input_handler_t::region()
{
    return m_region;
}

QString
input_handler_t::dump_state(mbutton_state state)
{
    switch (state) {
        case mbutton_press:
            return QString("mbutton_press");
        case mbutton_held:
            return QString("mbutton_held");
        case mbutton_release:
            return QString("mbutton_release");
        case mbutton_clear:
            return QString("mbutton_clear");
        default:
            return QString("INVALID_MBUTTON_STATE");
    }
}

void
input_handler_t::dump_state_transition(mbutton_state old_state, mbutton_state new_state)
{
    if (old_state != new_state) {
        //    vg_debug() << dump_state(old_state) << " -> " << dump_state(new_state);
    }
}

void
input_handler_t::process_mbutton_state_transitions(bool mouse_down)
{
    mbutton_state old = m_mbutton_state;

    // Process the state transitions:
    switch (m_mbutton_state) {
        case mbutton_press: {
            if (mouse_down) {
                m_mbutton_state = mbutton_held;
            } else {
                m_mbutton_state = mbutton_release;
            }
            break;
        }
        case mbutton_held: {
            if (mouse_down) {
                m_mbutton_state = mbutton_held;
            } else {
                m_mbutton_state = mbutton_release;
            }
            break;
        }
        case mbutton_release: {
            if (mouse_down) {
                m_mbutton_state = mbutton_press;
            } else {
                m_mbutton_state = mbutton_clear;
            }
            break;
        }
        case mbutton_clear:
            if (mouse_down) {
                m_mbutton_state = mbutton_press;
            } else {
                m_mbutton_state = mbutton_clear;
            }

            break;
        default:
            m_mbutton_state = mbutton_clear;
    }

    dump_state_transition(old, m_mbutton_state);
}
