//
// Glass Display
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#include <input_plane.h>

input_plane_t::input_plane_t(rect_t rect, point_t plane_origin, std::shared_ptr<plane_t> parent) : plane_t(rect, plane_origin, parent),
                                                                                                   m_hotspot(-1, -1)
{
}

rect_t
input_plane_t::rect()
{
    return rect_t(point_t(0, 0), m_plane.boundingRect().size());
}

void
input_plane_t::add_desktop(std::shared_ptr<desktop_plane_t> plane)
{
    if (!plane) {
        return;
    }

    if (plane->renderable() && hotspot() == point_t(-1, -1)) {
        for (auto &display : plane->displays()) {
            if (!display) {
                continue;
            }

            rect_t extents(display->origin(), display->rect().size());
            hotspot() = display->origin() + plane->origin();
            break;
        }
    }

    // Manage geometry
    rect_t extents = rect_t(plane->origin().x(),
                            plane->origin().y(),
                            plane->rect().width(),
                            plane->rect().height());

    m_plane += extents;

    // Manage resources
    m_desktop_planes[plane->uuid()] = plane;
}

void
input_plane_t::reset()
{
    m_plane &= rect_t();

    for (auto desktop : m_desktop_planes.values()) {
        if (!desktop) {
            continue;
        }
        desktop->reset();
        desktop = nullptr;
    }
    m_desktop_planes.clear();

    m_current_desktop = nullptr;
}

qhash_t<uuid_t, std::shared_ptr<desktop_plane_t>> &
input_plane_t::desktops()
{
    return m_desktop_planes;
}

std::shared_ptr<desktop_plane_t>
input_plane_t::desktop(point_t point)
{
    for (auto desktop : m_desktop_planes.values()) {
        if (!desktop) {
            continue;
        }
        region_t visible_input = desktop->visible_region();
        visible_input.translate(desktop->origin().x(), desktop->origin().y());

        if (visible_input.contains(point)) {
            return desktop;
        }
    }

    return nullptr;
}
