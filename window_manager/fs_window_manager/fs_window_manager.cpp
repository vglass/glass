//
// Glass Display
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#include <fs_window_manager.h>
#include <gsl/gsl>
#include <vm_region.h>

fs_window_manager_t::fs_window_manager_t()
{
}

rect_t
fs_window_manager_t::target(uuid_t uuid, point_t point)
{
    auto desktop = m_input_plane->desktop(point);

    if (!desktop) {
        return rect_t();
    }

    point_t desktop_point = m_input_plane->map_to(desktop.get(), point);

    auto rtp = desktop->render_target(uuid, desktop_point);
    if (rtp) {
        auto rsp = rtp->render_source();
        if (rsp) {
            return rsp->rect();
        }
    }

    return rect_t();
}

rect_t
fs_window_manager_t::clamp_rect(uuid_t uuid, point_t point, bool mouse_down)
{
    auto desktop = m_input_plane->desktop(point).get();
    display_plane_t *check_display = nullptr;

    if (desktop) {
        auto check_point = m_input_plane->map_to(desktop, point);
        check_display = desktop->display(check_point);
    }

    if (desktop && check_display && !mouse_down && m_seamless_mouse &&
        (desktop->uuid() == desktop_uuid_t() || m_vms.contains(desktop->uuid()))) {

        if (m_input_plane->current_desktop() && m_input_plane->current_desktop()->current_display() && m_input_plane->current_desktop() != desktop) {
            m_input_plane->current_desktop()->current_display()->hide_cursor();
        }

        m_input_plane->set_current_desktop(desktop);

        auto vm = vm_at_location(point);

        if (vm) {
            set_render_focus(vm->uuid());
        }
    }
    desktop = m_input_plane->current_desktop();

    if (!desktop) {
        return m_input_plane->rect();
    }

    auto desktop_point = m_input_plane->map_to(desktop, point);
    auto display = desktop->display(desktop_point);
    auto check_rtp = desktop->render_target(uuid, display);

    if (display && display != desktop->current_display() &&
        check_rtp && (check_rtp->render_source() || !desktop->renderable())) {

        for (auto old_desktop : m_input_plane->desktops()) {
            if (!old_desktop) {
                continue;
            }

            if (old_desktop->current_display()) {
                old_desktop->current_display()->hide_cursor();
            }
        }

        desktop->set_current_display(display);
    }
    display = desktop->current_display();

    if (!display) {
        return m_input_plane->map_from(desktop, desktop->rect());
    }

    auto rtp = desktop->render_target(uuid, display);

    if (!rtp || !rtp->render_source()) {
        return m_input_plane->rect();
    }

    if (mouse_down || !m_seamless_mouse) {
        return m_input_plane->map_from(desktop, rtp->parent_rect());
    } else {
        return m_input_plane->rect();
    }
}

guest_mouse_event
fs_window_manager_t::map_event_to_guest(uuid_t uuid, point_t point, bool mouse_down, bool has_absolute_sink, bool is_touch)
{
    (void) mouse_down;
    guest_mouse_event mouse_event(point_t(-1, -1), QSize(0, 0));

    auto desktop = m_input_plane->desktop(point);
    if (!desktop) {
        return mouse_event;
    }

    point_t desktop_point = m_input_plane->map_to(desktop.get(), point);

    auto display = desktop->display(desktop_point);
    if (!display) {
        return mouse_event;
    }

    uint32_t valid_rsp_count = 0;
    for (auto &rtp : desktop->render_targets(uuid)) {
        if (!rtp) {
            continue;
        }

        if (!rtp->render_source() || !rtp->render_source()->valid()) {
            continue;
        }
        valid_rsp_count++;
    }
    auto rtp = desktop->render_target(uuid, display);

    if (rtp) {
        point_t rtp_point = desktop->map_to(rtp, desktop_point);
        auto rsp = rtp->render_source();

        if (rsp && rsp->valid()) {
            if (!rtp->is_qemu() || has_absolute_sink) {
                if (rsp->cursor()) {
                    display->show_cursor(rsp->cursor());
                }
                point_t rsp_point = rtp->map_to(rsp, rtp_point);

                if (valid_rsp_count == 1) {
                    mouse_event.point = rsp_point;
                } else {
                    mouse_event.point = is_touch ? rsp_point : rsp_point + rsp->origin();
                }

                if (mouse_event.point.x() < 0) {
                    mouse_event.point.setX(0);
                }

                if (mouse_event.point.y() < 0) {
                    mouse_event.point.setY(0);
                }

                mouse_event.size = is_touch ? rsp->rect().size() : m_vms[uuid]->guest_desktop().boundingRect().size();

                if (mouse_event.size == QSize(0, 0)) {
                    mouse_event.size = rsp->rect().size();
                }

                mouse_event.translate_status = TRANSLATE_SUCCESS;
                return mouse_event;

            } else {
                // Handle as relative, because it's in qemu?
                mouse_event.size = QSize(1, 1);
                mouse_event.translate_status = TRANSLATE_SOLARIS_HACK;
                return mouse_event;
            }
        } else {
            vg_debug() << "rsp bad or invalid";
        }
    } else if (!desktop->renderable() && uuid == desktop->uuid()) {
        mouse_event.point = desktop_point;
        mouse_event.size = m_vms[uuid]->guest_desktop().boundingRect().size();
        mouse_event.translate_status = TRANSLATE_SUCCESS;

        return mouse_event;
    }

    for (auto &desk : m_input_plane->desktops()) {
        if (!desk) {
            continue;
        }

        for (auto &d : desk->displays()) {
            if (!d) {
                continue;
            }

            d->hide_cursor();
        }
    }

    mouse_event.translate_status = TRANSLATE_FAIL;
    return mouse_event;
}

window_key_t
fs_window_manager_t::key(uuid_t uuid, render_plane_t *render_plane)
{
    (void) uuid;
    return static_cast<window_key_t>(render_plane->key());
}

void
fs_window_manager_t::create_render_targets(uuid_t uuid)
{
    uint32_t display_count = 0;
    desktop_uuid_t duuid = desktop_uuid(uuid);
    auto desktop = m_input_plane->desktops()[duuid];

    if (!desktop) {
        // Throw?
        return;
    }

    QMutexLocker locker(desktop->lock());

    //If desktop is not renderable, we have pt gpu. If we have pt gpu,
    //we should bail out because all the correct render targets are created.
    if (!desktop->renderable()) {
        vg_debug() << "create_render_targets(): Have pt_gpu desktop, already have good rtps, bailing.";
        auto vm = m_vms[uuid];
        if (vm) {
            vm->add_visible_rect(desktop->rect());
        }
        return;
    }

    desktop->reset_render_planes(uuid);
    for (auto &display : desktop->displays()) {
        if (!display) {
            continue;
        }

        desktop->render_targets(uuid).push_back(std::make_unique<render_target_plane_t>(display->render_plane().rect(),
                                                                                        display->origin() + display->render_plane().origin(),
                                                                                        desktop));

        desktop->render_targets(uuid).back()->set_key(display->unique_id());

        m_vms[uuid]->clear_visible_region();
        m_vms[uuid]->add_visible_rect(desktop->render_targets(uuid).back()->parent_rect());
        display_count++;
    }

    if (m_vms[uuid] && display_count) {
        m_vms[uuid]->update_render_targets(desktop.get(), uuid);
        m_vms[uuid]->set_visible_updated(true);
    }
}

template <typename Overlay>
void
fs_window_manager_t::process_overlays(Overlay &overlays,
                                      std::shared_ptr<desktop_plane_t> &desktop,
                                      std::shared_ptr<display_plane_t> &display,
                                      region_t &calc_visible_region,
                                      region_t &overlay_clip)
{
    if (!desktop) {
        return;
    }

    if (!display) {
        return;
    }

    for (auto &overlay : overlays) {
        if (!overlay) {
            continue;
        }

        if (display->unique_id() != overlay->display_id() || !overlay->visible()) {
            continue;
        }
        overlay->process_updates(display->framebuffer());
        overlay->clip_visible_region(overlay_clip);
        calc_visible_region -= desktop->map_from(display.get(), overlay->visible_region());
        overlay_clip -= desktop->map_from(display.get(), overlay->visible_region());
    }
}

void
fs_window_manager_t::process_updates()
{
    qhash_t<desktop_plane_t *, uuid_t> focus_map;
    qhash_t<desktop_plane_t *, region_t> calc_visible_region;
    list_t<battery_overlay_t>::iterator battery_overlay;
    list_t<switcher_overlay_t>::iterator switcher_overlay;
    list_t<text_overlay_t>::iterator text_overlay;
    region_t overlay_clip;

    for (auto &desktop : m_input_plane->desktops()) {
        if (!desktop) {
            continue;
        }

        calc_visible_region[desktop.get()] = desktop->rect();
        for (auto &display : desktop->displays()) {
            if (!display) {
                continue;
            }

            if (!display->framebuffer()) {
                continue;
            }
            overlay_clip = display->framebuffer()->rect();
            process_overlays(m_banner_overlays,
                             desktop,
                             display,
                             calc_visible_region[desktop.get()],
                             overlay_clip);
            process_overlays(m_switcher_overlays,
                             desktop,
                             display,
                             calc_visible_region[desktop.get()],
                             overlay_clip);
            process_overlays(m_text_overlays,
                             desktop,
                             display,
                             calc_visible_region[desktop.get()],
                             overlay_clip);
        }
    }

    for (std::list<uuid_t>::reverse_iterator rit = m_focus_stack.rbegin(); rit != m_focus_stack.rend(); ++rit) {
        uuid_t uuid = *rit;
        vm_region_t *vm = m_vms[uuid].get();
        desktop_plane_t *desktop = m_input_plane->desktops()[desktop_uuid(uuid)].get();
        if (!vm) {
            continue;
        }

        // Not checking the desktop pointer here, as it isn't dereferenced
        if (!focus_map.contains(desktop)) {
            focus_map[desktop] = uuid;
        }

        if (focus_map[desktop] == uuid) {
            if (m_global_update) {
                vm->add_dirty_region(vm->guest_desktop());
            }
            vm->clear_visible_region();
            vm->add_visible_region(calc_visible_region[desktop]);
            check_and_set_banner_overlays(desktop, vm->base());
        } else {
            vm->clear_visible_region();
        }
    }

    m_global_update = false;
}

list_t<uuid_t>
fs_window_manager_t::render_focus_stack()
{
    process_updates();
    return m_focus_stack;
}
