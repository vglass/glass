//
// Glass Display
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#ifndef TEXT_OVERLAY__H
#define TEXT_OVERLAY__H

#include <QObject>
#include <glass_types.h>
#include <overlay.h>
#include <vm_region.h>

class text_overlay_t : public QObject, public overlay_t
{
    Q_OBJECT

public:
    text_overlay_t(uint32_t display_id, region_t region, std::string text);
    virtual ~text_overlay_t() = default;

    const std::string &text();

    void process_updates(std::shared_ptr<framebuffer_t> display);
    void render(QPainter &p,
                desktop_plane_t *desktop,
                display_plane_t *display,
                region_t &display_clip,
                region_t &painted_clip);

private:
    rect_t m_draw_rect;
    point_t m_text_origin;
    std::string m_text;
    int m_count;
};

#endif //TEXT_OVERLAY__H
