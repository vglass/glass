//
// Glass Display
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#include "clock_overlay.h"
#include <QTime>

clock_overlay_t::clock_overlay_t(point_t position, region_t region) : overlay_t(region),
                                                                      m_position(position),
                                                                      m_width(0),
                                                                      m_height(0),
                                                                      m_clock_rect(rect_t(0, 0, 0, 0))
{
}

void
clock_overlay_t::render(QPainter &painter,
                        desktop_plane_t *desktop,
                        display_plane_t *display,
                        region_t &display_clip,
                        region_t &painted_clip)
{
    (void) desktop;
    (void) display;

    QString current_time = QTime::currentTime().toString("hh:mm");

    if (m_width == 0 && m_height == 0) {
        QFont font("DejaVu Sans", 12);
        QFontMetrics fm(font);
        m_width = fm.horizontalAdvance(current_time + "Z");
        m_height = fm.height();
        m_clock_rect = rect_t(m_position, QSize(m_width + 4, m_height + 2));
    }

    if (m_last_time[painter.device()] != current_time) {
        m_region = m_clock_rect;
        painter.setClipRegion(m_clock_rect);
        painter.setPen(Qt::black);
        painter.fillRect(m_clock_rect, Qt::white);
        painter.drawText(point_t(m_position.x() + 2, m_height - m_height / 5),
                         current_time + "Z");
        m_last_time[painter.device()] = current_time;
    }

    display_clip -= m_clock_rect;
    painted_clip += m_clock_rect;
}
